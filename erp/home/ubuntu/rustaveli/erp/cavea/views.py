# -*- coding: utf-8 -*-
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth import authenticate, login, logout
from django.core import serializers
from datetime import datetime, timedelta
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
import random
from cavea.models import *
import django_socketio
from jsonify.decorators import ajax_request
from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail, EmailMessage, EmailMultiAlternatives
from email.MIMEImage import MIMEImage
from django.template import Context
from django.template.loader import get_template
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render
import hashlib, uuid
import testing
from testing.models import *
from api.models import *
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from pymongo import *
#import pymongo
from bson.objectid import ObjectId
from erp.settings import DATABASES
import uuid
import json
from bson.timestamp import Timestamp
from bson import BSON
from bson import json_util
from django.views.decorators.cache import cache_page

@csrf_exempt
def change_language(request):
    request.session[request.POST["variable"]] = request.POST["lang"]
    return HttpResponse(request.session[request.POST["variable"]])


def cavea_main(request):

    d_type = "json"
    #cache.clear()
    lang = "geo"
    user = "anon"
    if request.user.is_authenticated():
        user = request.user
    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    url = settings.ROOT_URL
    pages = Page.objects.filter(is_active=True).order_by("position")
    current_movies = get_current_movies(request, lang)

    coming_soon_movies = get_coming_soon_movies(request)

    if d_type=="json":
        current_movies = json.dumps(current_movies, default=json_util.default)
        coming_soon_movies = json.dumps(coming_soon_movies, default=json_util.default)

    #return HttpResponse(current_movies)
    return render(request, "cavea/index.html",{"url":url,
                                               "d_type":d_type,
                                               "pages": pages,
                                               "lang":lang,
                                               "title":"Main Page",
                                               "current_movies":current_movies,
                                               "coming_soon_movies":coming_soon_movies,
                                               "user":user})

def movie(request, movie_cat):
    time_from = datetime.now()-timedelta(days=14)
    time_to = datetime.now()  -timedelta(days=7)
    d_type = "json"
    lang = "geo"
    uuids = []
    for n in range(1,30):
        uuids.append(str(uuid.uuid1()))
    request.session["uuids"] = uuids

    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    url = settings.ROOT_URL
    pages = Page.objects.filter(is_active=True).order_by("position")
    movie = get_objects("testing_movie", [{"_id":ObjectId(request.GET["id"])}], "_id")

    movie_descr = None
    if lang =="geo":
        movie_descr = get_objects("testing_moviedescription",[{"movie_id":ObjectId(request.GET["id"])}],"_id")
    else:
        movie_descr = get_objects("testing_moviedescriptionenglish",[{"movie_id":ObjectId(request.GET["id"])}],"_id")
    title = movie[0]["name"]
    if lang=="eng":
        title = movie[0]["name_english"]

    translation = {"genre":{"eng":"Genre", "geo":"ჟანრი"},
                   "directors":{"eng":"Director", "geo":"რეჯისორი"},
                   "cast":{"eng":"Cast", "geo":"როლებში"},
                   "script":{"eng":"Script", "geo":"სცენარი"},
                   "premiere_in_georgia":{"eng":"Premiere in Georgia", "geo":"პრემიერა საქართველოში"},
                   "premiere":{"eng":"Premiere", "geo":"მსოფლიო პრემიერა"},
                   "budget":{"eng":"Budget", "geo":"ბიუჯეტი"},
                   "imdb_score":{"eng":"IMDB Score", "geo":"IMDB რეიტინგი"},
                   "description":{"eng":"Description", "geo":"არწერა"},
                   "language":{"eng":"Language", "geo":"გახმოვანება"},
                   "hall":{"eng":"Hall", "geo":"დარბაზი"},
                   "time":{"eng":"Time", "geo":"დასაწყისი"},
                   "date":{"eng":"Date", "geo":"თარიღი"},
                   }
    sess_filter = [{"movie_id":ObjectId(request.GET["id"])}]
    halls = get_distinct_ids("testing_hall", [{"name":{"$regex":"cav_"}}])
    sess_filter.append({"hall_id":{"$in":halls}})
    sess_filter.append({"start_time":{"$gt":time_from}})
    sess_filter.append({"start_time":{"$lt":time_to}})
    sessions = get_grouped_objects("testing_session",sess_filter,"$start_time")
    sessions = sessions["result"]
    sessions.sort()

    return render(request, "cavea/movie.html",{"url":url,
                                               "d_type":d_type,
                                               "pages": pages,
                                               "lang":lang,
                                               "title":title,
                                               "movie":movie[0],
                                               "descr":movie_descr[0],
                                               "trans":translation,
                                               "sessions":sessions
                                               })
    return HttpResponse(request.GET["id"])

def cavea_sessions(request):
    time_from = datetime.now()-timedelta(days=1)
    time_to = datetime.now() + timedelta(days=7)
    lang = "geo"
    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    url = settings.ROOT_URL
    pages = Page.objects.filter(is_active=True).order_by("position")
    sess = []
    movies = get_current_movies(request, lang)
    halls = get_distinct_ids("testing_hall", [{"name":{"$regex":"cav_"}}])


    for m in movies:
        movie_name = m["name"]
        if lang=="eng":
            movie_name = m["name_english"]
        sess_filter =[{"movie_id":ObjectId(m["_id"])}]
        sess_filter.append({"hall_id":{"$in":halls}})
        sess_filter.append({"start_time":{"$gt":time_from}})
        sess_filter.append({"start_time":{"$lt":time_to}})
        sessions = get_grouped_objects("testing_session",sess_filter,"$start_time")
        sessions = sessions["result"]
        sessions.sort()
        sess.append({"movie_name":movie_name,"sessions":sessions})
    #return HttpResponse(sess)
    return render(request, "cavea/sessions.html",{"url":url,
                                                  "pages": pages,
                                                  "lang":lang,
                                                  "title":"Sessions",
                                                  "sess":sess})

def login_page(request):
    path = request.GET.get("next")
    lang = "geo"
    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    url = settings.ROOT_URL
    pages = Page.objects.filter(is_active=True).order_by("position")
    return render(request, "cavea/login.html",{"url": url,
                                               "pages": pages,
                                               "lang":lang,
                                               "next": path,
                                               "title":"Login Page"})

def cavea_booking(request):
    lang = "geo"
    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    url = settings.ROOT_URL
    pages = Page.objects.filter(is_active=True).order_by("position")
    return render(request, "cavea/booking.html",{"url":url, "pages": pages,"lang":lang,"title":"Booking"})

def cavea_discounts(request):
    lang = "geo"
    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    url = settings.ROOT_URL
    pages = Page.objects.filter(is_active=True).order_by("position")
    return render(request, "cavea/discounts.html",{"url":url, "pages": pages,"lang":lang,"title":"Discounts"})


def get_distinct_ids(collection, filter):
    k = MongoClient()
    result = k["rustaveli"][collection].find({"$and":filter}).distinct("_id")
    return result

def get_distinct_ids_by_attr(collection, filter, attr):
    k = MongoClient()
    result = k["rustaveli"][collection].find({"$and":filter}).distinct(attr)
    return result

def get_objects(collection, filter,sort):
    k = MongoClient()
    result = k["rustaveli"][collection].find({"$and":filter}).sort(sort,-1)
    return list(result)


def get_grouped_objects(collection,filter, group_by):
    k = MongoClient()
    sessions = k["rustaveli"]["testing_session"].aggregate([{"$match":{"$and":filter}},{"$sort":{"start_time":1}},
                                                            {"$group":{
                                                                "_id":{"$substr":["$start_time",0,10]},
                                                                "sessions":{
                                                                    "$push":{
                                                                        "id":"$_id",
                                                                        "is_3d":"$is_3d",
                                                                        "lang":"$session_language",
                                                                        "prices":"$prices"
                                                                    }}
                                                                                                  }}])
    return sessions


def get_current_movie_ids(request, lang):
    k = MongoClient()
    movie_details_collection = "testing_moviedescription"
    if lang=="eng":
        movie_details_collection = "testing_moviedescriptionenglish"
    time_from = datetime.now() - timedelta(minutes=440)
    unique_halls = get_distinct_ids("testing_hall",[{"theater_id": {"$ne":ObjectId("55411ef3956167856613a01d")}},{"theater_id":ObjectId("54ec23a69561676764d1a778")}])
    unique_movies =  get_distinct_ids_by_attr("testing_session", [{"hall_id":{"$in":unique_halls}}, {"start_time":{"$gt":time_from}}],"movie_id")


def get_current_movies(request, lang):
    k = MongoClient()
    time_from = datetime.now() - timedelta(minutes=440)
    movie_details_collection = "testing_moviedescription"
    if lang=="eng":
        movie_details_collection = "testing_moviedescriptionenglish"
    time_from = datetime.now() - timedelta(minutes=440)
    unique_halls = get_distinct_ids("testing_hall",[{"theater_id": {"$ne":ObjectId("55411ef3956167856613a01d")}},{"theater_id":ObjectId("54ec23a69561676764d1a778")}])
    #unique_movies =  get_distinct_ids_by_attr("testing_session", [{"hall_id":{"$in":unique_halls}}, {"start_time":{"$gt":time_from}}],"movie_id")
    unique_movies =  get_distinct_ids_by_attr("testing_session", [{"start_time":{"$gt":time_from}}],"movie_id")

    #sessions = list(k["rustaveli"]["testing_session"].find({"$and":[{"hall_id":{"$in":unique_halls}}, {"start_time":{"$gt":time_from}}]}))


    movies_descr = list(get_objects("testing_moviedescription", [{"movie_id":{"$in":unique_movies}}],"_id"))
    #movies_descr = list(k["rustaveli"][movie_details_collection].find({"$and":[{"movie_id":{"$in":unique_movies}}]},{"imdb_score":1, "movie_id":1}))

    m_d = {}
    for m_ in movies_descr:
        m_d.update({str(m_["movie_id"]):m_["imdb_score"]})

    movies = get_objects("testing_movie", [{"_id":{"$in":unique_movies}}],"_id")
    for m in movies:
        m.update({"_id":str(m["_id"])})
        #m.update({"imdb_score":m_d[str(m["_id"])]})
    return movies

def get_movie_details(language):
    collection = "testing_moviedescription"
    if language =="eng":
        collection = "testing_moviedescriptionenglish"

    time_from = datetime.now() - timedelta(minutes=440)
    unique_halls = get_distinct_ids("testing_hall",[{"theater_id": {"$ne":ObjectId("55411ef3956167856613a01d")}},{"theater_id":ObjectId("54ec23a69561676764d1a778")}])
    unique_movies =  get_distinct_ids_by_attr("testing_session", [{"hall_id":{"$in":unique_halls}}, {"start_time":{"$gt":time_from}}],"movie_id")
    movies = get_objects(collection, [{"_id":{"$in":unique_movies}}],"_id")
    return movies

def get_coming_soon_movies(request):
    movies = get_objects("testing_movie", [{"coming_soon":True}],"website_rank")
    for m in movies:
        m.update({"_id":str(m["_id"])})
    return movies

@csrf_exempt
def get_current_movies_short(request):
    movies = get_objects("testing_movie", [{"is_active":True}],"website_rank")
    return movies

@csrf_exempt
def get_seats_for_hall(request):
    hall_name = request.POST["session"].split("/")[0]
    hall_id = Hall.objects.get(name=hall_name)
    k = MongoClient()
    print request.user.is_authenticated()
    if request.user.is_authenticated():
        seats = list(k["rustaveli"]["testing_seat"].find({"$and":[{"hall_id":ObjectId(hall_id.id)}]},{"name":1,
                                                                                                  "width":1,
                                                                                                  "left":1,
                                                                                                  "top":1,
                                                                                                  "ttype":1,
                                                                                                  "row":1,
                                                                                                  "seat":1
                                                                                                  }))
        return HttpResponse(json.dumps(seats, default=json_util.default))
    else:
        return HttpResponse("notloggedin")

@csrf_exempt
def get_session_tickets(request):
    request.session["cur_session"] = request.POST["session"]
    time_now = datetime.now()
    time_post = datetime.strptime(request.POST["session"].split("/")[1],"%d-%m-%Y")
    session = request.POST["session"]
    k = MongoClient()
    if request.user.is_authenticated():
        tickets = list(k["rustaveli"]["testing_session"].find({"$and":[{"_id":session}]},{"prices":1,"session_language":1,"tickets.emb_seat":1}))
        bl_tickets = list(k["rustaveli"]["testing_ticket"].find({"$and":[{"session_id":session},{"canceledByUser":None}]}))

        return HttpResponse(json.dumps(tickets, default=json_util.default))
    else:
        return HttpResponse("notloggedin")




def logout_user(request):
    logout(request)
    return HttpResponseRedirect(request.POST["url"])

def login_user(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    next=request.POST.get('next')
    token = request.POST.get('csrfmiddlewaretoken')

    user = auth.authenticate(username=username, password=password)

    if user is not None and user.is_active:
        user_ip=request.META.get('REMOTE_ADDR')
        hard_id='fake_Id'

        create_token(username, hard_id, user_ip)
        auth.login(request, user)
        if request.POST['source'] == 'cavea.ge':
            token = django.middleware.csrf.get_token(request)
            Shift.objects.create(agent="554cebb6956167b76e9f1e09", csrftoken=token,user=username).save()
            request.session["csrf_token"] = token
            print token
        return HttpResponseRedirect(next)
    else:
        return HttpResponseRedirect("/cavea/login/?next="+next)