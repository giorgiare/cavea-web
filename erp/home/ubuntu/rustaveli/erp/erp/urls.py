from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from bar.views import *

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^admin_tools/', include('admin_tools.urls')),
    url("", include('django_socketio.urls')),
    url(r'^$', 'testing.views.home', name='home'),
    url(r'^logout/$', "testing.views.logout_user"),
    url(r'^accounts/login/$',"testing.views.account_login"),
    url(r'^login/$', "testing.views.login_user"),  
    url(r'^search/$', "testing.views.search"),
    url(r'^post/$', "testing.views.post"),
    url(r'^hall/$', "testing.views.hall"),
    url(r'^session/$', "testing.views.session"),
    url(r'^ticket/$', "testing.views.ticket"),
    url(r'^ticket_order/$', "testing.views.ticket_order"),
    url(r'^test_post/$', "testing.views.test_post"),
    url(r'^ticketSales/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{,2}:\d{2})/$',"testing.views.ticketSales"),
    url(r'^session/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{,2}:\d{2})/$',"testing.views.session"),
    url(r'^session/(?P<hall>\w{5})/$',"testing.views.session_hall"),
    url(r'^validation/(?P<ticket>\w+)/$',"testing.views.validation"),
    url(r'^test/$',"testing.views.test"),
    url(r'^session/add/$',"testing.views.session_add"),
    url(r'^refresh_seats/$',"testing.views.refresh_seats"),
    url(r'^remove_ticket/$',"testing.views.remove_ticket"),
    url(r'^block_ticket/$',"testing.views.block_ticket"),
    url(r'^get_blocked_tickets/$',"testing.views.get_blocked"),
    url(r'^swapTimeBetweenSessions/$',"api.views.swapTimeBetweenSessions"),

    url(r'^test_admin/$',"api.views.test_admin"),

    url(r'^SaveConfig/$',"testing.views.SaveConfig"),
    url(r'^GetConfig/$',"testing.views.GetConfig"),

    #sessions editor Aresha
    url(r'^CreateSessionSet/$',"testing.views.create_session_set"),
    url(r'^create_session/$',"testing.views.create_session"),
    url(r'^createMovie/$',"api.views.createMovie"),
    url(r'^deleteSessions/$',"api.views.deleteSessions"),
    url(r'^updateSession/$',"api.views.updateSession"),
    url(r'^RemoveReservation/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{,2}:\d{2})/$',"testing.views.remove_reservation"),
    
    url(r'^hall.api/(?P<cinema_id>\w+)/$',"testing.views.get_halls_by_cinema"),
    url(r'^seat.api/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{,2}:\d{,2})/$',"testing.views.get_seats_by_session"),
    url(r'^seatstates.api/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{,2}:\d{2})/$',"testing.views.get_seats_states_by_session"),
    url(r'^seatstatessimple.api/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{,2}:\d{2})/$',"testing.views.get_seats_states_by_session_simple"),
    url(r'^theater.api/$', "testing.views.get_theaters"),
    url(r'^theaternew.api/$', "testing.views.get_theaters_non_ios"),
    
    
    url(r'^sendemail/$',"testing.views.sendemail"),
    url(r'^send_ticket/$',"testing.views.send_ticket_to_user_external"), # send iOS ticket
    url(r'^send_webticket/$',"testing.views.sent_weborder_to_external"),    # send WebTicketToEmail
    #url(r'^send_webticket/$',"testing.views.send_webticket_to_user"),
    url(r'^sendWithAttachment/$',"testing.views.sendmail_withattachment"),
    #url(r'^show_order/$',"testing.views.show_weborder_fororder"),
    url(r'^showonlineorder.api/(?P<order_id>\w+)/$',"testing.views.show_weborder_external"),
    url(r'^getOrderByPin/$',"testing.views.getOrderByPin"),
    url(r'^testPython.api/$',"testing.views.testPython"),

    #url(r'^theaterApi/$',"api.views.theaterApi"),
    url(r'^createBarOrder/$',"bar.views.createBarOrder"),
    url(r'^testDelete/$',"api.views.testDelete"),
    url(r'^cancelBarOrder/$',"api.views.cancelBarOrder"),
    url(r'^addItemsToBarOrder/$',"api.views.addItemsToBarOrder"),
    url(r'^removeItemsFromBarOrder/$',"api.views.removeItemsFromBarOrder"),

    #url(r'^TicketBookAPI/$',"api.views.TicketBookApi"),
    #url(r'^TicketBuyAPI/$',"api.views.TicketBuyApi"),
    #url(r'^TicketBuyBookedAPI/$',"api.views.TicketBuyBookedApi"),

    #Sessions
    url(r'^session.api/(?P<hall_id>\w+)/$',"testing.views.get_sessions_by_hall"),
    url(r'^specificsession.api/$', "testing.views.get_specific_session"),
    url(r'^sessionNew.api/(?P<hall_id>\w+)/$',"testing.views.get_sessions_by_hall_only_new"),
    url(r'^sessionforday.api/$',"testing.views.get_sessions_by_days"),
    url(r'^sessionforcinema.api/$',"testing.views.get_sessions_by_cinema_range"),

    url(r'^getSessionsByDates/$',"api.views.getSessionsByDates"),
    url(r'^seatsGenerator/$',"api.views.seatsGenerator"),
    url(r'^createSeats/$',"api.views.createSeats"),
    url(r'^loadSeats/$',"api.views.loadSeats"),
    url(r'^removeSeats/$',"api.views.removeSeats"),

    url(r'^testlog.api/$',"testing.views.test_log"),


    #url(r'^ticketselect.api/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{2}:\d{2})/(?P<seat_id>\w+)/$', "testing.views.ticket_selects"),
    # UNUSED
    # url(r'^ticketbook.api/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{,2}:\d{2})/(?P<seat_id>\w+)/$', "testing.views.ticket_book"),
    # url(r'^ticketbuy.api/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{,2}:\d{2})/(?P<seat_id>\w+)/$', "testing.views.ticket_buy"),
    # url(r'^ticketSelectsAPI/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{,2}:\d{2})/(?P<seat_id>\w+)/$', "testing.views.ticketSelectsAPI"),

    #url(r'^sock.api/$',"testing.views.send_to_dispatch_sock_server"),
#    url(r'^shifts.api/$', "testing.views.register_shift"),

    url(r'^boxofficeproductbuy.api/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{,2}:\d{2})/$',"api.views.boxoffice_buy_product"),

    
    url(r'^discountsApi/$', "testing.views.DiscountsApi"),
    url(r'^discountname.api/$',"testing.views.DiscountDescriptionApi"),
    url(r'^bardiscountsApi/$', "testing.views.BarDiscountsApi"),
    url(r'^ticketselect.api/$', "testing.views.TicketSelectApi"),
    url(r'TicketUnselectAPI/$',"testing.views.TicketUnselectApi"),
    url(r'^ticketcancel.api/$', "testing.views.TicketCancelApi"),
    url(r'^TicketBookAPI/$',"testing.views.TicketBookApi"),
    url(r'^TicketBuyAPI/$',"testing.views.TicketBuyApi"),
    url(r'^TicketBuyBookedAPI/$',"testing.views.TicketBuyBookedApi"),

    url(r'^boxofficeproducts.api/$',"api.views.get_boxoffice_product_list"),

    # REPORTS
    url(r'^agentreport.api/$',"testing.views.get_report_for_agent"),
    url(r'^reports.api/$', "reports.views.all_tickets_report"),
    url(r'^theaterfullreport.api/$', "reports.views.full_theater_report"),
    url(r'^barreports.api/$', "reports.views.bar_full_report"),
    url(r'^barreportsnew.api/$', "reports.views.bar_full_report_new"),
    url(r'^barincomelog.api/$', "reports.views.bar_income_log"),
    url(r'^userstats.api/$', "reports.views.stats_about_users"),
    url(r'^userorderslist.api/$', "reports.views.user_orders_list"),
    url(r'^moviestats.api/$', "reports.views.stats_about_movie"),
    url(r'^reportboxmanager.api', "testing.views.getOrdersForAgent"),
    url(r'^filmsforreport.api/(?P<cinema_id>\w+)/$',"testing.views.getFilmsForReport"),

    # CUSTOMERS
    url(r'^registercustomer.api/$', "testing.views.register_customer"),
    url(r'^creategiftcardwithcustomer', "testing.views.create_giftcard_with_customer_id"),
    url(r'^findgiftcard.api/$', "testing.views.find_giftcard"),
    url(r'^findcustomer.api/$',"testing.views.find_customer"),

    url(r'^registerexternaluser.api/$', "testing.views.create_external_user"),


    url(r'^getPoster.api/(?P<movieID>\w+)/$', "testing.views.getPosterForMovieApi"),
    url(r'^getsessionsbymovieincinema.api/(?P<cinema_id>\w+)/(?P<movie_id>\w+)/$', "testing.views.get_sessions_by_cinema_movie"),
    url(r'^getsessionsbymovieincinemadate.api/(?P<cinema_id>\w+)/(?P<movie_id>\w+)/(?P<date_offset>\w+)/$', "testing.views.get_sessions_by_cinema_movie_date"),
    url(r'^getcleansession.api/$', "testing.views.get_session_clean_info"),


    url(r'^allmovies.api/$',"testing.views.getallmovies"),
    url(r'^allactivemovies.api/$',"testing.views.getallactivemovies"),
    url(r'^films.api/(?P<cinema_id>\w+)/$', "api.views.getFilmsForCinema"),
    url(r'^filmdetails.api/$',"testing.views.get_movie_details"),
    url(r'^filmdetailsenglish.api/$',"testing.views.get_movie_details_english"),
    url(r'^allfilms.api/$', "testing.views.getAllFilms"),
    url(r'^allfilmsnotesting.api',"testing.views.getAllNoTestingFilms"),
    url(r'^comingsoonfilms.api',"testing.views.get_all_comingsoon_films"),



    url(r'^getreservationsforsession.api/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{,2}:\d{2})/$',"testing.views.getReseverationsForSession"),
    url(r'^reservationsexpired.api/$',"testing.views.check_expired_reservations"),
    url(r'^findorder.api/$',"testing.views.find_order"),
    url(r'^findordersbought.api/$',"testing.views.find_orders_bought_for_user"),
    url(r'^findreservationsuser.api/$',"testing.views.find_reservations_for_user"),
    url(r'^findticket.api/$',"testing.views.find_ticket"),
    url(r'^validate.api/(?P<order_id>\w+)/$',"testing.views.validateOrder"),
    url(r'^authenticate.api/$',"testing.views.loginUser"),
    url(r'^getlistofboxagents.api/$',"testing.views.get_listof_box_agents"),

    #TBC Payment URLS
    url(r'^orderpayonline.api/$',"testing.views.OrderPayOnline"),
    url(r'^checkonlinepaymentstatus.api/$',"testing.views.check_transaction_status_return"),
    url(r'^paymentok.api/$',"testing.views.paymentok"),
    url(r'^cancelonlineorder.api/$',"testing.views.cancel_online_order_transaction"),
    url(r'^closecardday.api/$',"testing.views.close_card_day"),
    url(r'^listonlineorders.api/$',"testing.views.list_online_app_orders"),
    url(r'^listonlineordersagent.api/$',"testing.views.list_online_orders"),

    #Bar Api Giorgi
    url(r'^bar/', include('bar.urls')),
    url(r'^bar/catalog/$', catalog),
     url(r'^bar/get_category_products/(?P<categoryId>\d+)/$', get_category_products),
     url(r'^bar/product/create/$', BarProductCreate),
     url(r'^bar/category/create/$', BarCategoryCreate),
     url(r'^bar/category/edit/$', BarCategoryEdit),
     url(r'^bar/product/list/$', ProductList),
     url(r'^bar/categories/$', getCategories),
     url(r'^bar/allProducts/(?P<barId>\w+)/$', getAllProducts),
     url(r'^bar/productsbycategory/(?P<categoryId>\d+)/(?P<barId>\w+)/$', get_category_products),
     url(r'^bar/transferproducts/$', transferProducts),
     url(r'^bar/getbarsbybar/$', getBarsByBar),
     url(r'^bar/getallbarlist/$', get_all_bar_list),
     url(r'^bar/getclientsbybar/$', get_clients_by_bar),
     url(r'^bar/transferproductsdiscount/$', transferProductsDiscount),
     url(r'^register_cavea/$', 'testing.views.register_user_cavea'),
     url(r'^change_password_user_cavea/$', 'testing.views.change_password_user_cavea'),     
     url(r'^test_post_curl/$', 'testing.views.test_post_url'),


     #website
     url(r'^websitegetcurrentmovies.api/$',"testing.views.get_all_current_movies"),
     url(r'^websitegetmovieinfo.api/$',"testing.views.get_movie_info"),
     url(r'^websitegetsessionsbymovie.api/$',"testing.websiteViews.get_sessions_by_movie"),

     url(r'^updateSessionTime/$',"api.views.updateSessionTime"),
    #cleanup cron
     url(r'^cron.api/$',"testing.views.five_minute_cron"),

    #server ping
     url(r'^serverstatus.api/$',"testing.views.server_is_alive"),

    #test sms
     url(r'^sendsms.api/$',"testing.views.test_sms"),

    #url(r'^erp/', include('erp.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
     url(r'^cavea/', include('cavea.urls')),
     url(r'^android/', include('android.urls')),

)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root':settings.MEDIA_ROOT}))
