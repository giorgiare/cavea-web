from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from cavea.views import *


urlpatterns = patterns('',
#Android
url(r'^get_current_movies/(?P<theater>\w{3})/(?P<lang>\w{3})/(?P<type>\w{4})/$', 'android.views.get_current_movies'),\
url(r'^get_coming_soon_movies/$', 'android.views.get_coming_soon_movies'),\
url(r'^get_movie_description/(?P<movie_id>\w{24})/(?P<lang>\w{3})/$', 'android.views.get_movie_description'),\
url(r'^get_sessions_by_movie_and_theater/(?P<movie_id>\w{24})/(?P<theater>\w{3})/$', 'android.views.get_sessions_by_movie_and_theater'),\
url(r'^get_current_sessions/(?P<theater>\w{3})/(?P<lang>\w{3})/$', 'android.views.get_current_sessions'),\
url(r'^get_current_day_sessions_by_hall/(?P<hall>\w{5})/(?P<lang>\w{3})/$', 'android.views.get_current_day_sessions_by_hall'),\
)