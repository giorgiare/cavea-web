from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, timedelta

# Create your models here.


class Page(models.Model):
    name = models.CharField(max_length=30)
    title_geo = models.CharField(max_length=30)
    title_eng = models.CharField(max_length=30)
    url = models.CharField(max_length=60)
    position = models.IntegerField(default=0)
    is_active = models.BooleanField(default=True)


class PasswordHashItem(models.Model):
    user = models.ForeignKey(User)
    key = models.CharField(max_length=30)
    created = models.DateTimeField(default=datetime.now())
