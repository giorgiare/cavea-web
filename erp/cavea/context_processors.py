# -*- coding: utf-8 -*-
from cavea.models import *
from datetime import datetime, timedelta
from pymongo import *
import json
from bson import json_util

pages = Page.objects.filter(is_active=True).order_by("position")
translations = {"login":{"eng":"Login", "geo":"შესვლა"},
                "logout":{"eng":"Logout", "geo":"გამოსვლა"},
                "profile":{"eng":"Profile", "geo":"პროფილი"},
                "sessions":{"eng":"Sessions", "geo":"სეანსები"},
                "buy":{"eng":"Buy", "geo":"ყიდვა"},
                "book":{"eng":"Book", "geo":"დაჯავშნა"},
                "email":{"eng":"Email", "geo":"ელ. ფოსტა"},
                "submit":{"eng":"Submit", "geo":"დადასტურება"},
                "new_password":{"eng":"New Password", "geo":"ახალი პაროლი"},
                "password":{"eng":"Password", "geo":"პაროლი"},
                "first_name":{"eng":"First Name", "geo":"სახელი"},
                "last_name":{"eng":"Last Name", "geo":"გვარი"},
                "confirm_password":{"eng":"Confirm Password", "geo":"პაროლის დადასტურება"},
                "mobile":{"eng":"Mobile", "geo":"მობილური"},
                "register":{"eng":"Register", "geo":"რეგისტრაცია"},
                "passwords_dont_match":{"eng":"Passwords do not match!", "geo":"პარილები არ ემთხვევა!"},
                "email_format_error":{"eng":"Email format is not valid!", "geo":"ელ.ფოსტის ფორმატი არასწორია!"},
                "first_name_is_required":{"eng":"First name field is required!", "geo":"მომხმარებლის სახელი სავალდებულოა!"},
                "last_name_is_required":{"eng":"Last name is required!", "geo":"მომხმარებლის გვარი სავალდებულოა!"},
                "email_is_required":{"eng":"Email is required!", "geo":"მომხმარებლის ელ.ფოსტა სავალდებულოა!"},
                "mobile_is_required":{"eng":"Mobile is required!", "geo":"მომხმარებლის მობილურის ნომერი სავალდებულოა!"},
                "password_is_required":{"eng":"Password is required!", "geo":"მომხმარებლის პაროლი სავალდებულოა!"},
                "confirm_password_is_required":{"eng":"Password confirmation is required!", "geo":"მომხმარებლის პაროლის დადასტურება სავალდებულოა!"},
                "password_is_short":{"eng":"Password must be minimum 6 characters!", "geo":"პაროლი უნდა იყოს არანაკლებ 6 ასოსი!"},
                "ticket":{"eng":"Tickets","geo":"ბილეთი"},
                "forgot_password":{"eng":"Forgot Password?","geo":"პაროლის შეცვლა"},
                "change_password":{"eng":"Change Password","geo":"პაროლის შეცვლა"},
                "language":{"eng":"language" , "geo":"ენა"},
                "georgian":{"eng":"Georgian" , "geo":"ქართული"},
                "russian":{"eng":"Russian" , "geo":"რუსული"},
                "english":{"eng":"English" , "geo":"ინგლისური"},
                "date":{"eng":"Date" , "geo":"თარიღი"},
                "time":{"eng":"Time" , "geo":"დრო"},
                "hall":{"eng":"Hall" , "geo":"დარბაზი"},
                "row":{"eng":"Row" , "geo":"რიგი"},
                "seat":{"eng":"Seat" , "geo":"ადგილი"},
                "price":{"eng":"price" , "geo":"ფასი"},
                "pin":{"eng":"Pin" , "geo":"პინი"},
                "cancel":{"eng":"Cancel" , "geo":"დაბრუნება"},
                "quantity":{"eng":"Qty." , "geo":"რაო."},
                "accept":{"eng":"Accept" , "geo":"გადახდა"},
                "total":{"eng":"Total" , "geo":"ჯამი"},
                "buy":{"eng":"Buy" , "geo":"ყიდვა"},
                "name":{"eng":"Movie Name" , "geo":"ფილმის დასახელება"},
                "warning":{"eng":"Be aware that you will have only 3 minutes to complete the transaction" , "geo":"გაფრთხილებთ რომ ბილეთის შესაძენად გადახდის გვერდზე გექნებათ მხოლოდ 3 წუთი"},
               }

def cart(request):
    k = MongoClient()
    user = request.user.username
    if not user:
        return {"cart":0}
    orders = list(k["rustaveli"]["testing_ticketorder"].find({"user":user},{"reservation":1, "session_id":1, "items":1}))
    total = 0
    for order in orders:
        spl = order["session_id"].split("/")
        t = "%s %s" % (spl[1],spl[2])
        if datetime.strptime(t, "%d-%m-%Y %H:%M") > datetime.now()-timedelta(days=1):
            total += len(order["items"])

    return {"cart":total}


def translation(requesst):

    return {"translations":translations}

def content(request):

    return {"content": {"pages":pages}}
