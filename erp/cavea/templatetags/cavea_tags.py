# -*- coding: utf-8 -*-
from django import template
import datetime
from django.utils.timezone import now
from decimal import Decimal
import json
import django

months = {
		"01":{"geo":"იან","eng":"Jan"},
		"02":{"geo":"თებ","eng":"Feb"},
		"03":{"geo":"მარ","eng":"Mar"},
		"04":{"geo":"აპრ","eng":"Apr"},
		"05":{"geo":"მაი","eng":"May"},
		"06":{"geo":"ივნ","eng":"Jun"},
		"07":{"geo":"ივლ","eng":"Jul"},
		"08":{"geo":"აგვ","eng":"Aug"},
		"09":{"geo":"სექ","eng":"Sep"},
		"10":{"geo":"ოქტ","eng":"Oct"},
		"11":{"geo":"ნოე","eng":"Nov"},
		"12":{"geo":"დეკ","eng":"Dec"},
}

days = {
		"6":{"geo":"კვირა","eng":"Sunday"},
        "0":{"geo":"ორშაბათი","eng":"Monday"},
        "1":{"geo":"სამშაბათი","eng":"Tuesday"},
        "2":{"geo":"ოთხშაბათი","eng":"Wednsday"},
        "3":{"geo":"ხუთშაბათი","eng":"Thursday"},
        "4":{"geo":"პარასკევი","eng":"Friday"},
        "5":{"geo":"შაბათი","eng":"Suturday"},
}
register = template.Library()

@register.filter
def translate(value,arg):
    return value[arg]
@register.filter
def get_time(value):
    spl = value.split("/")
    return spl[2]\

@register.filter
def get_date(value):
    spl = value.split("/")
    return spl[1]
@register.filter
def get_std_and_sta(value):
    js = json.loads(value)
    return js["std"]+"-"+js["sta"]
@register.filter
def get_3d_tag(value):
    if value:
        return "3D"
    else:
        return ""
@register.filter
def convert_date(value):
    d = datetime.datetime.strptime(value, "%Y-%m-%d")
    return d.strftime("%d-%m-%Y")

@register.filter
def get_string(value, arg):
    d = datetime.datetime.strptime(value["_id"], "%Y-%m-%d")
    return "{} - {} {}".format(days[str(d.weekday())][arg], d.strftime("%d"), months[d.strftime("%m")][arg])
    return value["_id"]

@register.filter
def user_logged_in(value):
    if str(value)=="AnonymousUser":
        return False
    else:
        return True

@register.filter
def get_token_from_request(request):
    return django.middleware.csrf.get_token(request)
