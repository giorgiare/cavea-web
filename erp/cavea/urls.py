from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from cavea.views import *


urlpatterns = patterns('',
url(r'^sessions$', 'cavea.views.cavea_sessions'),\
url(r'^booking$', 'cavea.views.cavea_booking'),
url(r'^discounts$', 'cavea.views.cavea_discounts'),\
url(r'^change_language/', 'cavea.views.change_language'),
url(r'^get_current_movies/', 'cavea.views.get_current_movies'),
url(r'^get_coming_soon_movies/', 'cavea.views.get_coming_soon_movies'),
url(r'^movie/(?P<movie_cat>\w+)/$', 'cavea.views.movie'),
url(r'^logout_user$', 'cavea.views.logout_user'),
url(r'^get_seats_for_hall/$', 'cavea.views.get_seats_for_hall'),
url(r'^get_session_tickets/$', 'cavea.views.get_session_tickets'),
url(r'^login_user/$', 'testing.views.login_user'),
url(r'^login/$', "cavea.views.login_page"),
url(r'^getOrderByPinCavea/$', "cavea.views.getOrderByPinCavea"),
url(r'^profile$', "cavea.views.get_list_of_reservations_for_cavea"),
url(r'^seat.api/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{,2}:\d{,2})/$',"testing.views.get_seats_by_session"),
url(r'^generate_password_change_access_for_user/$', "cavea.views.generate_password_change_access_for_user"),
url(r'^user_password_change/(?P<username>[^/]+)/(?P<key>\w+)/$', "cavea.views.user_password_change"),
url(r'^register_cavea/$', 'cavea.views.register_user_cavea'),
url(r'^change_password_user_cavea/$', 'cavea.views.change_password_user_cavea'),
url(r'^', 'cavea.views.cavea_main'),


                       )
