# -*- coding: utf-8 -*-
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth import authenticate, login, logout
from django.core import serializers
from datetime import datetime, timedelta
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
import random
from django.core.mail import EmailMultiAlternatives
import django
from django.contrib.auth.models import User
from cavea.models import *
import django_socketio
from jsonify.decorators import ajax_request
from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail, EmailMessage, EmailMultiAlternatives
from email.MIMEImage import MIMEImage
from django.template import Context
from django.template.loader import get_template
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render
import hashlib, uuid
import testing
from testing.models import *
from api.models import *
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from pymongo import *
#import pymongo
from bson.objectid import ObjectId
from erp.settings import DATABASES
import uuid
import json
from bson.timestamp import Timestamp
from bson import BSON
from bson import json_util
from django.views.decorators.cache import cache_page
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
import hashlib
from cavea.models import PasswordHashItem
from django.core.mail import EmailMultiAlternatives
from testing.views import create_token
from django.contrib.auth.backends import ModelBackend

ROOT_URL = "http://127.0.0.1:8000/cavea"

@csrf_exempt
def change_language(request):
    request.session[request.POST["variable"]] = request.POST["lang"]
    return HttpResponse(request.session[request.POST["variable"]])


def cavea_main(request):
    d_type = "json"
    #cache.clear()
    lang = "geo"
    user = "anon"
    username = ""
    if "username" in request.GET and request.GET["username"]:
        username = request.GET["username"]
    if request.user.is_authenticated():
        user = request.user
    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    title = "Main Page"
    if lang == "geo":
        title = "მთავარი გვერდი"

    url = ROOT_URL
    pages = Page.objects.filter(is_active=True).order_by("position")
    current_movies = get_current_movies(request, lang)

    coming_soon_movies = get_coming_soon_movies(request)

    if d_type=="json":
        current_movies = json.dumps(current_movies, default=json_util.default)
        coming_soon_movies = json.dumps(coming_soon_movies, default=json_util.default)

    #return HttpResponse(current_movies)
    return render(request, "cavea/index.html",{"url":url,
                                               "d_type":d_type,
                                               "username": username,
                                               "lang":lang,
                                               "title":title,
                                               "current_movies":current_movies,
                                               "coming_soon_movies":coming_soon_movies,
                                               "user":user}

                                               )

def movie(request, movie_cat):
    time_from = datetime.now()-timedelta(days=7)
    time_to = datetime.now() + timedelta(days=0)
    d_type = "json"
    lang = "geo"
    uuids = []
    for n in range(1,30):
        uuids.append(str(uuid.uuid1()))
    request.session["uuids"] = uuids
    if movie_cat =="comint_soon":
        request.session["cur_session"] = None
        request.session["is_coming_soon"] = True
    else:
        request.session["is_coming_soon"] = False
    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    url = ROOT_URL
    pages = Page.objects.filter(is_active=True).order_by("position")
    movie = get_objects("testing_movie", [{"_id":ObjectId(request.GET["id"])}], "_id")

    movie_descr = None
    if lang =="geo":
        movie_descr = get_objects("testing_moviedescription",[{"movie_id":ObjectId(request.GET["id"])}],"_id")
    else:
        movie_descr = get_objects("testing_moviedescriptionenglish",[{"movie_id":ObjectId(request.GET["id"])}],"_id")
    title = movie[0]["name"]
    if lang=="eng":
        title = movie[0]["name_english"]

    translation = {"genre":{"eng":"Genre", "geo":"ჟანრი"},
                   "directors":{"eng":"Director", "geo":"რეჯისორი"},
                   "cast":{"eng":"Cast", "geo":"როლებში"},
                   "script":{"eng":"Script", "geo":"სცენარი"},
                   "premiere_in_georgia":{"eng":"Premiere in Georgia", "geo":"პრემიერა საქართველოში"},
                   "premiere":{"eng":"Premiere", "geo":"მსოფლიო პრემიერა"},
                   "budget":{"eng":"Budget", "geo":"ბიუჯეტი"},
                   "imdb_score":{"eng":"IMDB Score", "geo":"IMDB რეიტინგი"},
                   "description":{"eng":"Description", "geo":"აღწერა"},
                   "language":{"eng":"Language", "geo":"გახმოვანება"},
                   "hall":{"eng":"Hall", "geo":"დარბაზი"},
                   "time":{"eng":"Time", "geo":"დასაწყისი"},
                   "date":{"eng":"Date", "geo":"თარიღი"},
                   "rus":{"eng":"Russian", "geo":"რუსული"},
                   "eng":{"eng":"English", "geo":"ინგლისური"},
                   "geo":{"eng":"Georgian", "geo":"ქართული"},
                   "cav":{"eng":"Cavea", "geo":"კავეა"},
                   "cep":{"eng":"East Point", "geo":"ისტ პოინტი"},
                   "total":{"eng":"Total amount", "geo":"სულ გადასახდელია"},
                   "lari":{"eng":"GEL", "geo":"ლ"},
                   "session":{"eng":"Session", "geo":"სეანსები"},
                   "booking":{"eng":"Booking", "geo":"დაჯავშნა"},
                   "comments":{"eng":"Comments", "geo":"კომენტარები"},
                   "buy":{"eng":"Buy", "geo":"ყიდვა"},
                   "book":{"eng":"Book", "geo":"დაჯავშნა"},
                   }
    sess_filter = [{"movie_id":ObjectId(request.GET["id"])}]
    halls = get_distinct_ids("testing_hall", [{"name":{"$regex":"cav_"}},{"name":{"$regex":"cep_"}}])
    sess_filter.append({"hall_id":{"$in":halls}})
    sess_filter.append({"start_time":{"$gt":time_from}})
    sess_filter.append({"start_time":{"$lt":time_to}})
    sessions = get_grouped_objects("testing_session",sess_filter,"$start_time")
    sessions = sessions["result"]
    sessions.sort()


    return render(request, "cavea/movie.html",{"url":url,
                                               "d_type":d_type,
                                               "lang":lang,
                                               "title":title,
                                               "movie":movie[0],
                                               "descr":movie_descr[0],
                                               "trans":translation,
                                               "trans_json": json.dumps(translation),
                                               "sessions":sessions}

                                               )
    return HttpResponse(request.GET["id"])

def cavea_sessions(request):
    time_from = datetime.now()-timedelta(days=7)
    time_to = datetime.now() + timedelta(days=-1)
    lang = "geo"
    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    url = ROOT_URL
    pages = Page.objects.filter(is_active=True).order_by("position")
    sess = []
    movies = get_current_movies(request, lang)
    halls = get_distinct_ids("testing_hall", [{"name":{"$regex":"cav_"}}])
    title = "სეანსები"
    if lang=="eng":
        title = "Sessions"

    for m in movies:
        movie_name = m["name"]
        movie_poster = m["poster_url"]
        movie_id = m["_id"]
        if lang=="eng":
            movie_name = m["name_english"]
        sess_filter =[{"movie_id":ObjectId(m["_id"])}]
        sess_filter.append({"hall_id":{"$in":halls}})
        sess_filter.append({"start_time":{"$gt":time_from}})
        sess_filter.append({"start_time":{"$lt":time_to}})
        sessions = get_grouped_objects("testing_session",sess_filter,"$start_time")
        sessions = sessions["result"]
        sessions.sort()
        sess.append({"movie_id":movie_id,"movie_name":movie_name,"movie_poster":movie_poster,"sessions":sessions})
    #return HttpResponse(sess)
    return render(request, "cavea/sessions.html",{"url":url,
                                                  "pages": pages,
                                                  "lang":lang,
                                                  "title":title,
                                                  "sess":sess})

def login_page(request):
    path = request.GET.get("next")
    lang = "geo"
    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    url = ROOT_URL
    pages = Page.objects.filter(is_active=True).order_by("position")
    return render(request, "cavea/login.html",{"url": url,
                                               "pages": pages,
                                               "lang":lang,
                                               "next": path,
                                               "title":"Login Page"})

def cavea_booking(request):
    lang = "geo"
    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    url = ROOT_URL
    pages = Page.objects.filter(is_active=True).order_by("position")
    return render(request, "cavea/booking.html",{"url":url, "pages": pages,"lang":lang,"title":"Booking"})

def cavea_discounts(request):
    lang = "geo"
    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    url = ROOT_URL
    pages = Page.objects.filter(is_active=True).order_by("position")
    return render(request, "cavea/discounts.html",{"url":url, "pages": pages,"lang":lang,"title":"Discounts"})


def get_distinct_ids(collection, filter):
        k = MongoClient()
	result = k["rustaveli"][collection].find({"$or":filter}).distinct("_id")
	return result

def get_distinct_ids_by_attr(collection, filter, attr):
    k = MongoClient()
    result = k["rustaveli"][collection].find({"$and":filter}).distinct(attr)
    return result

def get_objects(collection, filter,sort):
    k = MongoClient()
    result = k["rustaveli"][collection].find({"$and":filter}).sort(sort,-1)
    return list(result)


def get_grouped_objects(collection,filter, group_by):
    k = MongoClient()
    sessions = k["rustaveli"]["testing_session"].aggregate([{"$match":{"$and":filter}},{"$sort":{"start_time":1}},
                                                            {"$group":{
                                                                "_id":{"$substr":["$start_time",0,10]},
                                                                "sessions":{
                                                                    "$push":{
                                                                        "id":"$_id",
                                                                        "is_3d":"$is_3d",
                                                                        "movie_id":"$movie_id",
                                                                        "lang":"$session_language",
                                                                        "prices":"$prices"
                                                                    }}
                                                                                                  }}])
    return sessions


def get_current_movie_ids(request, lang):
    k = MongoClient()
    movie_details_collection = "testing_moviedescription"
    if lang=="eng":
        movie_details_collection = "testing_moviedescriptionenglish"
    time_from = datetime.now() - timedelta(minutes=440)
    unique_halls = get_distinct_ids("testing_hall",[{"theater_id": {"$ne":ObjectId("55411ef3956167856613a01d")}},{"theater_id":ObjectId("54ec23a69561676764d1a778")}])
    unique_movies =  get_distinct_ids_by_attr("testing_session", [{"hall_id":{"$in":unique_halls}}, {"start_time":{"$gt":time_from}}],"movie_id")

    return unique_movies

def get_current_movies(request, lang):
    k = MongoClient()
    movie_details_collection = "testing_moviedescription"
    if lang=="eng":
        movie_details_collection = "testing_moviedescriptionenglish"
    time_from = datetime.now() - timedelta(minutes=440)
    unique_halls = get_distinct_ids("testing_hall",[{"theater_id": {"$ne":ObjectId("55411ef3956167856613a01d")}},{"theater_id":{"$in": [ObjectId("55e0d0da956167a5a79cc43c"), ObjectId("54ec23a69561676764d1a778")]}}])
    #unique_halls = get_distinct_ids("testing_hall",[{"theater_id": {"$ne":ObjectId("55411ef3956167856613a01d")}},{"theater_id":ObjectId("54ec23a69561676764d1a778")}])
    unique_movies =  get_distinct_ids_by_attr("testing_session", [{"hall_id":{"$in":unique_halls}}, {"start_time":{"$gt":time_from}}],"movie_id")

    #movies_descr = list(get_objects("testing_moviedescription", [{"movie_id":{"$in":unique_movies}}],"_id"))
    movies_descr = list(k["rustaveli"][movie_details_collection].find({"$and":[{"movie_id":{"$in":unique_movies}}]},{"imdb_score":1, "movie_id":1}))
    sessions = list(k["rustaveli"]["testing_session"].find({"$and":[{"hall_id":{"$in":unique_halls}}, {"start_time":{"$gt":time_from}}]}))


    m_d = {}
    for m_ in movies_descr:
        m_d.update({str(m_["movie_id"]):m_["imdb_score"]})

    movies = get_objects("testing_movie", [{"_id":{"$in":unique_movies}}],"_id")
    for m in movies:
        m.update({"_id":str(m["_id"])})
        m.update({"imdb_score":m_d[str(m["_id"])]})
    return movies

def get_movie_details(language):
    collection = "testing_moviedescription"
    if language =="eng":
        collection = "testing_moviedescriptionenglish"

    time_from = datetime.now() - timedelta(minutes=440)
    unique_halls = get_distinct_ids("testing_hall",[{"theater_id": {"$ne":ObjectId("55411ef3956167856613a01d")}},{"theater_id":ObjectId("54ec23a69561676764d1a778")}])
    unique_movies =  get_distinct_ids_by_attr("testing_session", [{"hall_id":{"$in":unique_halls}}, {"start_time":{"$gt":time_from}}],"movie_id")
    movies = get_objects(collection, [{"_id":{"$in":unique_movies}}],"_id")
    return movies

def get_coming_soon_movies(request):
    movies = get_objects("testing_movie", [{"coming_soon":True}],"website_rank")
    for m in movies:
        m.update({"_id":str(m["_id"])})
    return movies

def get_current_movies_short(request):
    movies = get_objects("testing_movie", [{"is_active":True}],"website_rank")
    return movies


def get_seats_for_hall(request):
    hall_name = request.POST["session"].split("/")[0]
    hall_id = Hall.objects.get(name=hall_name)
    k = MongoClient()

    if request.user.is_authenticated():
        seats = list(k["rustaveli"]["testing_seat"].find({"$and":[{"hall_id":ObjectId(hall_id.id)}]},{"name":1,
                                                                                                  "width":1,
                                                                                                  "left":1,
                                                                                                  "top":1,
                                                                                                  "ttype":1,
                                                                                                  "row":1,
                                                                                                  "seat":1
                                                                                                  }))
        return HttpResponse(json.dumps(seats, default=json_util.default))
    else:
        return HttpResponse("notloggedin")


def get_session_tickets(request):
    request.session["cur_session"] = request.POST["session"]
    time_now = datetime.now()
    time_post = datetime.strptime(request.POST["session"].split("/")[1],"%d-%m-%Y")

    session = request.POST["session"]
    k = MongoClient()
    if request.user.is_authenticated():
        tickets = list(k["rustaveli"]["testing_session"].find({"$and":[{"_id":session}]},{"prices":1,"session_language":1,"tickets.emb_seat":1}))
        #bl_tickets = list(k["rustaveli"]["testing_ticket"].find({"$and":[{"session_id":session},{"canceledByUser":None}]}))

        return HttpResponse(json.dumps(tickets, default=json_util.default))
    else:
        return HttpResponse("notloggedin")



def logout_user(request):
    logout(request)
    return HttpResponseRedirect(request.POST["url"])

def login_user(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    next=request.POST.get('next')
    user = auth.authenticate(username=username, password=password)

    if user is not None and user.is_active:
        user_ip=request.META.get('REMOTE_ADDR')
        hard_id='fake_Id'

        auth.login(request, user)
        return HttpResponseRedirect("/")
    else:
        return HttpResponseRedirect("http://google.com")



def get_list_of_reservations_for_cavea(request):
    lang = "geo"
    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    title = "Profile"
    if lang=="geo":
        title="პროფილი"
    user = request.user.username
    if not user:
        return HttpResponseRedirect("login/?next=/cavea/profile")
    k = MongoClient();
    orders = list(k["rustaveli"]["testing_ticketorder"].find({"user":user},{"reservation":1, "session_id":1, "items":1}))
    url = ROOT_URL
    output = []
    for order in orders:
        spl = order["session_id"].split("/")
        t = "%s %s" % (spl[1],spl[2])
        total = 0
        if datetime.strptime(t, "%d-%m-%Y %H:%M") > datetime.now()-timedelta(days=1):
            session = Session.objects.get(pk=order["session_id"])
            movie = Movie.objects.get(pk = session.movie_id)
            name = {"geo": movie.name, "eng":movie.name_english}
            order.update({"name":name})
            order.update({"date":session.pk.split("/")[1]})
            order.update({"time":session.pk.split("/")[2]})
            order.update({"count":len(order["items"])})
            order.update({"pin":order["reservation"]["pin"]})
            order.update({"lang":session.session_language})
            for item in order["items"]:
                total += item["price"]
            order.update({"total":float(total)})
            order.pop("items")
            order.pop("reservation")
            output.append(order)
    #return HttpResponse(json.dumps(orders, default=json_util.default))

    return render(request, "cavea/profile.html", {"url":url, "reservations":output, "lang":lang, "title":title})


@csrf_exempt
def generate_password_change_access_for_user(request):
    username = request.POST["username"]
    try:
        user = User.objects.get(username=username)
        hash_item = PasswordHashItem()
        #generate user-key pair
        hash_item.user = user
        hash_item.created = datetime.now()
        hash_str = user.email[:3] + hash_item.created.strftime("%d%m%Y%H%M")
        hash_item.key = hashlib.md5(hash_str).hexdigest()
        hash_item.save()


        subject, from_email, to = 'hello', '192.168.1.7', username
        text_content = 'Change password now.'
        html_content = '<p>Link to change password is : <a>%s/user_password_change/%s/%s/</a> message.</p>' % (ROOT_URL, user.username, hash_item.key)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return HttpResponse("done")
    except:
        return HttpResponse("no user")

def user_password_change(request, username,key):
    url = ROOT_URL
    lang = "geo"
    if "lang" in request.session and request.session["lang"]=="eng":
        lang = "eng"
    user  = User.objects.get(username=username)
    hash = PasswordHashItem.objects.filter(user = user, key=key)
    if len(hash):
        return render(request, "cavea/change_password.html", {"lang":lang, "username":username, "token":key, "url":url})
    else:
        return HttpResponse("not found")

def register_user_cavea(request):

    username = request.POST["username"]
    user_email = request.POST["user_email"]
    first_name = request.POST["first_name"]
    last_name = request.POST["last_name"]
    password = request.POST["password"]
    phone = request.POST["telephone"]
    next_ = request.POST["next"]
    #return HttpResponse(phone);

    user = User(username = username,first_name = first_name)
    user.last_name = last_name
    user.email = user_email
    user.set_password(password)
    if len(User.objects.filter(email=user_email)):
      return HttpResponse("Fail:user exists")
    else:
      user.save()
      subject, from_email, to = 'Cavea.ge Registration', '192.168.1.7', username
      text_content = 'Thanks for registering'
      html_content = "you are now registered"
      msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
      msg.attach_alternative(html_content, "text/html")
      msg.send()
      return HttpResponse("done")

def getOrderByPinCavea(request):
    if 'pin' in request.POST:
        pin = request.POST["pin"]
        order = TicketOrder.objects.raw_query({"reservation.pin":pin})
        seats = []
        for item in order[0].items:
            seat = testing.models.Seat.objects.get(pk=item.seat.pk)
            seats.append(seat)
        seats_json = serializers.serialize("json", seats)


        #order_json = serializers.serialize("json", [order[0]])
        return HttpResponse(json.dumps({"seats": json.loads(seats_json), "order": {"language": order[0].session.session_language,"cust_name": order[0].reservation.name, "cust_surname":order[0].reservation.surname, "cust_email": order[0].reservation.email , "cust_mobile":order[0].reservation.mobile,"session_id":order[0].session_id, "hall":order[0].session_id[:5]}, "prices":order[0].session.prices}))
        #return {"data":json.loads(seats_json)}

@csrf_exempt
def change_password_user_cavea(request):

  username = request.POST["username"]
  new_password = request.POST["password"]
  key = request.POST["key"]
  user  = User.objects.get(username=username)
  hash = PasswordHashItem.objects.filter(user = user, key=key)
  if len(hash):
      try:
          user = User.objects.get(username=username)
          user.set_password(new_password)
          user.save()
      except:
          pass
  hash.delete()
  return HttpResponse("done")
