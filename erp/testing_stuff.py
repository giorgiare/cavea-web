# -*- coding: utf-8 -*-
from pymongo import *
from bson.objectid import ObjectId
from erp.settings import DATABASES
from testing.models import *
from bson import json_util
import json
import csv
import datetime
import locale


def update():
  tickets = Ticket.objects.all()
  for ticket in tickets:
    sess_id = ticket.session_id
    session = Session.objects.get(pk=sess_id)
    hall = session.hall
    theater = hall.theater
    ticket.movie = session.movie
    ticket.hall = hall
    ticket.theater = theater
    ticket.save()
    print ticket.id


def mongo_do_connect():
    conn = Connection()
    db = conn[DATABASES["default"]["NAME"]]
    return db


def update_pymongo():
  conn = mongo_do_connect()
  sessions_conn = k["testing_session"]
  tickets_k = conn["testing_ticket"]
  sessions = list(sessions_k.find({"_id":{"$regex":"cav"}},{"movie_id":1, "hall_id":1}))
  sess = []
  
  for s in sessions:
    
    #tickets_k.update({})
    tickets = list(tickets_k.find({"session_id":s["_id"]},{}))
    c = 0
    for t in tickets:
      tickets_k.update({"_id":t["_id"]},{"$set":{"hall_id":s["hall_id"]}})
      

def add_hall_movie_theater():
    k = MongoClient()
    theaters = {"rus": {
        "_id": ObjectId("54a19c820bc7cb374e6436c4"),
        "name": "რუსთაველი"
    }, "ami": {
        "_id": ObjectId("54b631190bc7cb0deb0c4b75"),
        "name": "ამირანი"
    }, "cav": {
        "_id": ObjectId("54ec23a69561676764d1a778"),
        "name": "Cavea"
    }, "tes": {
        "_id": ObjectId("55411ef3956167856613a01d"),
        "name": "TestingCinema"
    }, "apo": {
        "_id": ObjectId("558ab8fb9561674800f4b3e5"),
        "name": "აპოლო ბათუმი"
    }}

    sess = list(k["rustaveli"]["testing_session"].find({}, {"hall_id": 1, "movie_id": 1}))
    sd = {}
    for i in sess:
        sd[i["_id"]] = i
        sd[i["_id"]]["theater_id"] = theaters[i["_id"][:3]]["_id"]

    
    listoftickets = list(k["rustaveli"]["testing_ticketorder"].find({}, {"session_id": 1}))
    j = 0
    for i in listoftickets:
        j += 1
        if j % 100 == 0:
            print j
        k["rustaveli"]["testing_ticketorder"].update({"_id": i["_id"]},
                                                     {"$set": {"movie_id": sd[i["session_id"]]["movie_id"],
                                                               "hall_id": sd[i["session_id"]]["hall_id"],
                                                               "theater_id": sd[i["session_id"]]["theater_id"]}})



def get_users_from_json():
    conn = mongo_do_connect()
    customer_conn = conn["testing_customer"]
    with open('/home/giorgiare/Desktop/rustaveli/users.json', 'rb') as data_file:
        data = json.loads(data_file.read())
        data.sort(key=lambda x:x["l"], reverse= False)

    c = 0
    for d in data:
        #match by phone and last name
        '''
        integ = 0
        str_ = "this.email.length==%s" % integ
        name_ = "this.surname=='%s'" % d["last_name"]
        users = list(customer_conn.find({"$and":[{"phone":d["telephone"]},{"surname":d["last_name"]},{"$where":"this.phone.length > 7"},{"$where":str_}]},{"email":1, "name":1, "surname":1}).sort([("surrname",-1)]))
     	'''
        #match by email has no phone
        users = list(customer_conn.find({"email":d["e"]},{"email":1, "name":1, "surname":1, "phone":1}))
     	if len(users):
            for u in users:
                if not len(u["name"]):
                    print "must update name " + d["e"]
                if not len(u["surname"]):
                    print "must update surname " + d["e"]
                if not len(u["phone"]):
                    print "must update phone " + d["e"]
                if not len(u["email"]):
                    print "must update phone " + d["e"]
        else:
            customer_conn.insert({"email": d["e"],"name":d["f"], "surname":d["l"], "phone":d["t"]})
            print "insert %s" % d["e"]
            c+=1
    print c
             #print u["name"] +"  "+u["surname"]+ "   " +d["first_name"]+ "   "+ d["last_name"] + "   " + d["user_email"] + "  "+d["telephone"]

def check_double_entries():
    conn = mongo_do_connect()
    customer_conn = conn["testing_customer"]
    customers_distinct = customer_conn.distinct("phone")
    str_ = ""
    c=0
    for cust in customers_distinct:
        de = list(customer_conn.find({"phone":cust}))
        if len(de)>1:
            c+=1
            str_ += "\n"
            for d in de:
                if len(cust):
                    #print d["email"] + "  "+ d["phone"]
                    str_ += str(c)+","+d["email"] + ","+ d["phone"]+"\n"
    f = open("/home/giorgiare/Desktop/rustaveli/users_double_entry.txt", "w+")
    f.write(str_)
    f.close()
    print c
#get_users_from_json()
#add_hall_movie_theater()
#check_double_entries()

def get_ticket_orders():
    conn = mongo_do_connect()
    order_conn = conn["testing_ticketorder"]
    agent_conn = conn["testing_agent"]
    theater_conn = conn["testing_theater"]
    hall_conn = conn["testing_hall"]
    theaters = list(theater_conn.find({"$where": "this.name.substr(0,7)!='Testing'"}).distinct("_id"))
    halls = hall_conn.find({"theater_id":{"$in":theaters}}).distinct("_id")
    agents = list(agent_conn.find({},{"name":1}))
    agents.sort(key=lambda x:x["name"])
    agents_d = {}
    for l in agents:
         agents_d[l["name"]] = l["_id"]
    output = {}
    total = 0

    for k, v in agents_d.iteritems():
        orders = order_conn.aggregate([{"$match":{"$and":[{"agent.name":k},{"hall_id":{"$in":halls}}]}},{"$group":{"_id":"$agent.name", "sum":{"$sum":"$discounted_amount"}}}])
        orders = orders["result"]
        if len(orders):
            if orders[0]["sum"]:
                print orders
                total += float(orders[0]["sum"])
    orders_total = order_conn.aggregate([{"$group":{"_id":{"$substr":["$created",0,10]}, "sum":{"$sum":"$discounted_amount"}}}])
    totals = orders_total["result"]
    totals.sort(key=lambda x:x["_id"], reverse=True)
    print totals


def get_daily_by_agent(agent):
    agent = ObjectId(agent)
    conn = mongo_do_connect()
    order_conn = conn["testing_ticketorder"]
    agent_conn = conn["testing_agent"]
    theater_conn = conn["testing_theater"]
    hall_conn = conn["testing_hall"]
    theaters = list(theater_conn.find({"$where": "this.name.substr(0,7)!='Testing'"}).distinct("_id"))
    halls = hall_conn.find({"theater_id":{"$in":theaters}}).distinct("_id")

    #orders_total = order_conn.aggregate([{"$match":{"$and":[{"agent.id":agent},{"hall_id":{"$in":halls}}]}},{"$group":{"_id":{"dayweek":{"$dayOfWeek": "$created"}}, "sum":{"$sum":"$discounted_amount"}}}])
    orders_total = order_conn.aggregate([{"$match":{"$and":[{"agent.id":agent},{"hall_id":{"$in":halls}}]}},{"$group":{"_id":{"$substr":["$created", 0,10]},"sum":{"$sum":"$discounted_amount"}}}])
    totals = orders_total["result"]
    totals.sort(key=lambda x:x["_id"], reverse=True)
    out = {}
    days = {"0":"Monday", "1":"Tuesday","2":"Wednesday", "3":"Thursday", "4":"Friday", "5":"Saturday", "6":"Sunday"}

    print totals


#get_daily_by_agent("554cebb6956167b76e9f1e09")

#update_pymongo()




def test_size():
    conn = mongo_do_connect()
    order_conn = conn["testing_ticketorder"]
    agent_conn = conn["testing_agent"]
    theater_conn = conn["testing_theater"]
    hall_conn = conn["testing_hall"]
    theaters = list(theater_conn.find({"$where": "this.name.substr(0,7)!='Testing'"}).distinct("_id"))
    halls = hall_conn.find({"theater_id":{"$in":theaters}}).distinct("_id")
    agents = list(agent_conn.find({},{"name":1}))
    agents.sort(key=lambda x:x["name"])
    agents_d = {}



    orders_total = order_conn.aggregate([{},{"$unwind":"$items"}])
    totals = orders_total["result"]
    totals.sort(key=lambda x:x["_id"], reverse=True)
    print totals

#test_size()

class Model():
    def __init__(self, app, model, attrs):
        self.__dict__ = {}
        self.model = model
        self.app = app
        self.filter = {}
        self.attrs = attrs


    def create_query(self):
        attributes = self.model._meta.get_all_field_names()
        for at in attributes:
            for k, v in self.attrs.iteritems():
                if at in self.attrs.values():
                    self.__dict__[k] = v

        k = MongoClient()
        self.collection = self.app+"_"+self.model.__name__.lower()
        q_filter ={}
        q_result = {}
        filter_list = []
        for r in self.attrs["result"]:
            for tr in r.keys():
                q_result.update({tr:r[tr]})
                '''
                if tr == "sum":
                    q_result.update({"sum":{"$sum":"$"+r[tr]}})
                if tr == "count":
                    q_result.update({"count":{"$sum":1}})
                if tr == "sum_size":

                    q_result.update({tr+"_"+r[tr]:{"$sum":{"$size":{"$ifNull":["$box_office_items",[]]}}}})

                '''

        q_result.update({"_id":"$"+self.attrs["group"]+"_id"})
        #filter_list.append({r:{"$ne":None}})
        if len(self.attrs["filter"])>0:
            for filt in self.attrs["filter"]:
                for tr in filt.keys():
                    filter_list.append({tr:filt[tr]})

        filter_list.append({"session_id":{"$regex":"cav_"}})
            #filter_list.append({r:{"$ne":0}})
        q_filter.update({"$match":{"$and":filter_list}})
        query_ = []
        if len(self.attrs["group"]):
            query_.append(q_filter)
            query_.append({"$group":q_result})
        else:
            d = [q_filter, q_result]
        #print json.dumps(d, default=json_util.default)
        str_ = """var from_ = new Date("%s");"
                var to_ = new Date("%s");""" % (from_, to_)
        result = k["rustaveli"][self.collection].aggregate(query_)
        print result["result"]

from_ = datetime.datetime.now()-datetime.timedelta(days=50)
to_ = from_ + datetime.timedelta(days=10)

'''
print from_.strftime("%d %b %Y %H:%M:%S") + " 0000"
print to_.strftime("%d %b %Y %H:%M:%S") + " 0000"
print from_.isoformat()
'''

sum_ = {"sum":{"$sum":"$discounted_amount"}}
count_ = {"count":{"$sum":1}}
sum_size_ = {"sum_size":{"$sum":{"$size":{"$ifNull":["$box_office_items",[]]}}}}
result_q = [sum_,count_,sum_size_]
'''
test = Model("testing", TicketOrder, { "result":result_q, "group":"movie", "filter":[{"created":{"$gt":from_}},{"created":{"$lt":to_}}]})
test.create_query()
sum_ = {"sum":{"$sum":"$price_paid"}}
count_ = {"count":{"$sum":1}}
result_q = [sum_,count_]

filter =[{"created":{"$gt":from_}},{"created":{"$lt":to_}},{"canceledByUser":None},{"canceledDate":None},{"process":"SL"}]
test = Model("testing", Ticket, { "result":result_q, "group":"movie", "filter":filter})
test.create_query()



def return_distinct(collection, attr, filt, arg):
        k = MongoClient()
	result = k["rustaveli"][collection].find({filt:arg}).distinct(attr)
	return result
halls =  return_distinct("testing_hall", "_id", "theater_id",ObjectId("54ec23a69561676764d1a778"))
print halls
print return_distinct("testing_session", "_id", "hall_id",{"$in":halls})
'''
get_users_from_json()
