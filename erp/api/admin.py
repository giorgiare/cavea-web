from django.contrib import admin
from testing.models import *
from models import *

class ClientAdmin(admin.ModelAdmin):
    list_display = ("ip",)


class PermissionAdmin(admin.ModelAdmin):
    list_display = ("name","model_name", "codename")



class AgentAdmin(admin.ModelAdmin):
    list_display = ("ttype", "name")


#admin.site.register(Client, ClientAdmin)
#admin.site.register(Permission, PermissionAdmin)