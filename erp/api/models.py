from django.db import models
import random
from testing.models import *
from datetime import datetime
from djangotoolbox.fields import EmbeddedModelField, DictField
from django_mongodb_engine.contrib import MongoDBManager
from jsonfield import JSONField
from django.db import models
import testing
from djangotoolbox.fields import ListField
from django.contrib.auth.models import User
# Create your models here.

from django.db.models.loading import cache as model_cache
if not model_cache.loaded:
    model_cache._populate()


class Seat(models.Model):
    uid = models.CharField(max_length=20) #, primary_key=True
    hall = models.ForeignKey(testing.models.Hall, null=True, related_name="api_hall")
    section = models.CharField(max_length=30, null=True)
    name = models.CharField(max_length=20)
    row = models.CharField(max_length=10)
    seat = models.IntegerField(blank=True,null=True)
    status = models.CharField(max_length=40,blank=True,null=True)
    left  = models.FloatField(null=True, blank=True)
    top  = models.FloatField(null=True, blank=True)
    width  = models.FloatField(null=True, blank=True)

    EMPTY = "emp"
    ECONOMY = "eco"
    STANDARD = "std"
    STATUS = "sta"
    VIP = "vip"

    SEAT_TYPES = ((ECONOMY, "Economy"),(STANDARD, "Standard"),(STATUS,"Status"),(VIP,"VIP"),(EMPTY,"Empty"))
    ttype = models.CharField(max_length=3,choices=SEAT_TYPES)

    def __unicode__(self):
        return self.uid  #r"Row:%s Seat:%s" % (self.row, self.seat)


class ClientRegistry(models.Model):
    uid = models.CharField(max_length=2000)
    username = models.CharField(max_length=120)
    ip = models.CharField(max_length=16)
    hardware_id = models.CharField(max_length=120)
    location_id = models.CharField(max_length=120)

class Client(models.Model):
    uid = models.CharField(max_length=120)
    ip = models.CharField(max_length=16)
    users = ListField(EmbeddedModelField(User))
    hardware_id = models.CharField(max_length=120)
    ttype = models.CharField(max_length=3,choices=(("bof", "Box Office"),("bar", "Bar"), ("wst", "Website")))
    emb_obj = EmbeddedModelField()

    def __unicode__(self):
        return self.uid


class ClientSession(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = EmbeddedModelField(User)
    client = EmbeddedModelField(Client)

    def __unicode__(self):
        return self.ttype


# class Seat(models.Model):
#     uid = models.CharField(max_length=20, primary_key=True) #, primary_key=True
#     hall = models.ForeignKey(Hall, null=True, related_name="hall_api")
#     section = models.CharField(max_length=30, blank=True, null=True)
#     name = models.CharField(max_length=20)
#     row = models.CharField(max_length=10)
#     seat = models.IntegerField(blank=True,null=True)
#     status = models.CharField(max_length=40,blank=True,null=True)
#     left  = models.FloatField(null=True, blank=True)
#     top  = models.FloatField(null=True, blank=True)
#     width  = models.FloatField(null=True, blank=True)

#     EMPTY = "emp"
#     ECONOMY = "eco"
#     STANDARD = "std"
#     STATUS = "sta"
#     VIP = "vip"

#     SEAT_TYPES = ((ECONOMY, "Economy"),(STANDARD, "Standard"),(STATUS,"Status"),(VIP,"VIP"),(EMPTY,"Empty"))
#     ttype = models.CharField(max_length=3,choices=SEAT_TYPES)

#     def __unicode__(self):
#         return self.uid  #r"Row:%s Seat:%s" % (self.row, self.seat)


