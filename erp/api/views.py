# -*- coding: utf-8 -*-
from django.views.decorators.csrf import csrf_exempt
import random
import django_socketio
from jsonify.decorators import ajax_request
from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail, EmailMessage
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render
from testing.models import *
from api.models import *
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
import datetime
import json
from django.http.response import HttpResponseRedirect, HttpResponse
from decimal import Decimal
from django.core import serializers
from django.conf import settings
from pip._vendor.requests.sessions import session
from django_socketio import events
import api

@ajax_request
@csrf_exempt


#utilities

def client_registry_checked(ip, hardware_id, location_id ):
    print ip, hardware_id, location_id
    reg = ClientRegistry.objects.filter(ip=ip,hardware_id=hardware_id, location_id=location_id)
    if reg:
        return True
    else:
        return False

def user_has_permission(user,permission):
    for perms in user.permissions:
        if perms.codename==permission:
            return True
    return False

def theaterApi(request):
    if "theater" in request.GET and request.GET["theater"]:
        theater = Theater.objects.get(name=request.GET["theater"])
        halls = Hall.objects.filter(theater=theater)
        return {"response":serializers.serialize("json", halls)}
    return HttpResponse("done")

@csrf_exempt
def createBarOrder(request):
    if validate_posted_variables(request, "client_id", "user_id"):
        client_id = request.POST["client_id"]
        user_id = request.POST["user_id"]

        client = Client.objects.get(pk=client_id)

        user = User.objects.get(pk=user_id)

        if user_has_permission(user, "create_bar_order") and client_registry_checked(client.ip,client.hardware_id, client.emb_obj.uid):
            str = "%s%s" % (client.pk, user.pk)
            order = BarOrder(client=client, user=user)
            order.save()

            return HttpResponse(order.pk)

def get_boxoffice_product_list(request):
    box = testing.models.BoxOfficeProduct.objects.all()
    response = {"status":"OK"}
    response['data'] = serializers.serialize("json",box)
    return HttpResponse(json.dumps(response))


def validate_posted_variables(request, *args):
    coll = []
    for arg in args:
        if arg in request.POST and request.POST[arg]:
            coll.append(True)
    if len(coll)==len(args):
        return True

@csrf_exempt
def addItemsToBarOrder(request):
    if validate_posted_variables(request, "items", "bar_order_id"):
        bar_order_id = request.POST["bar_order_id"]

        barOrder = BarOrder.objects.get(pk=bar_order_id)
        if not barOrder.items:
            barOrder.items = []
        cl = Client.objects.get(uid=barOrder.client)
        bar = cl.emb_obj

        json_items = json.loads(request.POST['items'])

        for json_item in json_items:
            product_id, qty = json_item
            product = Product.objects.get(pk=product_id)
            item = Item(product=product, qty=qty)

            for inv in product.inventory:
                if inv.bar_id==bar.pk:
                    inv.qty = inv.qty - qty
                    product.save()

            barOrder.total = barOrder.total + product.price*qty
            barOrder.items.extend([item])
            barOrder.save()
        return HttpResponse(barOrder.total)




@csrf_exempt
def testDelete(request):
    BarOrder.objects.raw_update({"_id": "54da2c8e0bc7cb7aac6a1b5e54d7d8ef0bc7cb37534786af"}, {'$pull' : {'items' : {'product.sku' : "234408092"}}})
    return HttpResponse("done")


def remove_embedded_item(bar_order_id, product_id):
    BarOrder.objects.raw_update({"uid": bar_order_id }, {'$pull' : {'items' : {'product.sku' : product_id}}})

@csrf_exempt
def removeItemsFromBarOrder(request):
    if validate_posted_variables(request, "product_id", "bar_order_id"):
        bar_order_id = request.POST["bar_order_id"]
        product_id = request.POST["product_id"]

        barOrder = BarOrder.objects.get(pk=bar_order_id)

        cl = Client.objects.get(uid=barOrder.client)
        bar = cl.emb_obj

        #use temprorary list to store items

        for item in barOrder.items:
            if item.product.sku==product_id:
                #reduce the total of order by eliminated product qty times product price
                barOrder.total = barOrder.total - item.product.price*item.qty
                barOrder.save()
                #increase stock of the product
                bar_id = barOrder.client.emb_obj.pk
                product = Product.objects.get(pk=product_id)
                for inv in product.inventory:
                    if inv.bar_id==bar.pk:
                        inv.qty = inv.qty + item.qty
                        product.save()
                remove_embedded_item(barOrder.uid,product_id)
                return HttpResponse("done")
        return HttpResponse(bar_order_id)

@csrf_exempt
def cancelBarOrder(request):
    if validate_posted_variables(request, "client_id", "user_id", "bar_order_id"):
        client_id = request.POST["client_id"]
        user_id = request.POST["user_id"]
        bar_order_id = request.POST["bar_order_id"]

        client = Client.objects.get(pk=client_id)
        user = User.objects.get(pk=user_id)
        barOrder = BarOrder.objects.get(pk=bar_order_id)
        if user_has_permission(user, "cancel_bar_order") and client_registry_checked(client.ip,client.hardware_id, client.emb_obj.uid):
            items = barOrder.items
            bar_id = barOrder.client.emb_obj.pk

            for item in items:
                product = item.product
                for inv in product.inventory:
                    if inv.bar.pk == bar_id:
                        #return HttpResponse(item.qty)
                        inv.qty = inv.qty + item.qty

                product.save()
            barOrder.delete()
            return HttpResponse("deleted")



@csrf_exempt
def TicketBookApi(request):
    if validate_posted_variables(request, "seats", "session_id", "cust_name","cust_surname"):
        # get posted variables
        seats = request.POST["seats"]
        session_id = request.POST["session_id"]
        cust_name = request.POST["cust_name"]
        cust_surname = request.POST["cust_surname"]
        cust_mobile = request.POST['cust_mobile']
        cust_email = request.POST['cust_email']
        # get django objects from database
        session = Session.objects.get(uid=session_id)
        #create order
        order = TicketOrder.objects.create()
        #create reservation
        pin = str(random.randint(1,9999)).zfill(4)
        reservation = Reservation.objects.create(name=cust_name,surname=cust_surname, mobile=cust_mobile, email=cust_email, pin=pin)
        order.reservation = reservation
        #create tickets for separate seat in posted variable
        ticket_list = []
        for seat_ in json.loads(seats):
            seat = Seat.objects.get(uid=seat_)
            #make sure ticket does not exist in db
            t = None
            for ticket in session.tickets:
                if ticket.seat.id == seat.id:
                        t = ticket
                        return HttpResponse("already exists")
            # if ticket does not exist : create ticket
            if not t:
                price = session.prices[seat.ttype]
                ticket = Ticket.objects.create(session=session, emb_seat=seat,seat=seat, process="RE", has_discount=False, price=price, date_printed=datetime.datetime.now(), date_sold=datetime.datetime.now(),ticket_order=order)
                ticket.save()
                ticket.uid = ticket.id
                ticket_list.append(ticket)
        #seats.append(seat)
        if ticket_list:
            session.tickets.extend(ticket_list)
            session.save()
        order.items = Ticket.objects.filter(ticket_order=order)
        order.save()
        return HttpResponse(ticket_list)
    else:
        return HttpResponse("variables not posted")

@csrf_exempt
def TicketBuyApi(request):
    if validate_posted_variables(request, "session_id", "seats"):
        # get posted variables
        seats = request.POST["seats"]
        session_id = request.POST["session_id"]

        # get django objects from database
        session = Session.objects.get(uid=session_id)
        #create order
        order = TicketOrder.objects.create()
        #create tickets for separate seat in posted variable
        ticket_list = []
        for seat_ in json.loads(seats):
            seat = Seat.objects.get(uid=seat_)
            #make sure ticket does not exist in db
            t = None
            for ticket in session.tickets:
                if ticket.seat.id == seat.id:
                        t = ticket
                        return HttpResponse("already exists")
            # if ticket does not exist : create ticket
            if not t:
                price = session.prices[seat.ttype]
                ticket = Ticket.objects.create(session=session, emb_seat=seat,seat=seat, process="SL", has_discount=False, price=price, date_printed=datetime.datetime.now(), date_sold=datetime.datetime.now(),ticket_order=order)
                ticket.save()
                ticket.uid = ticket.id
                ticket_list.append(ticket)
        #seats.append(seat)
        if ticket_list:
            session.tickets.extend(ticket_list)
            session.save()
        order.items = Ticket.objects.filter(ticket_order=order)
        order.save()
        return HttpResponse(ticket_list)
    else:
        return HttpResponse("variables not posted")

@csrf_exempt
def TicketBuyBookedApi(request):
    if validate_posted_variables(request, "session_id", "seats"):
        # get posted variables
        seats = request.POST["seats"]
        session_id = request.POST["session_id"]

        # get django objects from database
        session = Session.objects.get(uid=session_id)
        #create order

        order = TicketOrder.objects.create()
        #create tickets for separate seat in posted variable
        ticket_list = []
        for seat_ in json.loads(seats):
            seat = Seat.objects.get(uid=seat_)
            tickets = session.tickets
            for t in tickets:

                if t.process =="RE" and t.seat.uid==seat_:
                    #Session.objects.raw_update({}, {'$pull' : {'tickets' : {'uid' : t.uid}}})
                    #return HttpResponse("dss")
                    t.process = "SL"
                    ticket_list.append(t)
                    #session.tickets.extend([t])
                    session.save()
        return HttpResponse(ticket_list)

@ajax_request
@csrf_exempt
def getSessionsByDates(request):
    if validate_posted_variables(request,"from", "to", "hall_id"):
        hall_id = request.POST["hall_id"]
        from_ = request.POST["from"]
        to_ = request.POST["to"]
        from_split = from_.split("-")
        to_split = to_.split("-")
        from_date = datetime.date(int(from_split[0]),int(from_split[1]),int(from_split[2]))
        to_date = datetime.date(int(to_split[0]),int(to_split[1]),int(to_split[2]))
        hall = Hall.objects.get(name=hall_id)
        sessions = Session.objects.filter(hall=hall, start_time__gt=from_date, start_time__lt=to_date)
        sess_json = []
        for session in sessions:

            session.tickets = []
            session.emb_movie = json.loads(serializers.serialize("json",[Movie.objects.get(pk=session.movie.pk)]))
            sess_json.append(session)

        return HttpResponse(serializers.serialize("json", sess_json))

@csrf_exempt
def createSeats(request):
    if validate_posted_variables(request, "list"):
        l = json.loads(request.POST["list"])
        for obj in l:
            if not api.models.Seat.objects.filter(uid=obj["uid"]):
                hall = Hall.objects.get(name = obj["hall_name"])
                seat = testing.models.Seat(row=obj["row"])
                seat.seat=obj["seat"]
                seat.name=obj["name"]
                seat.uid=obj["uid"]
                seat.ttype=obj["ttype"]
                seat.top = obj["top"]
                seat.hall = hall
                seat.left = obj["left"]
                seat.width = obj["width"]
                seat.save()
            else:
                hall = Hall.objects.get(name = obj["hall_name"])
                seat = testing.models.Seat.objects.get(uid=obj["uid"])
                seat.row = obj["row"]
                seat.seat=obj["seat"]
                seat.name=obj["name"]
                seat.ttype=obj["ttype"]
                seat.top = obj["top"]
                seat.hall = hall
                seat.left = obj["left"]
                seat.width = obj["width"]
                seat.save()

        return HttpResponse("done")

@csrf_exempt
def loadSeats(request):
    if validate_posted_variables(request, "hall"):
        hall_name = request.POST["hall"]
        hall = Hall.objects.get(name=hall_name)
        seats = testing.models.Seat.objects.filter(hall=hall)

        return HttpResponse(serializers.serialize("json",seats))

@csrf_exempt
def removeSeats(request):
    if validate_posted_variables(request, "list"):
        l = json.loads(request.POST["list"])
        for uid in l:
            seat = testing.models.Seat.objects.get(uid=uid)
            seat.delete()


        return HttpResponse("removed")


@csrf_exempt
def swapTimeBetweenSessions(request):
    if validate_posted_variables(request,"array"):
        array = json.loads(request.POST["array"])
        l = list()

        for pair in array:
            for obj in pair:
                l.append(obj)

        unique = list(set(l))
        objects = {}
        for uni in unique:
            objects[uni] = Session.objects.get(uid=uni)

        for pair in array:
            uid_1 = pair[0]



            sess_1 = Session.objects.get(uid=uid_1)
            sess_2 = objects[pair[1]]


            sess_1.movie = sess_2.movie
            sess_1.emb_movie = sess_2.emb_movie
            sess_1.hall = sess_2.hall
            sess_1.tickets = sess_2.tickets
            sess_1.emb_tickets = sess_2.emb_tickets
            sess_1.duration = sess_2.duration
            sess_1.prices = sess_2.prices
            sess_1.m_name = sess_2.emb_movie

            sess_1.save()




        return HttpResponse("successfully swapped times")


@csrf_exempt
def createMovie(request):
    if validate_posted_variables(request, "m_name", "country", "duration", "poster_url"):
        m_name = request.POST["m_name"]
        country = request.POST["country"]
        duration = request.POST["duration"]
        poster_url = request.POST["poster_url"]

        movie = Movie(name=m_name, duration=duration, country=country, poster_url=poster_url).save()

        return HttpResponse("Movie Created")

def getFilmsForCinema(request, cinema_id):
    if 'cinema_id' in request.GET and request.GET['cinema_id']:
        cinema_id=request.POST['cinema_id']

    halls = Hall.objects.filter(theater=cinema_id)
    halls_array = []
    for hall in halls:
        halls_array.append(hall.id)

    time_from = datetime.datetime.now() - timedelta(minutes=440)
    
    sessions = Session.objects.filter(start_time__gt=time_from, hall__in=halls_array)
    films = []
    for session in sessions:
        if session.movie in films:
            pass
        else:
            films.append(session.movie)
    #films = sessions.movie.name#Movie.objects.filter(theater=cinema_id)
    jsonOfFilms=serializers.serialize("json", films)
    films_dict={}
    if 1==1:
        json_status="OK!"
    else:
        json_status="no data.."
    
#    return HttpResponse(jsonOfHalls)
    films_dict['status'] = json_status
    films_dict['data'] = jsonOfFilms
    return HttpResponse(json.dumps(films_dict))

def boxoffice_buy_product(request, hall, date, time):
    return HttpResponse("DING DONG")



@login_required
def seatsGenerator(request):
    halls = Hall.objects.all()
    return render(request, "hall_layout.html", {'halls': halls})

def test_admin(request):
    return render(request,"test_admin.html",{})

def deleteSessions(request):
    if validate_posted_variables(request, "session_ids"):
        session_ids = json.loads(request.POST["session_ids"])
        report = []
        for session_id in session_ids:
            d = {}
            session = Session.objects.get(uid=session_id)

            d["session"]= session.uid
            if not len(session.tickets)>0:
                session.delete()
                d["status"] = "deleted"
            else:
                d["status"] = "Has orders. Not deleted."
            report.append(d)
        return HttpResponse(json.dumps(report))

def updateSession(request):
    if validate_posted_variables(request,"uid" ,"day", "month", "year", "hour", "minute","hall_id","hall_name","movie_id","duration","prices","session_language","is_3d"):
        session_id = request.POST["uid"]
        #time variables
        year = int(request.POST["year"])
        month = int(request.POST["month"])
        day = int(request.POST["day"])
        hour = int(request.POST["hour"])
        minute = int(request.POST["minute"])

        start_time = datetime.datetime(year, month, day, hour, minute)

        duration = int(request.POST["duration"])
        hall_id = request.POST["hall_id"]
        hall_name = request.POST["hall_name"]
        movie_id = request.POST["movie_id"]
        session_language = request.POST['session_language']
        is_3d_java = request.POST['is_3d']
        if is_3d_java == 'false':
            is_3d = False
        else:
            is_3d = True

        prices = json.loads(request.POST["prices"])
        session = Session.objects.filter(uid=session_id)

        d = {"session":session_id}
        if session:
            session = session[0]
            uid_rec = "%s/%s-%s-%s/%s:%s" % (hall_name, start_time.strftime("%d"),start_time.strftime("%m"), year, start_time.strftime("%H"), start_time.strftime("%M"))

            if not session_id==uid_rec:
                d["status"] = "error - uid does not coincide with start_time"
                return HttpResponse(json.dumps(d))

            hall = Hall.objects.get(pk=hall_id)
            movie = Movie.objects.get(pk=movie_id)
            session.hall = hall
            session.movie = movie
            session.prices  = prices
            session.duration = duration
            session.m_name = movie.name
            session.session_language = session_language
            session.is_3d = is_3d
            session.save()
            d["status"] = "updated"
            return HttpResponse(json.dumps(d))
        else:
            hall = Hall.objects.get(pk=hall_id)
            movie = Movie.objects.get(pk=movie_id)
            Session(start_time=start_time, duration=duration, hall=hall, movie=movie, prices=prices,m_name=movie.name, session_language=session_language, is_3d=is_3d).save()
            d["status"] = "created"
            return HttpResponse(json.dumps(d))