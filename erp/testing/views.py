# -*- coding: utf-8 -*-
from django.views.decorators.csrf import csrf_exempt
import random
import django_socketio
from jsonify.decorators import ajax_request
from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail, EmailMessage, EmailMultiAlternatives
from email.MIMEImage import MIMEImage
from django.template import Context
from django.template.loader import get_template
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render
import hashlib, uuid
import testing
from testing.models import *
from api.models import *
#from api.models import ClientRegistry
import os
import api
import qrcode
#from PIL import Image

from django.core.serializers.json import DjangoJSONEncoder
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
import datetime
import json
from django.http.response import HttpResponseRedirect, HttpResponse
from decimal import Decimal
from django.core import serializers
from django.conf import settings
from pip._vendor.requests.sessions import session
from django_socketio import events
import dateutil.parser
from datetime import timedelta
from django.db.models import Q
from django.contrib.auth import authenticate, login, logout
import django
from bson.objectid import ObjectId
from bson.timestamp import Timestamp
from bson import BSON
from bson import json_util
import re
from django.db.models import Sum, Count
import urllib
import urllib2
import pycurl
import cStringIO
import shlex
from socket import *
import socket
from testing.PassbookUtil.passGenerator import PassGenerator
# import the logging library
import logging




#TEMPORARY, REMOVE PART
from pymongo import *
#import pymongo
from bson.objectid import ObjectId
from erp.settings import DATABASES

from django.views.decorators.csrf import csrf_exempt
import random
import testing
import django_socketio
from jsonify.decorators import ajax_request
from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail, EmailMessage
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render

from testing.models import *
from api.models import *
#from api.models import ClientRegistry

from django.core.serializers.json import DjangoJSONEncoder
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
import datetime
import json
from django.http.response import HttpResponseRedirect, HttpResponse
from decimal import Decimal
from django.core import serializers
from django.conf import settings
from pip._vendor.requests.sessions import session
from django_socketio import events
import dateutil.parser
from dateutil import parser
from datetime import timedelta
from django.contrib.auth.models import User
from django.contrib.auth.backends import ModelBackend
import sys

#TEMPORARY END

# Get an instance of a logger
logger = logging.getLogger(__name__)

def test_log(request):
    #logger.log("Dingo")
    #logger.info("Dingo")
    print >>sys.stderr, 'Goodbye, cruel world!'
    return HttpResponse("Success")


@events.on_message(channel="^session")
def message(request, socket, context, message):
    #socket.broadcast(context)
    #    socket.broadcast(message)
    #if(message[""])
    if(message["command"]=="remove_blocked"):
        ch_name = message["channel"]
        session = Session.objects.get(uid=message["session"])
        bt = BlockedTicket.objects.filter(session=session)
        for b in bt:
            if b.seat.name == message["id"]:
                b.delete()
                socket.send_and_broadcast_channel(message, channel=ch_name)


@login_required
def test_post_url(request):
  return HttpResponse("success")

def register_user_cavea(request):

    username = request.POST["username"]
    user_email = request.POST["user_email"]
    first_name = request.POST["first_name"]
    last_name = request.POST["last_name"]
    password = request.POST["password"]
    phone = request.POST["telephone"]
    #return HttpResponse(phone);

    user = User(username = username,first_name = first_name)
    user.last_name = last_name
    user.email = user_email
    user.set_password(password)
    if len(User.objects.filter(email=user_email)):
      return HttpResponse("Fail:user exists")
    else:
      user.save()
      return HttpResponse("success")


def change_password_user_cavea(request):
  model = ModelBackend()
  username = request.POST["username"]
  password = request.POST["password"]
  new_password = request.POST["new_password"]
  try:
    user = model.authenticate(username=username, password=password)
    user.set_password(new_password)
    user.save()
  except:
    return HttpResponse("user not found or duplicate entry")
  return HttpResponse("password changed successfully")

@login_required
def home(request):
    employee = Employee.objects.all()
    #for invoice in invoices:
        #print invoice.emission_date
    return render(request, "base_extended.html", {'employee':employee})

def logout_user(request):
    auth.logout(request)

    return HttpResponseRedirect("/accounts/login/")

def account_login(request):
    path = request.GET.get("next")
    if path == None:
        path = "/search/"
    return render(request, "registration/login.html", {'next': path})


def register_shift(username):
    registryShiftId = get_registry_by_username(username)
    user=username
    #Shift.objects.create()
    pass



def get_registry_by_username(username):
    if username:
        pass
    pass
      #  registry = ClientRegistry.objects.get(uid=username)._id
  #  return registry._id


def login_user(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    next=request.POST.get('next')
    token = request.POST.get('csrfmiddlewaretoken')

    user = auth.authenticate(username=username, password=password)

    if user is not None and user.is_active:
        user_ip=request.META.get('REMOTE_ADDR')
        hard_id='fake_Id'

        create_token(username, hard_id, user_ip)
        auth.login(request, user)
        if request.POST['source'] == 'cavea.ge':
            token = django.middleware.csrf.get_token(request)
            Shift.objects.create(agent="554cebb6956167b76e9f1e09", csrftoken=token,user=username).save()
            request.session["csrf_token"] = token

        return HttpResponseRedirect(next)
    else:
        return HttpResponseRedirect("/cavea/login/?next="+next)

#@ensure_csrf_cookie
def post(request):
    if 'data' in request.POST and request.POST['data']:
        data = request.POST["data"]
        employees = Employee.objects.filter(name__istartswith=data)
        html =["<ul>"]
        for  employee in employees:
            html.append("<li class='fill_li' id='' onclick='fill_input(event)'>"+employee.surname+"</li>")

        html.append("</ul>")
        post = "".join(html)
        #post_json = json.dumps(post)
        return HttpResponse(post)

@login_required
def search(request):
    if 'name' in request.POST and request.POST['name']:
        name = request.POST['name']
        employee = Employee.objects.get(name=name)
        if not company:
            error = "no company found with this name!"
            return render(request, "search.html", {'error': error})
        employee_id = company.id
        return render(request, "search.html", {'surname':surname})
    name = "nothing"
    return render(request, "search.html", {'name': name})


@login_required
def hall(request):
    if 'name' in request.GET and request.GET['name']:
        name = request.GET['name']
        hall = Hall.objects.get(name=name)
        rows = []
        for row in hall.rows:
          seats = testing.models.Seat.objects.filter(hall=hall, row=row).order_by("id")
          #dict = {"name": row, "seats":seats}
          dict = {"name":row, "seats": serializers.serialize("json", seats)}
          rows.append(dict)
        #rows = Row.objects.filter(hall_id=hall.id).order_by("-id")
        if not hall or not rows:
            error = "no hall found with this name!"
            return render(request, "hall_.html", {'error': error})
        #return render(request, "hall_.html", {'hall':hall, "rows":rows})
        return HttpResponse(json.dumps(rows))
    name = "nothing"
    return render(request, "hall_.html", {'hall': hall, "rows":rows})

def parse_order_for_web(orderID):
    img = qrcode.make(orderID)
    qrCodePath = "/home/ubuntu/rustaveli/erp/media/ticketorders/qrCode%s.png" % orderID
    img.save(qrCodePath, "PNG")
    qrCodePath = "https://www.cinerp.com:9000/media/ticketorders/qrCode%s.png" % orderID
    ticketsArray = []
    order = TicketOrder.objects.get(id=orderID)
    sessionObject = order.session
    language = sessionObject.session_language
    movie = sessionObject.movie.name
    if len(movie) > 10:
        movie = movie[:9]
    start_datetime = sessionObject.start_time.strftime('%d-%b-%Y %H:%M')
    date = start_datetime[:11]
    time = start_datetime[12:5]
    datetimefull = '%s  %s' % (date, time)
    hall = sessionObject.hall.title
    cinema = sessionObject.hall.theater.name
    for ticket in order.items:
        price = ticket.price_paid
        row = ticket.emb_seat.row
        seat = ticket.emb_seat.seat
        dictionary = {"cinemaName":sessionObject.hall.theater.name}
        dictionary.update({"qrCodePath":qrCodePath, "cinemaLogo":"https://www.cinerp.com:9000/media/ticketorders/logo.png"})
        dictionary.update({"film":movie, "encodedauditorium":hall})
        dictionary.update({"row":row, "seat":seat, "price":price, "discountStr":orderID, "date":datetimefull, "time":start_datetime, "language":language})
        ticketsArray.append(dictionary)
    return ticketsArray


def parse_order_for_webmail(orderID):
    img = qrcode.make(orderID)
    qrCodePath = "/home/ubuntu/rustaveli/erp/testing/WebTicketUtil/qrCode%s.png" % orderID
    img.save(qrCodePath, "PNG")
    ticketsArray = []
    order = TicketOrder.objects.get(id=orderID)
    sessionObject = order.session
    movie = sessionObject.movie.name
    if len(movie) > 10:
        movie = movie[:9]
    start_datetime = sessionObject.start_time.strftime('%d-%b-%Y %H:%M')
    date = start_datetime[:11]
    time = start_datetime[12:5]
    datetimefull = '%s  %s' % (date, time)
    hall = sessionObject.hall.title
    cinema = sessionObject.hall.theater.name
    for ticket in order.items:
        price = ticket.price_paid
        row = ticket.emb_seat.row
        seat = ticket.emb_seat.seat
        dictionary = {"cinemaName":sessionObject.hall.theater.name}
        dictionary.update({"qrCodePath":qrCodePath, "cinemaLogo":"/home/ubuntu/rustaveli/erp/testing/WebTicketUtil/logo.png"})
        dictionary.update({"film":movie, "encodedauditorium":hall})
        dictionary.update({"row":row, "seat":seat, "price":price, "discountStr":orderID, "date":datetimefull})
        ticketsArray.append(dictionary)
    return ticketsArray


def parse_order(orderID):
    urls_array = []
    order = TicketOrder.objects.get(id=orderID)
    sessionObject = order.session
    movie = sessionObject.movie.name
    if len(movie) > 10:
        movie = movie[:9]
    start_datetime = sessionObject.start_time.strftime('%d-%b-%Y %H:%M')
    date = start_datetime[:11]
    time = start_datetime[12:5]
    hall = sessionObject.hall.title
    cinema = sessionObject.hall.theater.name
    for ticket in order.items:
        price = ticket.price_paid
        row = ticket.emb_seat.row
        seat = ticket.emb_seat.seat
        url = generate_ticket_urls(cinema, hall, row, seat, date, time, movie, price, orderID)
        urls_array.append(url)
    return urls_array

def send_weborder_to_user(toEmail, orderID):
    try:
        tickets =  parse_order_for_webmail(orderID)
        template = get_template('email_ticket.html')

        context = Context({"tickets":tickets })
        content = template.render(context)
        recipient = toEmail

        msg = EmailMessage('DO-NOT-REPLY', content, "tickets@cinerp.com", to=[recipient,],headers={
        'Content-Type': 'text/html; charset=UTF-8',
        'Content-Transfer-Encoding': 'base64',
        'MIME-Version': '1.0'
        })
        msg.mixed_subtype = 'related'
        for ticket in tickets:
            for f in [ticket["qrCodePath"], ticket['cinemaLogo']]:
                fp = open(os.path.join(os.path.dirname(__file__), f), 'rb')
                msg_img = MIMEImage(fp.read())
                fp.close()
                msg_img.add_header('Content-ID', '<{}>'.format(f))
                msg.attach(msg_img)
        msg.content_subtype = "html"
        msg.send()
    except Exception, e:
        return HttpResponse(str(e))


    return HttpResponse("Sent")


def generate_ticket_urls(cinemaName, hallName, row, seat, date, time, movie, price, orderID):
    generator = PassGenerator()
    rowandseat = u'რიგი %s ადგილი %s' % (row,seat)
    rowandseat = rowandseat.encode('utf-8')
    ticketURL = generator.generatePass(cinemaName, hallName, date, movie, rowandseat, orderID)
    ticketURL = '/home/ubuntu/rustaveli/erp/testing/PassbookUtil/Tickets/%s' % (ticketURL)
    return ticketURL

def sendemail(subject, message, sender, receiver):
    send_mail(subject, message, sender, receiver)
    # ('Subject here', 'Here is the message.', 'from@example.com', ['to@example.com'], fail_silently=False)
    return HttpResponse(["Sent"])

def sendmail_withalternative():
    subject, from_email, to = 'hello', 'django@cinerp.com', 'bakur@me.com'
    text_content = 'This is an important message.'
    html_content = '<p>This is an <strong>important</strong> message.</p>'
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

def sendmail_withattachment(request):
    send_ticket_to_user(request,'bakur@me.com','DO-NOT-REPLY','Your Ticket is Here', '554105719561677fc592485a')
    return HttpResponse("Done")
    email = EmailMessage('Hello', 'Body goes here', 'django@cinerp.com',
            ['bakur@me.com'], ['tsiklauri@hotmail.com'],
            headers = {'Reply-To': 'django@cinerp.com'})
    email.attach_file('/home/ubuntu/rustaveli/erp/media/product/images/base/dingdong.jpg')
    email.send(fail_silently=False)
    #['bakur@me.com', 'to2@example.com'], ['bcc@example.com'],
    #email.attach('design.png', img_data, 'image/png')
    #email.attach_file('/images/weather_map.png')

@csrf_exempt
def send_ticket_to_user_external(request):
    toEmail = request.POST['toemail']
    subject = request.POST['subject']
    message = request.POST['message']
    orderID = request.POST['orderID']
    email = EmailMessage(subject, message, 'tickets@cinerp.com',
            [toEmail],
            headers = {'Reply-To': 'tickets@cinerp.com'})
    urlsArray = parse_order(orderID)
    for url in urlsArray:
        email.attach_file(url)
    email.send(fail_silently=False)
    return HttpResponse("Done")

def send_ticket_to_user(request, toEmail, subject, message, orderID):
    email = EmailMessage(subject, message, 'tickets@cinerp.com',
            [toEmail],
            headers = {'Reply-To': 'tickets@cinerp.com'})
    urlsArray = parse_order(orderID)
    for url in urlsArray:
        email.attach_file(url)
    email.send(fail_silently=False)
    return HttpResponse("Done")

# def send_webticket_to_user(request):
#     return send_weborder_to_user('bakur@me.com', '5543c0979561677c14a875b4')
@csrf_exempt
def show_weborder_external(request, order_id):
    tickets = parse_order_for_web(order_id)
    template = get_template('web_ticket.html')

    context = Context({"tickets":tickets })
    content = template.render(context)
    return HttpResponse(content)


def show_weborder_fororder(orderID):
    tickets = parse_order_for_web(orderID)
    template = get_template('web_ticket.html')

    context = Context({"tickets":tickets })
    content = template.render(context)
    return HttpResponse(content)

@csrf_exempt
def sent_weborder_to_external(request):
    toEmail = request.POST['to_email']
    orderID = request.POST['order_id']
    return HttpResponse(send_weborder_to_user(toEmail, orderID))

#@csrf_exempt
def register_customer(request):
    name, surname, phone, email, national_id = request.POST['name'],request.POST['surname'],request.POST['phone'],request.POST['email'],request.POST['national_id']
    customer = Customer.objects.filter(name=name,surname=surname,phone=phone)
    if len(customer) > 0:
        return HttpResponse("Customer With Name Surname and Phone Exists")
    else:
        customer = Customer.objects.create(name=name, surname=surname, phone=phone, email=email, national_id=national_id)
        customer.save()
        return HttpResponse("Success")

#@csrf_exempt
def create_giftcard_with_customer_id(request):
    customer_id = request.POST['customer_id']
    face_value = request.POST['face_value']
    remaining_balance = float(face_value)
    #card_code
    #expiresAt
    customer = None
    try:
        customer = Customer.objects.get(id=customer_id)
        if not customer:
            return HttpResponse(["Customer Not Found"])
    except:
        return HttpResponse(["Customer Not Found"])
    giftcard = GiftCard.objects.create(customer=customer,face_value=float(face_value), remaining_balance=remaining_balance)
    giftcard.save()
    return HttpResponse(["Success"])


@csrf_exempt
def find_giftcard(request):

    name = request.POST['name']
    surname = request.POST['surname']
    phone = request.POST['phone']
    email = request.POST['email']
    national_id = request.POST['national_id']
    card_code = request.POST['card_code']

    filters = {}

    if card_code != 'ALL':
        filters.update({"card_code":card_code})
    if email != 'ALL':
        regx = re.compile("^%s"%email, re.IGNORECASE)
        filters.update({"customer.email":regx})
    if phone != 'ALL':
        regx = re.compile("^%s"%phone, re.IGNORECASE)
        filters.update({"customer.phone":regx})
    if surname != 'ALL':
        regx = re.compile("^%s"%surname, re.IGNORECASE)
        filters.update({"customer.surname":regx})
    if name != 'ALL':
        regx = re.compile("^%s"%name, re.IGNORECASE)
        filters.update({"customer.name":regx})
    if national_id != 'ALL':
        regx = re.compile("^%s"%national_id, re.IGNORECASE)
        filters.update({"customer.national_id":regx})
    #return HttpResponse([filters])

    giftCards = None
    if filters == None:
        giftCards = GiftCard.objects.all()
    else:
        giftCards = GiftCard.objects.raw_query(filters)
    #return HttpResponse(serializers.serialize("json",ticketReservations))

    response = {"status":"ok", "data": "NA"}
    if giftCards:
        for giftCard in giftCards:
            giftCard.customer = json.loads(serializers.serialize("json", [giftCard.customer]))
        if len(giftCards) > 0:
            response["data"] = serializers.serialize("json", giftCards)
    return HttpResponse(json.dumps(response))





def session_hall(request, hall):
    hall = Hall.objects.get(name=hall)
    sessions = Session.objects.filter(hall=hall).order_by("-start_time")
    return render(request, "session_list.html",{"sessions":sessions, "hall":hall})

def session(request, hall,date,time):
    sess_name ="%s/%s/%s" % (hall, date,time)
    var = True
    if hall and date and time:
        ip = request.META.get('REMOTE_ADDR')
        session = Session.objects.get(uid=sess_name)
        prices = session.prices


        hall = session.hall
        time = datetime.datetime.now()

        date_ = "%s-%s-%s" % (session.start_time.strftime("%d"),session.start_time.strftime("%m"),session.start_time.year)
        time = "%s:%s" % (session.start_time.hour,session.start_time.strftime("%M"))
        seat_types = session.prices
        seats = api.models.Seat.objects.filter(hall=hall)
        return render(request, "session_abs.html", {'hall':hall,  "time":time, "session":session, "prices":prices,"ip":ip,"const":settings.CONST, "date":date_,"time":time, "seat_types":seat_types, "username":request.user.email, "seats": seats})

def session_OLD(request, hall,date,time):
    sess_name ="%s/%s/%s" % (hall, date,time)
    #if 'sess' in request.GET and request.GET['sess']:
    var= True
    if hall and date and time:
        ip = request.META.get('REMOTE_ADDR')
        session = Session.objects.get(uid=sess_name)
        prices = session.prices


        hall = session.hall
        time = datetime.datetime.now()

        date_ = "%s-%s-%s" % (session.start_time.strftime("%d"),session.start_time.strftime("%m"),session.start_time.year)
        time = "%s:%s" % (session.start_time.hour,session.start_time.strftime("%M"))



        #start_time = {"year":year, "month":month, "day":day, "hour":hour,"min":min}
        seat_types = session.prices
        rows = []
        for row in hall.rows:
          seats = testing.models.Seat.objects.filter(hall=hall, row=row).order_by("id")
          dict = {"name": row, "seats":seats}
          rows.append(dict)


        #rows = Row.objects.filter(hall_id=hall.id).order_by("-id")

        if not hall or not rows:
            error = "no hall found with this name!"
            return render(request, "session.html", {'error': error})
        return render(request, "session.html", {'hall':hall, "rows":rows, "time":time, "session":session, "prices":prices,"ip":ip,"const":settings.CONST, "date":date_,"time":time, "seat_types":seat_types, "username":request.user.email})
    elif hall:
        return HttpResponse("smth")
    name = "nothing"
    return render(request, "session.html", {'hall': hall, "rows":rows, "time":time})


def get_blocked(request):
    if 'sess' in request.POST and request.POST['sess']:
        sess_id = request.POST['sess']
        session = Session.objects.get(uid=sess_id)
        btickets = BlockedTicket.objects.filter(session=session)
        seats = []
        for ticket in btickets:
            ticket.seat.status = "blocked"
            seats.append(ticket.seat)
        return HttpResponse(serializers.serialize("json",seats))
        #return HttpResponse(serializers.serialize("json", btickets))


def block_ticket(request):
    if 'data' in request.POST and request.POST['data']:
        id = request.POST['data']
        sess_id = request.POST['sess']
        session = Session.objects.get(uid=sess_id)
        seat = testing.models.testing.models.Seat.objects.get(uid = id)
        ticket = BlockedTicket.objects.create(session=session)
        ticket.seat=seat
        ticket.has_discount=False
        ticket.price=0.00
        ticket.date_printed="2015-01-12"
        ticket.date_sold=datetime.datetime.now()
        ticket.save()
        return HttpResponse(ticket.seat.name)


def test_post(request):
    if 'data' in request.POST and request.POST['data']:
        arr = json.loads(request.POST['data'])
        uid = request.POST['sess']
        process = request.POST['process']
        session = Session.objects.get(uid=uid)
        seats = []
        for ticket in arr:
            seat = testing.models.Seat.objects.get(uid=ticket)
            ticket = Ticket.objects.create(session=session, seat=seat,process=process, has_discount=False,
                                           price=0.00, date_printed="2015-01-12", date_sold=datetime.datetime.now())
            ticket.save()


def remove_ticket(request):
    if 'data' in request.GET and request.GET['data']:
        id = request.GET['data']
        id = "54c2318b0bc7cb1a87e942d4"
        obj = "ObjectId(\"%s\")" % id
        query = "{$pull:{tickets:{id:ObjectId(\"%s)}}" % id
        #return HttpResponse(obj)
        #query = "{'_id':ObjectId('54c2318b0bc7cb1a87e942d4')}"

        data = Session.objects.raw_update({}, {'$pull' : {'tickets' : {'uid' : r"54c25c080bc7cb316a3ed507"}}})

        return HttpResponse(data)



def getOrderByPinCavea(request):

    if 'pin' in request.POST:

        pin = request.POST["pin"]
        order = TicketOrder.objects.raw_query({"reservation.pin":pin})

	seats = []
        for item in order[0].items:
            seat = testing.models.Seat.objects.get(pk=item.seat.pk)
            seats.append(seat)
        seats_json = serializers.serialize("json", seats)


        #order_json = serializers.serialize("json", [order[0]])
        return HttpResponse(json.dumps({"seats": json.loads(seats_json), "order": {"cust_name": order[0].reservation.name, "cust_surname":order[0].reservation.surname, "cust_email": order[0].reservation.email , "cust_mobile":order[0].reservation.mobile,"session_id":order[0].session_id}, "prices":order[0].session.prices}))
        #return {"data":json.loads(seats_json)}







def remove_reservation(request, hall, date, time):
    if 'data' in request.POST and request.POST['data']:

        sess_name ="%s/%s/%s" % (hall, date,time)
        tickets=Session.objects.get(uid=sess_name).tickets


        for ticket in tickets:
            if ticket.process=='RE':
                uid=ticket.uid
                Session.objects.raw_update({}, {'$pull' : {'tickets' : {'uid' : ticket.uid}}})


        return




'''
chamouaro kvela sessias romelic aris =  CurrentTime - 10 tsuti
@returns session list


params- rangeTimeParam, (gets from config settings.py)
'''




def ticket_order(request):
    if 'data' in request.POST and request.POST['data']:
        arr = json.loads(request.POST['data'])
        uid = request.POST['sess']
        cust_name = request.POST['cust_name']
        cust_surname = request.POST['cust_surname']
        cust_mobile = request.POST['cust_mobile']
        cust_email = request.POST['cust_email']


        process = request.POST['process']

        order = TicketOrder.objects.create()
        pin = str(random.randint(1,9999)).zfill(4)
        if process=="RE":
            reservation = Reservation.objects.create(name=cust_name,surname=cust_surname, mobile=cust_mobile, email=cust_email, pin=pin)
            order.reservation = reservation

        if process=="SL":
            agent = Agent.objects.all()[0]
            order.agent = agent

        ticket_list = []

        if process=="RE" or process=="SL":
            session = Session.objects.get(uid=uid)
            order.session = session
            tickets =[]
            seats = []
            for seat_ in arr:
                seat = testing.models.testing.models.Seat.objects.get(uid=seat_)
                #t = Ticket.objects.filter(session=session, seat=seat)
                t = None
                for ticket in session.tickets:
                    if ticket.seat.uid == seat.uid:
                        t = ticket

                if not t:
                    seat = testing.models.testing.models.Seat.objects.get(uid=seat_)
                    ticket = Ticket.objects.create(session=session, emb_seat=seat,seat=seat, process=process, has_discount=False, price=0.00, date_printed="2015-01-12", date_sold=datetime.datetime.now(),ticket_order=order)

                    ticket.save()
                    ticket.uid = ticket.id
                    ticket_list.append(ticket)


                    #seats.append(seat)

            session.tickets.extend(ticket_list)
            session.save()

            order.items = Ticket.objects.filter(ticket_order=order)
            order.save()

        return HttpResponse(serializers.serialize("json",seats))

    if 'rs' in request.POST and request.POST['rs']:
        arr = json.loads(request.POST['rs'])
        uid = request.POST['sess']
        session = Session.objects.get(uid=uid)
        seats = []
        for ticket in arr:
            seat = testing.models.Seat.objects.get(uid=ticket)
            tickets = session.tickets
            for t in tickets:
                if t.process =="RE" and t.seat.uid==ticket:
                    Session.objects.raw_update({}, {'$pull' : {'tickets' : {'uid' : t.uid}}})
                    t.process = "SL"
                    session.tickets.extend([t])
                    session.save()

                    username = None
                    if request.user.is_authenticated():
                        username = request.user.email
                    else:
                        username = "giorgiare"
                    val = Validation.objects.create(ticket=t, user=username,status="valid")
                    val.save()
                    imagepath= "qrcode/%s.jpg" % val.id
                    link = "%s/validation/%s" % (settings.LOCAL_IP, val.id)
                    qrcode = QRCode.objects.create(ticket=t, imagepath=imagepath,link=link)
                    qrcode.save()
                    val.qrcode = qrcode;
                    val.save()
                #else:
                 #   return HttpResponse("fail")
        return HttpResponse(serializers.serialize("json",seats))

def refresh_seats(request):
    if 'sess' in request.POST and request.POST['sess']:
        pass

def ticket(request):
    if 'sess' in request.POST and request.POST['sess']:
        sess_id = request.POST['sess']
        session = Session.objects.get(uid=sess_id)
        #tickets = Ticket.objects.filter(session=session)
        tickets = session.tickets
        seats = []
        for ticket in tickets:
            ticket.seat.status = ticket.process
            seats.append(ticket.seat)
        return HttpResponse(serializers.serialize("json",seats))




def ticketSales(request,hall, date,time):
    sess_name ="%s/%s/%s" % (hall, date,time)
    session = Session.objects.get(uid=sess_name)
    hall = session.hall

    tickets = session.tickets
    number_of_eco = len(testing.models.Seat.objects.filter(hall=hall,ttype="eco"))
    number_of_std = len(testing.models.Seat.objects.filter(hall=hall,ttype="std"))
    number_of_sta = len(testing.models.Seat.objects.filter(hall=hall,ttype="sta"))
    number_of_vip = len(testing.models.Seat.objects.filter(hall=hall,ttype="vip"))

    number_of_reserved = 0
    number_of_sold = 0

    occupied_eco = 0
    occupied_std = 0
    occupied_sta = 0
    occupied_vip = 0

    for ticket in tickets:
        if ticket.seat.ttype == "eco":
            occupied_eco += 1
        if ticket.seat.ttype == "std":
            occupied_std += 1
        if ticket.seat.ttype == "sta":
            occupied_sta += 1
        if ticket.seat.ttype == "vip":
            occupied_vip += 1
        if ticket.process =="RE":
            number_of_reserved +=1
        if ticket.process =="SL":
            number_of_sold +=1

    ototal = number_of_reserved+number_of_sold
    total =   number_of_eco-occupied_eco+  number_of_std-occupied_std+number_of_sta-occupied_sta+number_of_vip-occupied_vip
    dict = {"eco": (number_of_eco-occupied_eco),"std":number_of_std-occupied_std,
                "sta":number_of_sta-occupied_sta,"vip":number_of_vip-occupied_vip,
                "total":total, "reserved":number_of_reserved,
                "sold":number_of_sold, "ototal":ototal}

    if not hall:
        error = "no hall found with this name!"
        return render(request, "test.html", {'error': error})

    return HttpResponse(json.dumps(dict))

def validation(request, ticket):
    val = Validation.objects.get(id=ticket)

    if val:
        if val.status =="used":
            return HttpResponse("<html><head></head><body style='background-color: red;'></body></html>")
        val.status = "used"
        val.save()
        return HttpResponse("<html><head></head><body style='background-color: green;'></body></html>")
    else:
        return HttpResponse("<html><head></head><body style='background-color: red;'></body></html>")

def test(request):

        return render(request, "test.html", {"seat":"seat"})
        #return HttpResponse(seat))

def session_add(request):
    movies = Movie.objects.all()
    halls = Hall.objects.all().order_by("title")
    return render(request, "session_add.html", {"movies":movies, "halls":halls})


def check_digits(obj):
     return obj

def validate_session_duration(session):
    sess = session;
    hall = sess.hall;
    #check session before
    time_from = sess.start_time - timedelta(minutes=440)
    time_to =  sess.start_time-timedelta(minutes=10);
    sessions = Session.objects.filter(start_time__gt=time_from, start_time__lt=time_to, hall=hall).order_by("-start_time")
    if sessions:
        sessions = sessions[0]
        if sessions.start_time+timedelta(minutes=sessions.duration) > sess.start_time:
            t = sessions.start_time+timedelta(minutes=sessions.duration)
            return "%s: The session that starts at %s lasts %s minutes. Session must start at least at %s." % (hall.title,sessions.start_time.strftime('%H:%M'),sessions.duration, t.strftime('%H:%M'))


       #check after session
    time_from = sess.start_time
    time_to =  sess.start_time+timedelta(minutes=sess.duration)
    sessions = Session.objects.filter(start_time__gt=time_from, start_time__lt=time_to, hall=hall).order_by("start_time")
    if sessions:
        diff = (sessions[0].start_time-sess.start_time).seconds/60

        return "%s: There is a session at %s. The duration of the session must be %s minutes." % (hall.title,sessions[0].start_time.strftime('%H:%M'),diff)

    return False

@csrf_exempt
def create_session(request):
     if validate_posted_variables(request, "hall_id", "movie_id", "day", "month", "year", "hour", "minute", "session_language","m_name", "duration", "prices", 'is_3d'):
        hall_id = request.POST["hall_id"]
        movie_id = request.POST["movie_id"]
        #start_time = request.POST["start_time"]
        day = int(request.POST["day"])
        month =  int(request.POST["month"])
        year = int(request.POST["year"])
        hour = int(request.POST["hour"])
        minute = int(request.POST["minute"])
        m_name = request.POST["m_name"]
        stime = "%s/%s/%s %s:%s" % (day, month,year, hour, minute)
        start_time =  datetime.datetime(year,month, day, hour, minute)
        hall = Hall.objects.get(id=hall_id)
        movie = Movie.objects.get(id=movie_id)
        duration = int(request.POST["duration"])
        prices = json.loads(request.POST["prices"])
        session_language = request.POST['session_language']
        is_3d_java = request.POST['is_3d']
        if is_3d_java == 'false':
            is_3d = False
        else:
            is_3d = True
        sess_s = Session.objects.filter(hall=hall, start_time=start_time)
        sessions = []

        session = Session(hall=hall, movie=movie,m_name=m_name,duration=duration, start_time=start_time, prices=prices, session_language = session_language, is_3d=is_3d)

        #validate session durations
        hall = session.hall;
        #return HttpResponse(session.start_time);
        #check session before
        time_from = session.start_time - timedelta(minutes=440)
        time_to =  session.start_time-timedelta(minutes=10);
        ses = Session.objects.filter(start_time__gt=time_from, start_time__lt=time_to, hall=hall).order_by("-start_time")
        flag = True
        text = ""
        if ses:
            ses = ses[0]
            if ses.start_time+timedelta(minutes=ses.duration) > session.start_time:
                t = ses.start_time+timedelta(minutes=ses.duration)
                flag = False
                return HttpResponse("%s: The session that starts at %s lasts %s minutes. Session must start at least at %s." % (hall.title,ses.start_time.strftime('%H:%M'),ses.duration, t.strftime('%H:%M')))


        #check after session
        time_from = session.start_time
        time_to =  session.start_time+timedelta(minutes=session.duration)
        ses = Session.objects.filter(start_time__gt=time_from, start_time__lt=time_to, hall=hall).order_by("start_time")
        if ses:
            diff = (ses[0].start_time-session.start_time).seconds/60
            flag = False
            return HttpResponse("%s: There is a session at %s. The duration of the session must be %s minutes." % (hall.title,ses[0].start_time.strftime('%H:%M'),diff))

        if not sess_s and flag:
            session.save()
            return HttpResponse("sessions created successfully")
        else:
            return HttpResponse("not successful")
        #return HttpResponse(serializers.serialize("json", sessions))
        #testComment
        #anotherTestComment

def SaveConfig(request):

    username = request.user.email
    data = json.loads(request.POST["data"])
    hall_name = request.POST["hall"]
    config = DesktopConfig.objects.filter(username=username, hall=hall_name)
    config_list = []
    if not config:
        config = DesktopConfig.objects.create(username=username, hall=hall_name)

        config.dic = data
        config.save()
        return HttpResponse(config)
    else:

        config[0].dic = data
        config[0].save()
        return HttpResponse(data)


def GetConfig(request):
    username = request.user.email
    hall_name = request.POST["hall"]
    config = DesktopConfig.objects.filter(username=username, hall = hall_name)
    if config:
        return HttpResponse(json.dumps(config[0].dic))
    return HttpResponse(json.dumps(config[0].dic))

@login_required
def create_session_set(request):
    log_request(request)
    movies = Movie.objects.filter(is_active=True)
    halls = Hall.objects.all().order_by("title")
    return render(request, "create_session_set.html", {"movies":movies, "halls": halls})

def getAllFilms(request):

    halls = Hall.objects.all()
    halls_array = []
    for hall in halls:
        halls_array.append(hall.id)

    time_from = datetime.datetime.now() - timedelta(minutes=440)

    sessions = Session.objects.filter(start_time__gt=time_from, hall__in=halls_array)
    films = []
    for session in sessions:
        if session.movie in films:
            pass
        else:
            films.append(session.movie)
    #films = sessions.movie.name#Movie.objects.filter(theater=cinema_id)
    jsonOfFilms=serializers.serialize("json", films)
    films_dict={}
    if 1==1:
        json_status="OK!"
    else:
        json_status="no data.."

#    return HttpResponse(jsonOfHalls)
    films_dict['status'] = json_status
    films_dict['data'] = jsonOfFilms
    return HttpResponse(json.dumps(films_dict))

def getAllNoTestingFilms(request):

    halls = Hall.objects.raw_query({"theater_id":{"$ne":ObjectId("55411ef3956167856613a01d")}})
    halls_array = []
    for hall in halls:
        halls_array.append(hall.id)

    time_from = datetime.datetime.now() - timedelta(minutes=440)

    sessions = Session.objects.filter(start_time__gt=time_from, hall__in=halls_array)
    films = []
    for session in sessions:
        if session.movie in films:
            pass
        else:
            films.append(session.movie)
    #films = sessions.movie.name#Movie.objects.filter(theater=cinema_id)
    jsonOfFilms=serializers.serialize("json", films)
    films_dict={}
    if 1==1:
        json_status="OK!"
    else:
        json_status="no data.."

#    return HttpResponse(jsonOfHalls)
    films_dict['status'] = json_status
    films_dict['data'] = jsonOfFilms
    return HttpResponse(json.dumps(films_dict))

def get_all_comingsoon_films(request):
    response = {"status":"empty"}
    try:
        movies = Movie.objects.filter(coming_soon=True)
    except:
        return HttpResponse(json.dumps(response))
    response['data'] = serializers.serialize("json",movies)
    return HttpResponse(json.dumps(response))




def get_seats_by_session(request,hall, date,time):
    log_request(request)
    sess_name = "%s/%s/%s" % (hall, date,time)
    session = Session.objects.get(uid=sess_name)
    hall_id = session.hall_id
    #return HttpResponse(sess_name)
    #if session_id in request.GET and request.GET[session_id]:
    #    sess_id=request.GET['session_id']
    #Session.objects.get(session_id=sess_id)[:1]
    #return HttpResponse(hall_id)
    seats=testing.models.Seat.objects.filter(hall=hall_id)
    return HttpResponse(serializers.serialize("json",seats))




def get_halls_by_cinema(request, cinema_id):
    log_request(request)
    if 'cinema_id' in request.GET and request.GET['cinema_id']:
        cinema_id=request.POST['cinema_id']
    halls = Hall.objects.filter(theater=cinema_id)
    jsonOfHalls=serializers.serialize("json", halls)
    halls_dict={}
    if 1==1:
        json_status="OK!"
    else:
        json_status="no data.."

#    return HttpResponse(jsonOfHalls)
    halls_dict['status'] = json_status
    halls_dict['data'] = jsonOfHalls
    return HttpResponse(json.dumps(halls_dict))


    return HttpResponse(json.dumps(serializers.serialize("json", halls)))

@csrf_exempt
def get_session_clean_info(request):
    try:
        session_id = request.POST['session_id']
        session = Session.objects.get(uid=session_id)
        session_meta = {}
        session_meta['movie'] = session.m_name
        session_meta['hall'] = session.hall.title
        session_meta['date_time'] = str(session.start_time)
        response = {"status":"ok", "data":session_meta}
        return HttpResponse(json.dumps(response))
    except Exception,e:
        return HttpResponse(json.dumps({"status":"failed", "Error":str(e)}))


def get_specific_session(request):
    session_id = request.GET['session_id']
    session = Session.objects.get(uid=session_id)#, is_canceled=None)
    session.tickets = []
    response = {"status":"ok"}
    response['data'] = serializers.serialize("json", [session])
    return HttpResponse(json.dumps(response))


def get_sessions_by_hall(request, hall_id):
    log_request(request)
    sessions=Session.objects.filter(hall_id=hall_id)#, is_canceled=None)
    jsonOfSessions=serializers.serialize("json", sessions, fields=('movie', 'start_time', 'hall','m_name','prices','duration', 'poster_url', 'is_3d', 'session_language', 'mpaa_rating'))
    #jsonFinal = []
    #for session in sessions:
    #    movie = serializers.serialize("json", [Movie.objects.get(name=session.movie)])
    #    sessionJSON = {"movie":session.movie, "start_time":session.start_time, "hall":session.hall, "m_name":session.m_name, "prices":session.prices, "duration":session.duration, "poster_url":session.poster_url, "emb_movie": movie}
        #jsonSession['emb_movie'] = serializers.serialize("json", movie)
    #    jsonFinal.append(sessionJSON)
    sessions_dict={}
    json_status=""
    if 1==1:
        json_status="OK!"
    else:
        json_status="no data.."
    sessions_dict['status'] = json_status
    sessions_dict['data']= jsonOfSessions
    return HttpResponse(json.dumps(sessions_dict))

def get_sessions_by_hall_only_new(request, hall_id):
    log_request(request)
    today = datetime.datetime.now()#today()
    today = today.replace(hour=0, minute=0, second=0)
    yesterday = today - timedelta(1)
    sessions=Session.objects.filter(hall_id=hall_id, start_time__gte=today)#, is_canceled=None)
    jsonOfSessions=serializers.serialize("json", sessions, fields=('movie', 'start_time', 'hall','m_name','prices','duration', 'poster_url', 'is_3d', 'session_language', 'mpaa_rating'))
    #jsonFinal = []
    #for session in sessions:
    #    movie = serializers.serialize("json", [Movie.objects.get(name=session.movie)])
    #    sessionJSON = {"movie":session.movie, "start_time":session.start_time, "hall":session.hall, "m_name":session.m_name, "prices":session.prices, "duration":session.duration, "poster_url":session.poster_url, "emb_movie": movie}
        #jsonSession['emb_movie'] = serializers.serialize("json", movie)
    #    jsonFinal.append(sessionJSON)
    sessions_dict={}
    json_status=""
    if 1==1:
        json_status="OK!"
    else:
        json_status="no data.."
    sessions_dict['status'] = json_status
    sessions_dict['data']= jsonOfSessions
    return HttpResponse(json.dumps(sessions_dict))

def get_sessions_by_cinema_movie(request, cinema_id, movie_id):
    log_request(request)
    today = datetime.datetime.now()#today()
    today = today.replace(hour=0, minute=0, second=0)
    yesterday = today - timedelta(1.0)
    halls = Hall.objects.filter(theater=cinema_id)
    halls_array = []
    for hall in halls:
        halls_array.append(hall.id)

    sessions=Session.objects.filter(hall__in=halls_array, movie=movie_id,start_time__gte=today)#, is_canceled=None)
    jsonOfSessions=json.loads(serializers.serialize("json", sessions, fields=('movie', 'start_time', 'hall','hall_name','m_name','prices','duration', 'poster_url', 'is_3d', 'session_language', 'mpaa_rating')))

    for session in jsonOfSessions:
        #return HttpResponse([session])
        hall_id = session['fields']['hall']
        #return HttpResponse(hall_id)
        hall = Hall.objects.get(id=hall_id)
        session['hall_name'] = hall.title
    jsonOfSessions = json.dumps(jsonOfSessions)

    sessions_dict={}
    json_status=""
    if 1==1:
        json_status="OK!"
    else:
        json_status="no data.."
    sessions_dict['status'] = json_status
    sessions_dict['data']= jsonOfSessions
    return HttpResponse(json.dumps(sessions_dict))

def get_sessions_by_cinema_movie_date(request, cinema_id, movie_id, date_offset):
    log_request(request)
    today = datetime.datetime.now()#today()
    today = today.replace(hour=0, minute=0, second=0)
    somedate = today + timedelta(int(date_offset))
    somedate = somedate.replace(hour=0, minute=0, second=0)
    somedateTomorrow = somedate + timedelta(1)
    somedateTomorrow = somedateTomorrow.replace(hour=0, minute=0, second=0)
    halls = Hall.objects.filter(theater=cinema_id)
    halls_array = []
    for hall in halls:
        halls_array.append(hall.id)

    sessions=Session.objects.filter(hall__in=halls_array, movie=movie_id,start_time__gte=somedate, start_time__lte=somedateTomorrow).order_by("-start_time") #, is_canceled=None
    jsonOfSessions=json.loads(serializers.serialize("json", sessions, fields=('movie', 'start_time', 'hall','hall_name','m_name','prices','duration', 'poster_url', 'is_3d', 'session_language', 'mpaa_rating')))

    for session in jsonOfSessions:
        #return HttpResponse([session])
        hall_id = session['fields']['hall']
        #return HttpResponse(hall_id)
        hall = Hall.objects.get(id=hall_id)
        session['hall_name'] = hall.title
    jsonOfSessions = json.dumps(jsonOfSessions)

    sessions_dict={}
    json_status=""
    if 1==1:
        json_status="OK!"
    else:
        json_status="no data.."
    sessions_dict['status'] = json_status
    sessions_dict['data']= jsonOfSessions
    return HttpResponse(json.dumps(sessions_dict))

#@csrf_exempt
def get_sessions_by_days(request):
    cinema_id = request.POST['cinema_id']
    time_delta = request.POST['time_delta']
    log_request(request)
    today = datetime.datetime.now()#today()
    today = today.replace(hour=0, minute=0, second=0)
    day = today + timedelta(int(time_delta))
    day = day.replace(hour=0, minute=0, second=0)
    dayTomorrow = day + timedelta(1)
    dayTomorrow = dayTomorrow.replace(hour=0, minute=0, second=0)
    halls = Hall.objects.filter(theater=cinema_id)
    halls_array = []
    for hall in halls:
        halls_array.append(hall.id)

    sessions=Session.objects.filter(hall__in=halls_array, start_time__gte=day, start_time__lt=dayTomorrow).order_by("-start_time") #, is_canceled=None
    jsonOfSessions=json.loads(serializers.serialize("json", sessions, fields=('movie', 'start_time', 'hall','hall_name','m_name','prices','duration', 'poster_url', 'is_3d', 'session_language', 'mpaa_rating')))

    for session in jsonOfSessions:
        #return HttpResponse([session])
        hall_id = session['fields']['hall']
        #return HttpResponse(hall_id)
        hall = Hall.objects.get(id=hall_id)
        session['hall_name'] = hall.title
    jsonOfSessions = json.dumps(jsonOfSessions)

    sessions_dict={}
    json_status=""
    if 1==1:
        json_status="OK!"
    else:
        json_status="no data.."
    sessions_dict['status'] = json_status
    sessions_dict['data']= jsonOfSessions
    return HttpResponse(json.dumps(sessions_dict))


@ajax_request
def get_seats_states_by_session(request,hall, date,time):
    log_request(request)
    #cancel all unpaid online orders
    #check_online_orders_to_cancel()
    sess_name = "%s/%s/%s" % (hall, date,time)
    tickets = Ticket.objects.filter(session_id=sess_name, canceledDate=None)

    #if 'tes_1' in hall:
    try:
        blocked = TicketBlocked.objects.filter(session_id=sess_name)
        returnArray = []
        for ticket in tickets:
            returnArray.append(ticket)

        for blockedTicket in blocked:
            returnArray.append(blockedTicket)

        sessions_dict = {}
        sessions_dict['status'] = "OK!"
        sessions_dict["data"] = serializers.serialize("json",returnArray)
        return sessions_dict
    except Exception, e:
        print >>sys.stderr,str(e)
        sessions_dict = {}
        sessions_dict['status'] = "Failed"
        sessions_dict['exception'] = str(e)
        return HttpResponse(json.dumps(sessions_dict))

    # sessions_dict = {}
    # sessions_dict['status'] = "OK!"
    # sessions_dict["data"] = serializers.serialize("json", tickets)
    # return sessions_dict


def get_theaters(request):
    log_request(request)
    theaters =  Theater.objects.filter(testing_only=False)
    jsonOfTheaters=serializers.serialize("json", theaters)
    response_dict = {}
    json_status=""
    if 1==1:
        json_status = "OK!"
    else:
        json_status = "no data.."

    response_dict['status'] = json_status
    response_dict['data'] = jsonOfTheaters
    return HttpResponse(json.dumps(response_dict))

def get_theaters_non_ios(request):
    log_request(request)
    theaters =  Theater.objects.all()
    jsonOfTheaters=serializers.serialize("json", theaters)
    response_dict = {}
    json_status=""
    if 1==1:
        json_status = "OK!"
    else:
        json_status = "no data.."

    response_dict['status'] = json_status
    response_dict['data'] = jsonOfTheaters
    return HttpResponse(json.dumps(response_dict))




def sendemail(request):
    email = request.GET["email"]
    to_emails = []
    to_emails.append(email)
    subject, from_email, to = 'hello', 'giorgiare@gmail.com', email
    text_content = 'This is an important message.'
    html_content = '<p>This is an <strong>important</strong> message.</p>'
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    #msg.attach_alternative(html_content, "text/html")
    msg.send()
    return HttpResponse("done")

@ajax_request
@csrf_exempt
def testPython(request):
    if "hall" in request.GET and request.GET["hall"]:
        hall = Hall.objects.get(name=request.GET["hall"])
        #return {"response": hall.pk}
        session = Session.objects.filter(hall_id=hall.pk)
        sessions = []
        for sess in session:
            sess.tickets = []
            sess.emb_movie = json.loads(serializers.serialize("json",[Movie.objects.get(pk=sess.movie.pk)]))
            sessions.append(sess)
        return {"response": serializers.serialize("json", sessions)}
    return HttpResponse("done")




#helper functions down

def isTicketAvailable(sess_id,seat_id):
    log_request(request)
    session = Session.objects.get(uid=sess_id)
    tickets = session.tickets
    for ticket in tickets:
      seat = testing.models.Seat.objects.get(pk=ticket.seat.pk)
      if seat.uid == seat_id:
        return False # if ticket is not Availble
    return True



def getTicketBySeatId(sess_id, seat_id):
    log_request(request)
    session = Session.objects.get(uid=sess_id)
    tickets = session.tickets
    for ticket in tickets:
      seat = testing.models.testing.models.Seat.objects.get(pk=ticket.seat.pk) # strange code here..
      if seat.uid == seat_id:
        return ticket



def block_ticket_api(request, data): # why do we need data if Request itself is gonna be sliced by params?
    log_request(request)
    if 'data' in request.POST and request.POST['data']:
        id = request.POST['data']
        sess_id = request.POST['sess']
        session = Session.objects.get(uid=sess_id)
        seat = testing.models.testing.models.Seat.objects.get(uid = id)
        ticket = BlockedTicket.objects.create(session=session)
        ticket.seat=seat
        ticket.has_discount=False
        ticket.price=0.00
        ticket.date_printed="2015-01-12"
        ticket.date_sold=datetime.datetime.now()
        ticket.save()
        return HttpResponse(ticket.seat.name)

def get_blocked_api(request,hall,day,month,year,hour,):   # same here.. these functions resembles ancestor ones above.
    log_request(request)
    if 'sess' in request.POST and request.POST['sess']:
        sess_id = request.POST['sess']
        session = Session.objects.get(uid=sess_id)
        btickets = BlockedTicket.objects.filter(session=session)
        seats = []
        for ticket in btickets:
            ticket.seat.status = "blocked"
            seats.append(ticket.seat)
        return HttpResponse(serializers.serialize("json",seats))
        #return HttpResponse(serializers.serialize("json", btickets))


def TicketSelectApi(request):
    log_request(request)
    session_id=request.POST['session_id']
    seat_id = request.POST['seats']
    csrftoken = request.COOKIES['csrftoken']

    tickets = Session.objects.get(uid=session_id).tickets

    for ticket in tickets:
        seatID = testing.models.Seat.objects.get(uid=ticket.emb_seat)
        if(seat_id==seatID.uid):
            if (ticket.process):
                order = TicketOrder.objects.get(pk=ticket.ticket_order_id)
                order.cart_log = None
#               return HttpResponse(order.pk)
#               to = TicketOrder.objects.all()[0]
                order_items = []
                for item in order.items:
                    jsonOfItem = json.loads(serializers.serialize("json", [item]))
                    order_items.append(jsonOfItem)
                order.items = order_items
                if order.reservation != None:
                    order.reservation = json.loads(serializers.serialize("json", [order.reservation]))
                if order.box_office_items:
                    box_items = []
                    for boxOrder in order.box_office_items:
                        jsonOfBoxItem = json.loads(serializers.serialize("json", [boxOrder]))
                        box_items.append(jsonOfBoxItem)
                    order.box_office_items = box_items

                to = serializers.serialize("json", [order])
                if order.agent:
                    to = json.loads(json.loads(json.dumps(to)))
                    to[0]['fields']['agent'] = order.agent.name
                    to = json.dumps(to)


#                order_items=[]
                #for item in order.items:
                 #   jsonOfItem=json.loads(serializers.serialize("json", [item]))
                  #  order_items.append(jsonOfItem)
                #order.items = order_items
                return HttpResponse(json.dumps(to))

            # elif (ticket.process=="RE"):
           #     order = TicketOrder.objects.get(pk=ticket.ticket_order_id)
            #    return HttpResponse(order.pk)
             #   order_items=[]
              #  for item in order.items:
               #     jsonOfItem=json.loads(serializers.serialize("json", [item]))
                #    order_items.append(jsonOfItem)
                #order.items = order_items
                #return HttpResponse(serializers.serializer("json", [order]))
            #else :


             #return HttpResponse(iterator)

    else:
        try:
            if not should_send_socket_for_seat(request.POST["seats"], session_id, request.user.username, csrftoken):
                response = {"status":"fail","message":"Failed", "TicketStatus":"Already Clicked"}
                return HttpResponse(json.dumps(response))
            seatsArray = []
            seatsArray.append(request.POST["seats"])
            seatsJson = json.dumps(seatsArray)
            sent_command_withseats("sell", request.user, session_id, seatsJson, csrftoken)
        except:
            response = {"status":"fail","message":"Exception Selecting Seats", "TicketStatus":"Exception"}
            return HttpResponse(json.dumps(response))
        block_ticket_insession(seat_id, session_id, request.user.username)
        response = {"status":"OK","message":"Sucess", "TicketStatus":"Ticket is Free"}
        return HttpResponse(json.dumps(response))


def TicketUnselectApi(request):
    log_request(request)
    session_id=request.POST['session_id']
    seats = request.POST['seats']
    csrftoken = request.COOKIES['csrftoken']
    seats_array = json.loads(seats)
    for seat in seats_array:
        unblock_ticket_insession(seat, session_id, request.user.username)

    try:
        #sent_command_withseats(command, user, session, seatsArray, token):
        sent_command_withseats('free', request.user, session_id, seats, csrftoken)
    except Exception,e:
        print >>sys.stderr, str(e)

    return HttpResponse("DINGO")

# ========== BLOCK TICKETS BAKUR API ================== #
def should_send_socket_for_seat(seat_id, session_id, user, csrftoken):
    seat = testing.models.Seat.objects.get(uid=seat_id.encode('utf-8'))#raw_query({"_id":seat_id.encode('utf-8')}).limit(1)
    session = Session.objects.get(uid=session_id)
    try:
        ticket = TicketBlocked.objects.get(session=session, seat=seat)
        if ticket:
            if ticket.block_user == user:
                #unselect
                unblock_ticket_insession(seat, session_id, request.user.username)
                try:
                    #sent_command_withseats(command, user, session, seatsArray, token):
                    sent_command_withseats('free', user, session_id, [seat_id], csrftoken)
                except Exception,e:
                    print >>sys.stderr, str(e)
                return False
            else:
                return False
        else:
            return False
    except Exception,e:
        print >>sys.stderr, str(e)
        return True


def block_ticket_insession(seat_id,session_id, user):
    print >>sys.stderr, seat_id
    order = None
    seat = testing.models.Seat.objects.get(uid=seat_id.encode('utf-8'))#raw_query({"_id":seat_id.encode('utf-8')}).limit(1)
    session = Session.objects.get(uid=session_id)
    try:
        #check if seat already blocked by the same user
        #if yes, return success
        #else fail
        ticket = TicketBlocked.objects.get(session=session, seat=seat)
        if ticket:
            if ticket.block_user == user:
                return "Success"
            else:
                return "Blocked By Other User"
        else:
            price = session.prices[seat.ttype]
            #set SEAT process to BL - blocked
            ticket = TicketBlocked.objects.create(block_user=user, session=session, emb_seat=seat,seat=seat, stype=seat.ttype,process="RE", has_discount=False, price=float(price), initial_price=float(price),date_printed=datetime.datetime.now(), date_sold=datetime.datetime.now(),ticket_order=order)
            ticket.save()
            return "Success"
    except Exception,e:
        print >>sys.stderr, str(e)
        try:
            price = session.prices[seat.ttype]
            #set SEAT process to BL - blocked
            ticket = TicketBlocked.objects.create(block_user=user, session=session, emb_seat=seat,seat=seat, stype=seat.ttype,process="RE", has_discount=False, price=float(price), initial_price=float(price),date_printed=datetime.datetime.now(), date_sold=datetime.datetime.now(),ticket_order=order)
            ticket.save()
            return "Success"
        except Exception,e:
            print >>sys.stderr, str(e)
            return "Failed"

def unblock_ticket_insession(seat_id,session_id, user):
    order = None
    seat = testing.models.Seat.objects.get(uid=seat_id.encode('utf-8'))
    session = Session.objects.get(uid=session_id)
    try:
        #check if seat already blocked by the same user
        #if yes, return success
        #else fail
        ticket = TicketBlocked.objects.get(session=session, seat=seat)
        if ticket:
            if ticket.block_user == user:
                ticket.delete()
                return "Success"
            else:
                return "Blocked By Other User"
        else:
            return "No SeatID or in Session Blocked"
    except Exception,e:
        print >>sys.stderr, str(e)
        return "No SeatID or in Session Blocked"

def expire_blocked_seats():
    dateNow = datetime.datetime.now() - datetime.timedelta(minutes=2)
    blockedTicketsToExpire = TicketBlocked.objects.filter(created__lte=dateNow)
    seats_array = {}
    for blockedTicket in blockedTicketsToExpire:
        session_id = blockedTicket.session.uid
        if session_id in seats_array:
            seats_array[session_id].append(blockedTicket.emb_seat.uid)
        else:
            seats_array[session_id] = [blockedTicket.emb_seat.uid]
        #emit to socket that tickets are freed
        blockedTicket.delete()
    try:
        for key, session_array in seats_array.iteritems():
            sent_command_withseats('free', 'DjangoCron', key.encode('utf-8'), json.dumps(session_array), 'nocookie')
    except Exception,e:
        print >>sys.stderr, str(e)

    return "Dingo"

# ========== BLOCK TICKETS BAKUR API END ================== #


def ticket_book(request, hall, date, time, seat_id):
    session_id=hall+"/"+date+"/"+time
    all_ticks = Session.objects.get(uid=session_id).tickets

    booked_tickets = {}
    for ticket in all_ticks:
        i=0
        booked_tickets[i] = ticket.status



    return HttpResponse(serializers.serialize("json", booked_tickets))


# ("rus1_r1_s1", "rus1_r1_s2"), "RE", "SL" "SR"


def ticket_buy(request, hall, date, time, seat_id):
    session_id=hall+"/"+date+"/"+time
    all_ticks = Session.objects.get(uid=session_id).tickets
    ticket=all_ticks.objects.get()



    if isTicketAvailable(session_id, seat_id):

        ticket = getTicketBySeatId(session_id, seat_id)
        orders = TicketOrder.objects.filter(session= Session.objects.get(uid=session_id))
        for order in orders:
            for item in order.items:
                if ticket.pk == item.pk:
                    item.process = "SL"
                    item.save()
                    return HttpResponse(item)

    else:

        pass



    return HttpResponse (isTicketAvailable)

@csrf_exempt
def getReseverationsForSession(request,hall, date,time):
    sess_name ="%s/%s/%s" % (hall, date,time)
    #this weird syntax is for reservation!=None
    ticketReservations = TicketOrder.objects.filter(~Q(reservation=None),session=sess_name)
    response = {"status":"ok", "data": "No Reservations"}
    for ticketOrder in ticketReservations:
        ticketOrder.cart_log = None
        order_items = []
        for item in ticketOrder.items:
            jsonOfItem = json.loads(serializers.serialize("json", [item]))
            order_items.append(jsonOfItem)
        ticketOrder.items = order_items
        if ticketOrder.box_office_items:
            box_items = []
            for boxOrder in ticketOrder.box_office_items:
                jsonOfBoxItem = json.loads(serializers.serialize("json", [boxOrder]))
                box_items.append(jsonOfBoxItem)
            ticketOrder.box_office_items = box_items
        ticketOrder.reservation = json.loads(serializers.serialize("json", [ticketOrder.reservation]))
    if len(ticketReservations) > 0:
        response["data"] = serializers.serialize("json", ticketReservations)
    return HttpResponse(json.dumps(response))




@csrf_exempt
def getPosterForMovieApi(request, movieID):
    log_request(request)
    movie = Movie.objects.get(id=movieID)
    poster_url = movie.poster_url
    return HttpResponse(poster_url)




#copied
@csrf_exempt
def DiscountsApi(request):
    log_request(request)
    discounts =  Discount.objects.all()
    jsonOfDiscounts=serializers.serialize("json", discounts)
    response_dict = {}
    json_status=""
    if 1==1:
        json_status = "OK!"
    else:
        json_status = "no data.."

    response_dict['status'] = json_status
    response_dict['data'] = jsonOfDiscounts
    return HttpResponse(json.dumps(response_dict))

@csrf_exempt
def BarDiscountsApi(request):
    log_request(request)
    discounts =  Discount.objects.filter(ttype="CS")
    jsonOfDiscounts=serializers.serialize("json", discounts)
    response_dict = {}
    json_status=""
    if 1==1:
        json_status = "OK!"
    else:
        json_status = "no data.."

    response_dict['status'] = json_status
    response_dict['data'] = jsonOfDiscounts
    return HttpResponse(json.dumps(response_dict))



#@csrf_exempt
def TicketBookApi(request):
    log_request(request)
    if validate_posted_variables(request, "seats", "session_id", "cust_name","cust_surname"):

        # get posted variables
        seats = request.POST["seats"]
        session_id = request.POST["session_id"]
        cust_name = request.POST["cust_name"]
        cust_surname = request.POST["cust_surname"]
        cust_mobile = request.POST['cust_mobile']
        cust_email = request.POST['cust_email']

        seats_array = json.loads(seats)
        if len(seats_array)==0 or not seats_array or not seats:
            response = {"status":"failed", "message":"No seats picked"}
            return HttpResponse(json.dumps(response))

        customer = None

        try:
            customer = Customer.objects.get(name=cust_name, surname=cust_surname, phone=cust_mobile, email=cust_email)
        except Customer.DoesNotExist:
            cust = Customer.objects.create(name=cust_name, surname=cust_surname, phone=cust_mobile, email=cust_email)
            cust.save()
            customer = Customer.objects.get(name=cust_name, surname=cust_surname, phone=cust_mobile, email=cust_email)


        csrftoken = request.COOKIES['csrftoken']
        user = request.user #request.META.get('REMOTE_USER')
        shiftagent = Shift.objects.get(user=user, csrftoken=csrftoken).agent
        agent = Agent.objects.get(id=shiftagent)
        # if user.username == 'ubuntu':
        #     agent = Agent.objects.get(id="554cebb6956167b76e9f1e09")

        if not user.is_staff and ('rus' in session_id or 'ami' in session_id):
            response = {"status":"failed", "message":u"ამ მოენტში მხოლოდ Cavea-ს ყიდვა/დაჯავშვნაა შესაძლებელი", "reservation_pin":u"ამ მოენტში მხოლოდ Cavea-ს ყიდვა/დაჯავშვნაა შესაძლებელი"}
            return HttpResponse(json.dumps(response))

        if not check_seats_for_availability(session_id, seats, user.username, None):
            response = {"status":"failed", "message":"Ticket Already Bought By Another User"}#u"ბილეთი უკვე გაყიდულია"}
            return HttpResponse(json.dumps(response))


        # get django objects from database
        session = Session.objects.get(uid=session_id)
        #create order
        order = TicketOrder.objects.create(session=session, user=user,agent=agent)
        #create reservation

        pin = str(random.randint(1,999999)).zfill(6)
        reservation = Reservation.objects.create(name=cust_name,surname=cust_surname, mobile=cust_mobile, email=cust_email, pin=pin, customer=customer)
        order.reservation = reservation
        #create tickets for separate seat in posted variable
        ticket_list = []
        for seat_ in seats_array:
            seat = testing.models.Seat.objects.get(uid=seat_['setUID'])
            #make sure ticket does not exist in db
            t = None
            for ticket in session.tickets:
                if ticket.seat.uid == seat.uid:
                        t = ticket
                        order.delete()
                        # TO-DO - query and delete tickets, if they were already created?
                        response = {"status":"failed", "message":u"ბილეთი უკვე გაყიდულია"}
                        return HttpResponse(json.dumps(response))
            # if ticket does not exist : create ticket
            if not t:
                price = session.prices[seat.ttype]
                ticket = Ticket.objects.create(session=session, emb_seat=seat,seat=seat, process="RE", has_discount=False, price=float(price), initial_price=float(price),date_printed=datetime.datetime.now(), date_sold=datetime.datetime.now(),ticket_order=order)
                ticket.save()
                ticket.uid = ticket.id
                ticket_list.append(ticket)
        #seats.append(seat)
        if ticket_list:
            session.tickets.extend(ticket_list)
            session.save()

        # if len(ticket_list)>0:
        #     pass
        # else:
        #     oder.delete()
        #     response = {"status":"Failed"}
        #     return HttpResponse(json.dumps(response))

        order.items = Ticket.objects.filter(ticket_order=order)
        order.user = user
        order.save()
        response = {"status":"Failed"}
        if ticket_list:
            response['status'] = 'Ok!'
            response['ticketOrderID'] = order.id
            response['reservation_pin'] = order.reservation.pin
            #removing blocked seats
            try:
                #unblock_ticket_insession(seat_id,session_id, user)
                for seatID in seats_array:
                    unblock_ticket_insession(seatID['setUID'], session_id, request.user.username)
            except:
                pass
            try:
                sent_command_withseats("book", request.user, session_id, request.POST["seats"], csrftoken)
            except:
                pass

        return HttpResponse(json.dumps(response))
    else:
        return HttpResponse("variables not posted")


def validateOrder(request, order_id):
    response = {"status":"Failed"}
#try:
    order = TicketOrder.objects.get(id=order_id)
    order_items = []
    for item in order.items:
        item.emb_seat = serializers.serialize("json", [item.emb_seat])
        jsonOfItem = serializers.serialize("json", [item])
        medeaOfItem = json.loads(jsonOfItem)
        returnItem = medeaOfItem[0]['fields']
        order_items.append(returnItem)
    order.items = order_items
    response = {"status":"ok", "error":"None", "seats":json.dumps(order_items), "movie":order.session.m_name, "hall":order.session.hall.title,"session_datetime":str(order.session.start_time)}
    return  HttpResponse(json.dumps(response))
#except Exception, e:
    # response["error"] = "No Order Found"
    # response["except"] = str(e)
    # return HttpResponse(json.dumps(response))

def TicketBuyBoxProductsOnly(request):
    session_id = request.POST["session_id"]
    cash_payment = request.POST["paidByCash"]
    creditcard_payment=request.POST["paidByCreditCard"]
    giftcard_payment = request.POST["paidByGiftCard"]
    discount_amount = request.POST["discountAmount"]
    initial_amount = request.POST["startAmount"]
    discounted_amount = request.POST["finalAmount"]
    boxOfficeProducts = request.POST["boxOfficeProducts"]
    cartData = None

    if 'cartData' in request.POST:
        cartData = request.POST['cartData']

    csrftoken = request.COOKIES['csrftoken']
    user = request.user #request.META.get('REMOTE_USER')
    shiftagent = Shift.objects.get(user=user, csrftoken=csrftoken).agent
    agent = Agent.objects.get(id=shiftagent)
    session = Session.objects.get(uid=session_id)
    # if user.username == 'ubuntu':
    #     agent = Agent.objects.get(id="554cebb6956167b76e9f1e09")
    #create order
    order = TicketOrder.objects.create(session=session, user=request.user, agent=agent, cart_log=cartData)
    box_products_list = []
    if boxOfficeProducts:
        for boxproduct_ in json.loads(boxOfficeProducts):
            if boxproduct_ == 'None':
                continue
            boxproduct = testing.models.BoxOfficeProduct.objects.get(sku=boxproduct_['sku'])
            box_item = testing.models.BoxOfficeProductItem(emb_product=boxproduct, ticket_order=order, quantity=float(boxproduct_['count']), price_paid_total=float(boxproduct_['total_price']),discount_applied=float(boxproduct_['total_discount']))
            box_item.save()
            box_products_list.append(box_item)

    if len(box_products_list)>0:
        order.box_office_items = testing.models.BoxOfficeProductItem.objects.filter(ticket_order=order)
    order.date_sold = datetime.datetime.now()
    order.payment_by_cash=cash_payment
    order.payment_by_creditcard=creditcard_payment
    order.payment_by_giftcard=giftcard_payment
    order.initial_price=initial_amount
    order.discounted_amount=discounted_amount
    order.discount_amount=discount_amount
    order.user = user
    order.save()
    response = {"status":"Failed"}
    if box_products_list:
        response['status'] = 'Ok!'
        response['ticketOrderID'] = order.id
    else:
        response['message'] = "Seats Unavailable"
    return HttpResponse(json.dumps(response))


#@csrf_exempt
#@login_required
def TicketBuyApi(request):
    #return HttpResponse([request.META])
    #return HttpResponse(request.COOKIES.keys())
    log_request(request)
    if 1==1:#validate_posted_variables(request, "session_id", "seats","boxOfficeProducts"):

        # get posted variables
        seats = request.POST["seats"]
        boxOfficeProducts = None
        if 'boxOfficeProducts' in request.POST:
            boxOfficeProducts = request.POST["boxOfficeProducts"]
            seatsArr = json.loads(seats)
            if len(seatsArr)==0:
                return TicketBuyBoxProductsOnly(request)
        session_id = request.POST["session_id"]
        cash_payment = request.POST["paidByCash"]
        creditcard_payment=request.POST["paidByCreditCard"]
        giftcard_payment = request.POST["paidByGiftCard"]
        discount_amount = request.POST["discountAmount"]
        initial_amount = request.POST["startAmount"]
        discounted_amount = request.POST["finalAmount"]
        pin = None
        cartData = None

        #if session expired, do not sell tickets

        if 'cartData' in request.POST:
            cartData = request.POST['cartData']


        csrftoken = request.COOKIES['csrftoken']
        user = request.user #request.META.get('REMOTE_USER')
        shiftagent = Shift.objects.get(user=user, csrftoken=csrftoken).agent
        agent = Agent.objects.get(id=shiftagent)

        if 'reservationPin' in request.POST:
            pin = request.POST['reservationPin']


        if not check_seats_for_availability(session_id, seats, user.username, pin):
            response = {"status":"failed", "message":"Ticket Already Bought By Another User"}#u"ბილეთი უკვე გაყიდულია"}
            return HttpResponse(json.dumps(response))

        # if user.username == 'ubuntu':
        #     agent = Agent.objects.get(id="554cebb6956167b76e9f1e09")

        #return HttpResponse(["==seats==", seats, "==box==", boxOfficeProducts, "==session==",session_id, "==cash==",cash_payment, "==card==",creditcard_payment, "==giftc==",giftcard_payment, "==discount==",discount_amount, "==initial==",initial_amount, "==isdiscounted==",discounted_amount])

        # get django objects from database
        session = Session.objects.get(uid=session_id)
        #create order
        order = TicketOrder.objects.create(session=session, user=request.user, agent=agent, cart_log=cartData)
        #create tickets for separate seat in posted variable
        ticket_list = []
        for seat_ in json.loads(seats):
            seat = testing.models.Seat.objects.get(uid=seat_['setUID'])
            seat_inital_price = seat_['seatPrice']
            seat_discount = seat_['discountAmount']
            price_paid = float(seat_inital_price) - float(seat_discount)
            #return HttpResponse(serializers.serialize("json",[seat]))
            #make sure ticket does not exist in db
            t = None
            for ticket in session.tickets:
                if ticket.seat.uid == seat.uid:
                    #TO-DO can some tickets be here as a leftover of unpulled order cancelations?
                    #TO-DO if order sent with extra tickets (on top of reservation), not handling given scenario

                    order = ticket.ticket_order
                    if 'reservationPin' in request.POST and request.POST['reservationPin'] and request.POST['reservationPin'] != 'None':
                        reservationTicketsCount = 0

                        # if attempt to sell the order of booked tickets, act accordingly
                        if order and order.reservation and order.reservation.pin == request.POST['reservationPin']:
                            tickets = session.tickets
                            seatsArrayJSON = json.loads(seats)

                            extra_tickets = []

                            for t in tickets:
                                for seatBundle in seatsArrayJSON:
                                    uid=seatBundle['setUID']
                                    if uid == t.emb_seat.uid:
                                        seat_inital_price = seatBundle['seatPrice']
                                        seat_discount = seatBundle['discountAmount']
                                        price_paid = float(seat_inital_price) - float(seat_discount)
                                        if t.process =="RE":
                                            reservationTicketsCount = reservationTicketsCount + 1
                                            t.process = "SL"
                                            t.date_printed=datetime.datetime.now()
                                            t.date_sold=datetime.datetime.now()
                                            t.initial_price=seat_inital_price
                                            t.discount_amount=seat_discount
                                            t.price_paid=price_paid
                                            t.save()
                                        break
                            session.save()

                            try:

                                tickets = order.items
                                for t in tickets:
                                    for seatBundle in seatsArrayJSON:
                                        uid=seatBundle['setUID']
                                        if uid == t.emb_seat.uid:
                                            seat_inital_price = seatBundle['seatPrice']
                                            seat_discount = seatBundle['discountAmount']
                                            price_paid = float(seat_inital_price) - float(seat_discount)
                                            if t.process == "RE":
                                                #reservationTicketsCount = reservationTicketsCount + 1
                                                t.process = "SL"
                                                t.date_printed=datetime.datetime.now()
                                                t.date_sold=datetime.datetime.now()
                                                t.initial_price=seat_inital_price
                                                t.discount_amount=seat_discount
                                                t.price_paid=price_paid
                                                t.save()
                                            break
                            except:
                                pass

                            box_products_list = []
                            for boxproduct_ in json.loads(boxOfficeProducts):
                                if boxproduct_ == 'None':
                                    continue
                                boxproduct = testing.models.BoxOfficeProduct.objects.get(sku=boxproduct_['sku'])
                                box_item = testing.models.BoxOfficeProductItem(emb_product=boxproduct, ticket_order=order, quantity=float(boxproduct_['count']), price_paid_total=float(boxproduct_['total_price']),discount_applied=float(boxproduct_['total_discount']))
                                box_item.save()
                                box_products_list.append(box_item)

                            #order.items = Ticket.objects.filter(ticket_order=order)
                            if len(box_products_list)>0:
                                order.box_office_items = testing.models.BoxOfficeProductItem.objects.filter(ticket_order=order)
                            order.agent = agent
                            order.date_sold = datetime.datetime.now()
                            order.payment_by_cash=cash_payment
                            order.payment_by_creditcard=creditcard_payment
                            order.payment_by_giftcard=giftcard_payment
                            order.initial_price=initial_amount
                            order.discounted_amount=discounted_amount
                            order.discount_amount=discount_amount
                            order.user = user
                            # TO-DO - BUG CRON CANCELING RESERVATION if RESERVATION PARTIALLY CANCELED?
                            order.reservation_sold = datetime.datetime.now()
                            order.save()
                            response = {'status':'Ok!','ticketOrderID': order.id, "reservationTicketsChanged":reservationTicketsCount}
                            #removing blocked seats
                            try:
                                #unblock_ticket_insession(seat_id,session_id, user)
                                for seatID in json.loads(seats):
                                    unblock_ticket_insession(seatID['setUID'], session_id, request.user.username)
                            except Exception,e:
                                print >>sys.stderr, str(e)

                            try:
                                sent_command_withseats("sell", request.user, session_id, request.POST["seats"], csrftoken)
                            except:
                                pass
                            return HttpResponse(json.dumps(response))


                    #sometimes canceled tickets don't get pulled from session so second check
                    else:

                        try:
                            order_ticket = TicketOrder.objects.get(id=order.id)
                            if not order_ticket or not order_ticket.items or len(order_ticket.items) == 0:
                                try:
                                    ticket.delete()
                                    session.save()
                                except:
                                    pass
                                price = session.prices[seat.ttype]
                                ticket = Ticket.objects.create(session=session, emb_seat=seat,seat=seat, process="SL", has_discount=False, price=float(price), date_printed=datetime.datetime.now(), date_sold=datetime.datetime.now(),ticket_order=order, stype=seat.ttype, initial_price=seat_inital_price, discount_amount=seat_discount, price_paid=price_paid)
                                ticket.save()
                                ticket.uid = ticket.id
                                ticket_list.append(ticket)
                                continue
                        except:
                            try:
                                ticket.delete()
                                session.save()
                            except:
                                pass
                            price = session.prices[seat.ttype]
                            ticket = Ticket.objects.create(session=session, emb_seat=seat,seat=seat, process="SL", has_discount=False, price=float(price), date_printed=datetime.datetime.now(), date_sold=datetime.datetime.now(),ticket_order=order, stype=seat.ttype, initial_price=seat_inital_price, discount_amount=seat_discount, price_paid=price_paid)
                            ticket.save()
                            ticket.uid = ticket.id
                            ticket_list.append(ticket)
                            continue

                    t = ticket
                    response = {"status":"failed", "message":"Ticket Already Sold"}#u"ბილეთი უკვე გაყიდულია"}
                    return HttpResponse(json.dumps(response))

            # if ticket does not exist : create ticket
            if not t:
                price = session.prices[seat.ttype]
                ticket = Ticket.objects.create(session=session, emb_seat=seat,seat=seat, process="SL", has_discount=False, price=float(price), date_printed=datetime.datetime.now(), date_sold=datetime.datetime.now(),ticket_order=order, stype=seat.ttype, initial_price=seat_inital_price, discount_amount=seat_discount, price_paid=price_paid)
                ticket.save()
                ticket.uid = ticket.id
                ticket_list.append(ticket)

        box_products_list = []
        if boxOfficeProducts:
            for boxproduct_ in json.loads(boxOfficeProducts):
                if boxproduct_ == 'None':
                    continue
                boxproduct = testing.models.BoxOfficeProduct.objects.get(sku=boxproduct_['sku'])
                box_item = testing.models.BoxOfficeProductItem(emb_product=boxproduct, ticket_order=order, quantity=float(boxproduct_['count']), price_paid_total=float(boxproduct_['total_price']),discount_applied=float(boxproduct_['total_discount']))
                box_item.save()
                box_products_list.append(box_item)

        #seats.append(seat)
        if ticket_list:
            session.tickets.extend(ticket_list)
            session.save()

        order.items = Ticket.objects.filter(ticket_order=order)
        if len(box_products_list)>0:
            order.box_office_items = testing.models.BoxOfficeProductItem.objects.filter(ticket_order=order)
        order.date_sold = datetime.datetime.now()
        order.payment_by_cash=cash_payment
        order.payment_by_creditcard=creditcard_payment
        order.payment_by_giftcard=giftcard_payment
        order.initial_price=initial_amount
        order.discounted_amount=discounted_amount
        order.discount_amount=discount_amount
        order.user = user
        order.save()
        response = {"status":"Failed"}
        if ticket_list:
            response['status'] = 'Ok!'
            response['ticketOrderID'] = order.id
            #removing blocked seats
            try:
                #unblock_ticket_insession(seat_id,session_id, user)
                for seatID in json.loads(seats):
                    unblock_ticket_insession(seatID['setUID'], session_id, request.user.username)
            except Exception, e:
                print >>sys.stderr, str(e)
            try:
                sent_command_withseats("sell", request.user, session_id, request.POST["seats"], csrftoken)
            except:
                pass
        else:
            response['message'] = "Seats Unavailable"
        return HttpResponse(json.dumps(response))
    else:
        return HttpResponse(json.dumps({"status":"Failed","message":"Error"}))


#@csrf_exempt
def TicketBuyBookedApi(request):
    log_request(request)
    if validate_posted_variables(request, "session_id", "seats"):

        seats = request.POST["seats"]
        session_id = request.POST["session_id"]


        session = Session.objects.get(uid=session_id)
        order = TicketOrder.objects.create()
        ticket_list = []
        for seat_ in json.loads(seats):

            seat = testing.models.Seat.objects.get(uid=seat_)
            tickets = session.tickets
            for t in tickets:
                if t.process =="RE" and t.seat.uid==seat_:
                    t.process = "SL"
                    ticket_list.append(t)
                    session.save()
        return HttpResponse(ticket_list)


def check_seats_for_availability(session_id, seats, user, hasPin):
    seats_array = json.loads(seats)
    for seat_dict in seats_array:
        seat_id = seat_dict['setUID']
        try:
            blocked = TicketBlocked.objects.raw_query({"$and":[{'session_id':session_id.encode('utf-8')}, {"emb_seat.uid":seat_id.encode('utf-8')}]})
            if blocked and blocked[0].block_user != user:
                return False
        except Exception,e:
            print >>sys.stderr, str(e)

        try:
            ticket = Ticket.objects.raw_query({"$and":[{"session_id":session_id.encode('utf-8')},{"emb_seat.uid":seat_id.encode('utf-8')}, {"canceledDate":None}]})
            if ticket and ticket[0]:
                order = ticket[0].ticket_order
                if order.user != user:
                    if not order.reservation:
                        return False
                    elif not hasPin:
                        return False

        except Exception,e:
            print >>sys.stderr, str(e)

    return True


def validate_posted_variables(request, *args):
    coll = []
    for arg in args:
        if arg in request.POST and request.POST[arg]:
            coll.append(True)
    if len(coll)==len(args):
        return True




#@csrf_exempt
def TicketCancelApi(request):
    log_request(request)
    session_id=request.POST["session_id"]
    seatsRaw = request.POST["seats"]
    csrftoken = request.COOKIES['csrftoken']
    user = request.user
    return ticketcancelapi_utility(session_id, seatsRaw, user, csrftoken)

def ticketcancelapi_utility(session_id, seatsRaw, user, csrftoken):
    tickets=json.loads(seatsRaw)

    session = Session.objects.get(uid=session_id)
    ticketOrder = None
    ticketOrderID = None

    session_tickets = session.tickets#Session.objects.get(pk=session_id).tickets
    #return HttpResponse(session_id)

    response = {"status":"Failed", "deleted":"0"}

    deleted = 0

    toDeleteArray = []
    deletedSeatIDs = []

    cumulative_returned_discount = 0
    cumulative_returned_amount = 0

    for ticket in session_tickets:
        for tick in tickets:
            if tick == ticket.emb_seat.uid:
                logged_ticket=create_log_ticket(ticket)
                if not ticketOrderID:
                    query = {"$and":[{"session_id":session_id.encode('utf-8')},{"items" : {"$elemMatch":{'emb_seat.uid' : tick.encode('utf-8')}}}]}
                    ticketOrderQuery = TicketOrder.objects.raw_query(query)
                    for ticketOrders in ticketOrderQuery:
                        ticketOrderID = ticketOrders.id
                        ticketOrder = ticketOrders
                    #return HttpResponse(json.dumps(serializers.serialize("json",ticketOrderQuery)))
                #return HttpResponse(serializers.serialize("json",[ticket]))
                logged_ticket.save()
                deletedSeatIDs.append(tick)
                ticket.canceledDate = datetime.datetime.now()
                ticket.canceledByUser = user
                ticket.save()
                queryLong = {'$and':[{'items.0':{'$exists':'true'}},{'session_id':session_id.encode('utf-8')}]}, {'$pull': {'items':{'emb_seat.uid': tick.encode('utf-8')}}},{"multi":'true'}
                query = {"session_id":session_id.encode('utf-8')}, {"$pull" : {"items" : {'emb_seat.uid' : tick.encode('utf-8')}}}
                #return HttpResponse(queryLong)
                # conn = mongo_connect()
                # k = conn["testing_ticketorder"]
                # k.update({'$and':[{'items.0':{'$exists':True}},{'session_id':session_id.encode('utf-8')}]}, {'$pull': {'items':{'emb_seat.uid': tick.encode('utf-8')}}},True)
                try:
                    TicketOrder.objects.raw_update({'$and':[{'items.0':{'$exists':True}},{'session_id':session_id.encode('utf-8')}]}, {'$pull': {'items':{'emb_seat.uid': tick.encode('utf-8')}}})
                except:
                    pass
                try:
                    Session.objects.raw_update({"_id": session_id.encode('utf-8')}, {"$pull" : {"tickets" : {'emb_seat.uid' : tick.encode('utf-8')}}}) #!!!!!!!!  #{"seat_id":tick}}})#
                except:
                    pass
                deleted = deleted + 1
                ticketOrder = TicketOrder.objects.get(id=ticketOrderID)

                #WARNING if canceling tickets which had reservation, what happens then?
                if not ticketOrder.reservation:
                    cumulative_returned_amount = cumulative_returned_amount + ticket.initial_price
                    cumulative_returned_discount = cumulative_returned_discount + ticket.discount_amount
                elif ticketOrder.payment_by_creditcard > 0 or ticketOrder.payment_by_cash > 0 or ticketOrder.payment_by_giftcard > 0:
                    if ticket.initial_price and ticket.discount_amount:
                        cumulative_returned_amount = cumulative_returned_amount + ticket.initial_price
                        cumulative_returned_discount = cumulative_returned_discount + ticket.discount_amount

                break

    if deleted == len(tickets):
        response["status"] = "Ok!"
        try:
            sent_command_withseats("free", user, session_id, seatsRaw, csrftoken)
        except:
            pass

        #WARNING if canceling tickets which had reservation, what happens then?
        if ticketOrder.payment_by_creditcard > 0 or ticketOrder.payment_by_cash > 0 or ticketOrder.payment_by_giftcard > 0: #not ticketOrder.reservation:
            cashReturn = 0
            cardReturn = 0
            giftReturn = 0
            if ticketOrder and ticketOrder.discounted_amount and ticketOrder.initial_price:
                total_discounted_sale_price = cumulative_returned_amount - cumulative_returned_discount
                ticketOrder.discount_amount = ticketOrder.discount_amount - cumulative_returned_discount
                ticketOrder.initial_price = ticketOrder.initial_price - cumulative_returned_amount
                ticketOrder.discounted_amount = ticketOrder.discounted_amount - total_discounted_sale_price
                if ticketOrder.payment_by_cash >= total_discounted_sale_price:
                    ticketOrder.payment_by_cash = ticketOrder.payment_by_cash - total_discounted_sale_price
                    cashReturn = total_discounted_sale_price
                elif ticketOrder.payment_by_creditcard >= total_discounted_sale_price:
                    ticketOrder.payment_by_creditcard = ticketOrder.payment_by_creditcard - total_discounted_sale_price
                    cardReturn = total_discounted_sale_price
                elif ticketOrder.payment_by_giftcard >= total_discounted_sale_price:
                    ticketOrder.payment_by_giftcard = ticketOrder.payment_by_giftcard - total_discounted_sale_price
                    giftReturn = total_discounted_sale_price
                else:
                    cashReturn = cashReturn + ticketOrder.payment_by_cash
                    total_discounted_sale_price = total_discounted_sale_price - ticketOrder.payment_by_cash
                    ticketOrder.payment_by_cash = 0.0
                    if total_discounted_sale_price >= ticketOrder.payment_by_creditcard:
                        total_discounted_sale_price = total_discounted_sale_price - ticketOrder.payment_by_creditcard
                        cardReturn = cardReturn + ticketOrder.payment_by_creditcard
                        ticketOrder.payment_by_creditcard = 0.0
                        if total_discounted_sale_price > 0.0:
                            giftReturn = giftReturn + ticketOrder.payment_by_giftcard - total_discounted_sale_price
                            ticketOrder.payment_by_giftcard = ticketOrder.payment_by_giftcard - total_discounted_sale_price
                    else:
                        cardReturn = cardReturn + ticketOrder.payment_by_creditcard - total_discounted_sale_price
                        ticketOrder.payment_by_creditcard = ticketOrder.payment_by_creditcard - total_discounted_sale_price
                #return HttpResponse(ticketOrder)
                ticketOrder.save()
                response['returnData'] = {"cashReturn":cashReturn, "cardReturn":cardReturn, "giftReturn":giftReturn}


    response['deleted'] = "%s" % (deleted)
    response['deletedIDs'] = deletedSeatIDs

    return HttpResponse(json.dumps(response))



def create_log_ticket(ticket):
    logticket = CancelledTicketsLog.objects.create(uid=ticket.uid, created=ticket.created, session=ticket.session,
                                                   ticket_order=ticket.ticket_order, seat=ticket.seat,
                                                   stype=ticket.stype, cancel_time=datetime.datetime.now())

    return logticket





def ticketSelectsAPI(request, hall, date, time, seat_id):
    log_request(request)
    session_id=hall+"/"+date+"/"+time
    tickets = Session.objects.get(uid=session_id).tickets

    order="{status, empty}"

    if tickets and session_id:
        #return HttpResponse ("JIGARI")
        for ticket in tickets:
            if (seat_id==ticket.emb_seat.uid):
                order = TicketOrder.objects.get(pk=ticket.ticket_order_id)
                order.cart_log = None
                order_items = []
                for item in order.items:
                    jsonOfItem = json.loads(serializers.serialize("json", [item]))
                    order_items.append(jsonOfItem)
                order.items = order_items
                if order.box_office_items:
                    box_items = []
                    for boxOrder in order.box_office_items:
                        jsonOfBoxItem = json.loads(serializers.serialize("json", [boxOrder]))
                        box_items.append(jsonOfBoxItem)
                    order.box_office_items = box_items

                return  HttpResponse(json.dumps(json.loads(serializers.serialize("json", [order]))))
    else:
        return HttpResponse ("Naaa")

def log_request(request):
    try:
        logInstance = AccessLogger.objects.create(client_ip=get_client_ip(request), accessed=datetime.datetime.now(), url_accessed=request.build_absolute_uri(), user=request.user)
        logInstance.save()
    except:
        pass

def get_client_ip(request):
    request.META['REMOTE_ADDR'] = request.META['HTTP_X_REAL_IP']
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip



def create_token(username, ip, hard_id):
    Token.objects.create(user_id=username, hardware_id=hard_id, ip=ip, logged_date=datetime.datetime.now()).save()

def get_report_for_agent(request):
    date_from_offset = request.POST["date_from_offset"]
    date_to_offset = request.POST["date_to_offset"]

    today = datetime.datetime.now()
    today = today.replace(hour=0, minute=0, second=0)
    date_from = today + timedelta(int(date_from_offset))
    date_from = date_from.replace(hour=03, minute=0, second=0)
    date_to = today + timedelta(int(date_to_offset))
    date_to = date_to.replace(hour=03, minute=0, second=0)

    csrftoken = request.COOKIES['csrftoken']
    user = request.user
    if not user:
        return HttpResponse("Missing User")
    shiftagent = Shift.objects.get(user=user, csrftoken=csrftoken).agent
    agent = Agent.objects.get(id=shiftagent).id

    orders = TicketOrder.objects.raw_query({"$and":[{"agent.id":ObjectId(agent)}, {"date_sold":{"$gte":date_from}},{"date_sold":{"$lte":date_to}}]})#{"created":{"$gte":date_from}},{"created":{"$lte":date_to}}]})

    if not orders:
        response = {"status":"ok"}
        response['data'] = {'cash':0, 'creditcard':0, 'giftcard':0.0}
        return HttpResponse(json.dumps(response))

    total_cash = orders.aggregate(sum=Sum('payment_by_cash'))
    total_credit_card = orders.aggregate(sum=Sum('payment_by_creditcard'))
    total_gift_card = orders.aggregate(sum=Sum('payment_by_giftcard'))

    number_of_small_glasses = 0
    number_of_large_glases = 0
    number_of_evoucher = 0

    for order in orders:
        order.cart_log = None
        conn = mongo_connect()
        k = conn["testing_boxofficeproductitem"]
        query = {"$and":[{"ticket_order_id":ObjectId(order.id)}, {"emb_product.sku":"cinema_3d_glass_small"}]}
        boxitems = k.find(query) #BoxOfficeProductItem.objects.raw_query(query)
        for item in boxitems:
            number_of_small_glasses = number_of_small_glasses + int(item['quantity'])

        query = {"$and":[{"ticket_order_id":ObjectId(order.id)}, {"emb_product.sku":"cinema_3d_glass_large"}]}
        boxitems = k.find(query) #BoxOfficeProductItem.objects.raw_query(query)
        for item in boxitems:
            number_of_large_glases = number_of_large_glases + int(item['quantity'])

        query = {"$and":[{"ticket_order_id":ObjectId(order.id)}, {"emb_product.sku":"boxoffice_economy_voucher"}]}
        boxitems = k.find(query) #BoxOfficeProductItem.objects.raw_query(query)
        for item in boxitems:
            number_of_evoucher = number_of_evoucher + int(item['quantity'])

    # conn = mongo_connect()
    # k = conn["testing_ticketorder"]
    # pline_large = [{"$match":{"$and":[{"agent.id":ObjectId(agent)}, {"created":{"$gte":date_from}},{"created":{"$lte":date_to}}, {"box_office_items.emb_product.sku":"cinema_3d_glass_large"}]}},  {"$unwind":"$box_office_items"}, {"$group":{"_id":"null","large_glasses_sum":{"$sum":"$box_office_items.quantity"}}}]
    # pline_small = [{"$match":{"$and":[{"agent.id":ObjectId(agent)}, {"created":{"$gte":date_from}},{"created":{"$lte":date_to}}, {"box_office_items.emb_product.sku":"cinema_3d_glass_small"}]}},  {"$unwind":"$box_office_items"}, {"$group":{"_id":"null","small_glasses_sum":{"$sum":"$box_office_items.quantity"}}}]
    # pline_evoucher = [{"$match":{"$and":[{"agent.id":ObjectId(agent)}, {"created":{"$gte":date_from}},{"created":{"$lte":date_to}}, {"box_office_items.emb_product.sku":"boxoffice_economy_voucher"}]}},  {"$unwind":"$box_office_items"}, {"$group":{"_id":"null","evoucher_sum":{"$sum":"$box_office_items.quantity"}}}]
    # small = k.aggregate(pline_small)["result"]
    # large = k.aggregate(pline_large)["result"]
    # evoucher = k.aggregate(pline_evoucher)["result"]

    # if len(small) > 0:
    #     number_of_small_glasses = small[0]['small_glasses_sum']
    # if len(large) > 0:
    #     number_of_large_glases = large[0]['large_glasses_sum']
    # if len(evoucher) > 0:
    #     number_of_evoucher = evoucher[0]['evoucher_sum']

    response = {"status":"ok"}
    response['data'] = {'cash':total_cash, 'creditcard':total_credit_card, 'giftcard':total_gift_card, 'glasses_small':number_of_small_glasses, 'glasses_large':number_of_large_glases, "evoucher":number_of_evoucher}
    return HttpResponse(json.dumps(response))

def mongo_connect():
    conn = Connection()
    db = conn[DATABASES["default"]["NAME"]]
    return db

@csrf_exempt
def create_external_user(request):
    username = request.POST['username']
    password = request.POST['password']
    email = request.POST['email']
    name = request.POST['name']
    surname = request.POST['surname']
    phone = request.POST['phone']

    try:
        u = User.objects.get(username=username)
        if u:
            response = {"status":"failed", "message":u"მოხმარებელი ამ სახელით უკვე არსებობს"}
            return HttpResponse(json.dumps(response))
    except User.DoesNotExist:
        user = User.objects.create_user(username, email, password)
        customer = None

        try:
            customer = Customer.objects.get(name=name, surname=surname, phone=phone, email=email)
        except Customer.DoesNotExist:
            cust = Customer.objects.create(name=name, surname=surname, phone=phone, email=email)
            cust.save()
            customer = Customer.objects.get(name=name, surname=surname, phone=phone, email=email)

        response = {"status":"ok"}
        return HttpResponse(json.dumps(response))




@csrf_exempt
def loginUser(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            # Redirect to a success page.
            token = django.middleware.csrf.get_token(request)
            if not user.is_staff:
                if 'source' in request.POST:
                    if request.POST['source'] == 'cavea.ge':
                        #cavea.ge agent
                        Shift.objects.create(agent="554cebb6956167b76e9f1e09", csrftoken=token,user=username).save()
                    else:
                        #ingorokva testing agent - unidentified
                        Shift.objects.create(agent="5528dadb95616746561877f6", csrftoken=token,user=username).save()
                else:
                    Shift.objects.create(agent="5528dadb95616746561877f6", csrftoken=token,user=username).save()
            else:
                if 'source' in request.POST:
                    if request.POST['source'] == 'cavea.ge':
                        #cavea.ge agent
                        Shift.objects.create(agent="554cebb6956167b76e9f1e09", csrftoken=token,user=username).save()
                    else:
                        #default is TESTING (ingorokva)
                        Shift.objects.create(agent="5528a668956167256e821536", csrftoken=token,user=username).save()
                elif 'agent' in request.POST:
                    agent = request.POST['agent']
                    Shift.objects.create(agent=agent, csrftoken=token,user=username).save()
                elif user.username == 'shuxtura':
                    Shift.objects.create(agent="5528a6bb956167256e821538", csrftoken=token,user=username).save()
                elif user.username == 'CaveaiOS':
                    Shift.objects.create(agent="5528dadb95616746561877f6", csrftoken=token,user=username).save()
                elif user.username == 'ubuntu':
                    Shift.objects.create(agent="554cebb6956167b76e9f1e09", csrftoken=token,user=username).save()
                else:
                    #default is TESTING (ingorokva)
                    Shift.objects.create(agent="5528a668956167256e821536", csrftoken=token,user=username).save()

            response = {'status':'Success', 'token':token, 'session':request.session.session_key}

            try:
                customer = Customer.objects.get(email=username)
                response.update({'name':customer.name, 'surname':customer.surname, 'phone': customer.phone, 'email': customer.email})
                return HttpResponse(json.dumps(response))
            except:
                return HttpResponse(json.dumps(response))

        else:
            # Return a 'disabled account' error message
            return HttpResponse([json.dumps({'status':'Failed', 'message':u'თქვენს ანგარიშს პრობლემა აქვს, დაგვიკავშირდით 2 555 000'})])
    else:
        # Return an 'invalid login' error message.
        return HttpResponse([json.dumps({'status':'Failed', 'message':u'მონაცემები არასწორია'})])

def get_listof_box_agents(request):
    optionList = ["BO", "TST", "WEB", "IAP"]
    agents = Agent.objects.filter(ttype__in=optionList)
    response = {'status': 'Failed'}
    if agents:
        response['status'] = 'Ok'
        agentsjson = serializers.serialize("json", agents)
        response['data'] = agentsjson
    return HttpResponse(json.dumps(response))

@csrf_exempt
def find_ticket(request):
    ticket_id = request.POST['ticket_id']
    try:
        ticket = Ticket.objects.get(id=ticket_id)
        hall = ticket.session.hall.title
        movie = ticket.session.movie.name
        theater = ticket.session.hall.theater.name
        session_datetime = str(ticket.session.start_time)
        row = ticket.emb_seat.row
        seat = ticket.emb_seat.seat
        price = ticket.price
        discount = ticket.discount_amount
        ticketJSON = {"cinema":theater, "hall":hall,"movie":movie,"session_datetime":session_datetime, "price":price,"discount":discount, "row":row, "seat":seat}
        response = {"status":"OK", "data":ticketJSON}
        return HttpResponse(json.dumps(response))

    except Exception,e:
        response = {"status":"failed", "message":"No ticket with such id", "exception":str(e)}
        return HttpResponse(json.dumps(response))

#@csrf_exempt
def find_order(request):
    date_from = request.POST['date_from']
    order_id = request.POST['order_id']
    name = request.POST['name']
    surname = request.POST['surname']
    mobile = request.POST['mobile']
    email = request.POST['email']
    reservation_pin = request.POST['reservation_pin']

    filters = {}

    if date_from == 'YESTERDAY':
        today = datetime.datetime.now()#today()
        yesterday = today - timedelta(1)
        sessions=Session.objects.filter(start_time__gt=yesterday)
        session_times = []
        for session in sessions:
            session_times.append(session.uid)
        filters = {"session_id":{"$in":session_times}}


    if reservation_pin != 'ALL':
        regx = re.compile("^%s"%reservation_pin, re.IGNORECASE)
        filters.update({"reservation.pin":regx})
    if email != 'ALL':
        regx = re.compile("^%s"%email, re.IGNORECASE)
        filters.update({"reservation.email":regx})
    if mobile != 'ALL':
        regx = re.compile("^%s"%mobile, re.IGNORECASE)
        filters.update({"reservation.mobile":regx})
    if surname != 'ALL':
        regx = re.compile("^%s"%surname, re.IGNORECASE)
        filters.update({"reservation.surname":regx})
    if name != 'ALL':
        regx = re.compile("^%s"%name, re.IGNORECASE)
        filters.update({"reservation.name":regx})
    if order_id != 'ALL':
        filters.update({"_id":ObjectId(order_id)})


    #return HttpResponse([filters])

    ticketReservations = None
    if filters == None:
        ticketReservations = TicketOrder.objects.all()
    else:
        ticketReservations = TicketOrder.objects.raw_query(filters)
    #return HttpResponse(serializers.serialize("json",ticketReservations))

    response = {"status":"ok", "data": "NA"}
    if ticketReservations:
        for ticketOrder in ticketReservations:
            ticketOrder.cart_log = None
            order_items = []
            for item in ticketOrder.items:
                jsonOfItem = json.loads(serializers.serialize("json", [item]))
                order_items.append(jsonOfItem)
            ticketOrder.items = order_items
            if ticketOrder.box_office_items:
                box_items = []
                for boxOrder in ticketOrder.box_office_items:
                    jsonOfBoxItem = json.loads(serializers.serialize("json", [boxOrder]))
                    box_items.append(jsonOfBoxItem)
                ticketOrder.box_office_items = box_items
            ticketOrder.reservation = json.loads(serializers.serialize("json", [ticketOrder.reservation]))
        if len(ticketReservations) > 0:
            session_hall_cinema = []
            for ticketReservation in ticketReservations:
                cinema_id = ticketReservation.session.hall.theater.id
                hall_id = ticketReservation.session.hall.id
                session_id = ticketReservation.session.uid
                session_hall_cinema.append({ticketReservation.id:{"cinema_id":cinema_id, "hall_ud":hall_id,"session_id":session_id}})

            response["data"] = serializers.serialize("json", ticketReservations) #
            response["combo_info"] = json.dumps(session_hall_cinema)
    return HttpResponse(json.dumps(response))

def find_orders_bought_for_user(request):
    user = request.user.username
    #user = request.GET['user']
    try:
        orders = TicketOrder.objects.filter((~Q(reservation_sold=None) | Q(reservation=None)), user=user)
        order_ids = []
        for order in orders:
            items_count = 0
            if order.items:
                items_count = len(order.items)
            order_dics = {"id":order.id, "items":items_count, "date":str(order.session.start_time)}
            order_ids.append(order_dics)
        response = {"status":"OK", "message":"Orders Found","data":order_ids}
        return HttpResponse(json.dumps(response))
    except Exception,e:
        response = {"status":"fail", "message":"No Orders Found", "Exception":str(e)}
        return HttpResponse(json.dumps(response))

def find_reservations_for_user(request):
    user = request.user.username
    #user = request.GET['user']
    try:
        orders = TicketOrder.objects.filter(~Q(reservation=None), user=user)
        order_ids = []
        for order in orders:
            items_count = 0
            if order.items:
                items_count = len(order.items)
            order_dics = {"id":order.id, "items":items_count, "date":str(order.session.start_time), "pin":order.reservation.pin}
            order_ids.append(order_dics)
        response = {"status":"OK", "message":"Orders Found","data":order_ids}
        return HttpResponse(json.dumps(response))
    except Exception,e:
        response = {"status":"fail", "message":"No Orders Found", "Exception":str(e)}
        return HttpResponse(json.dumps(response))



def find_customer(request):
    name = request.POST['name']
    surname = request.POST['surname']
    mobile = request.POST['mobile']
    email = request.POST['email']
    national_id = request.POST['national_id']

    filters = {}

    if email != 'ALL':
        regx = re.compile("^%s"%email, re.IGNORECASE)
        filters.update({"email":regx})
    if mobile != 'ALL':
        regx = re.compile("^%s"%mobile, re.IGNORECASE)
        filters.update({"phone":regx})
    if surname != 'ALL':
        regx = re.compile("^%s"%surname, re.IGNORECASE)
        filters.update({"surname":regx})
    if name != 'ALL':
        regx = re.compile("^%s"%name, re.IGNORECASE)
        filters.update({"name":regx})
    if national_id != 'ALL':
        regx = re.compile("^%s"%national_id, re.IGNORECASE)
        filters.update({"national_id":regx})

    #return HttpResponse([filters])

    customers = None
    if filters == None:
        customers = Customer.objects.all()
    else:
        customers = Customer.objects.raw_query(filters)
    #return HttpResponse(serializers.serialize("json",ticketReservations))

    response = {"status":"ok", "data": "NA"}
    if customers:
        response["data"] = serializers.serialize("json", customers)
    return HttpResponse(json.dumps(response))

@csrf_exempt
def get_movie_details(request):
    movieID = request.POST['movieID']
    details = None
    response = {"status":"failed"}
    try:
        details = MovieDescription.objects.get(movie=ObjectId(movieID))
    except:
        response["message"] = "Movie details not found"
        return HttpResponse(json.dumps(response))

    response['status'] = "OK"
    response['data'] = serializers.serialize("json", [details])
    return HttpResponse(json.dumps(response))

@csrf_exempt
def get_movie_details_english(request):
    movieID = request.POST['movieID']
    details = None
    response = {"status":"failed"}
    try:
        details = MovieDescriptionEnglish.objects.get(movie=ObjectId(movieID))
    except:
        response["message"] = "Movie details not found"
        return HttpResponse(json.dumps(response))

    response['status'] = "OK"
    response['data'] = serializers.serialize("json", [details])
    return HttpResponse(json.dumps(response))

def five_minute_cron(request):

    return HttpResponse(check_online_orders_to_cancel())

def check_expired_reservations(request):
    try:
        dateNow = datetime.datetime.now() + datetime.timedelta(hours=1)
        sessionsExpiredOrClose = Session.objects.filter(start_time__lte=dateNow)
        ordersArray = []
        totalCancelCount = 0
        ticketCancelResponses =[]
        for session in sessionsExpiredOrClose:
            try:
                #TO-DO - CANCEL ONLY ONES NOT SOLD
                orders = TicketOrder.objects.filter(~Q(reservation=None), ~Q(reservation_sold=None),session=session)
                for order in orders:
                    if order.reservation:
                        seatsArray = []
                        for ticket in order.items:
                            seatsArray.append(ticket.emb_seat.uid)
                        if len(seatsArray)>0:
                            totalCancelCount = totalCancelCount + len(seatsArray)
                            ticketCancelResponses.append(ticketcancelapi_utility(session.uid, json.dumps(seatsArray), 'DjangoCron', 'nocookie'))
            except Exception, e:
                return HttpResponse(str(e))
        responseString = 'Success %s' % totalCancelCount
        return HttpResponse(ticketCancelResponses)
        #pass
        #ordersWithReservations = TicketOrder.objects.filter(~Q(reservation=None), )
    except Exception, e:
        return HttpResponse(str(e))
    return HttpResponse("Done")

def expire_reservations():
    try:
        dateNow = datetime.datetime.now() + datetime.timedelta(minutes=50)
        sessionsExpiredOrClose = Session.objects.filter(start_time__lte=dateNow)
        ordersArray = []
        totalCancelCount = 0
        ticketCancelResponses =[]
        for session in sessionsExpiredOrClose:
            try:
                orders = TicketOrder.objects.filter(~Q(reservation=None) & Q(reservation_sold=None), session=session)
                for order in orders:
                    if order.reservation:
                        seatsArray = []
                        for ticket in order.items:
                            seatsArray.append(ticket.emb_seat.uid)
                        if len(seatsArray)>0:
                            totalCancelCount = totalCancelCount + len(seatsArray)
                            ticketCancelResponses.append(ticketcancelapi_utility(session.uid, json.dumps(seatsArray), 'DjangoCron', 'nocookie'))
            except Exception, e:
                return HttpResponse(str(e))
        responseString = 'Success %s' % totalCancelCount
        return HttpResponse(ticketCancelResponses)
        #pass
        #ordersWithReservations = TicketOrder.objects.filter(~Q(reservation=None), )
    except Exception, e:
        return HttpResponse(str(e))
    return HttpResponse("Done")


def check_online_orders_to_cancel():
    #cancel bookings
    expire_reservations()

    #cancel expired blocked tickets
    try:
        expire_blocked_seats()
    except Exception, e:
        print >>sys.stderr, str(e)

    #cancel unpaid orders
    cutoff_date = datetime.datetime.now() - datetime.timedelta(minutes=10)
    try:
        returns = []
        onlineOrders = OnlineCardPayment.objects.filter(canceled_date=None, created__lte=cutoff_date)
        for onlineOr in onlineOrders:
            transaction_userside_id = onlineOr.transaction_userside_id
            #return transaction_userside_id
            try:
                returns.append(cancel_online_order_utility(transaction_userside_id))
                #return "Success"
            except Exception, e:
                return e
        return returns
    except:
        return "failed Order Find"


#online payment gateway through tbc
@csrf_exempt
def OrderPayOnline(request):
    user = request.user
    amount = 99999
    description = 'Missing Amount'
    if 'amount' in request.POST and 'description' in request.POST:
        if float(request.POST['amount']) > 150:
            return HttpResponse(compileResponseHTML('Can not order so many tickets'))
        amount = int(float(request.POST["amount"])*100)
        description = request.POST["description"]

    transaction_source = ''
    transaction_source_code = ''
    if 'source' in request.POST:
        transaction_source = request.POST['source']
        transaction_source_code = request.POST['source_code']
        session_id = request.POST["session_id"]
        if (transaction_source == 'iOSAPP') and (not user.is_staff) and ('rus' in session_id or 'ami' in session_id):
            responseHTML = compileResponseHTML("At this moment only Cavea Can be Booked or Bought")#ამ მოენტში მხოლოდ Cavea-ს ყიდვა/დაჯავშვნაა შესაძლებელი")
            return HttpResponse(responseHTML)
    else:
        response = {"status":"error"}
        return HttpResponse(json.dumps(response))

    fullPost = {'command' : "v", 'amount' : amount,'currency' : 981,'client_ip_addr' : "31.146.174.10",'description' : description,'language' : "GEO"}
    response = cStringIO.StringIO()
    #31.146.174.10
    encodedPost = urllib.urlencode(fullPost)
    curl = pycurl.Curl()
    curl.setopt(pycurl.POSTFIELDS, encodedPost)
    curl.setopt(pycurl.URL, "https://securepay.ufc.ge:18443/ecomm2/MerchantHandler")
    curl.setopt(pycurl.SSLCERT,"/usr/share/nginx/html/tbconline/TBCCert.pem")
    curl.setopt(pycurl.SSLKEY,"/usr/share/nginx/html/tbconline/TBCCert.pem")
    curl.setopt(pycurl.SSLKEYPASSWD,"MYfkghc6dBVb37gfHB")
    curl.setopt(pycurl.SSL_VERIFYPEER, 0)
    curl.setopt(pycurl.SSL_VERIFYHOST, 0)
    curl.setopt(pycurl.WRITEFUNCTION, response.write)
    #curl.setopt(pycurl.CAINFO, "/path/to/updated-certificate-chain.crt")
    curl.perform()
    curl.close()

    transactionID = response.getvalue()[16:]
    customer = None

    try:
        customer = Customer.objects.get(email=user)
    except:
        pass
        # try:
        #     customer = Customer.objects.get(name=user)
        # except:
        #     pass
    orderCreateResponse = None

    try:
        orderCreateResponse = json.loads(TicketBuyApi(request).content)
        orderID = ''
        if 'ticketOrderID' in orderCreateResponse:
            orderID = orderCreateResponse['ticketOrderID']
        else:
            if transaction_source == 'iOSAPP':
                # document.location.href = "caveaurlscheme://DoSomething"
                htmlString = '''
                    <html>
                    <head>
                    <script type="text/javascript" src="/static/js/jquery-1.8.3.min.js"></script>
                    <script>
                            var init = function(){
                                document.getElementById("forma").submit();
                            }
                            $(document).ready(init);

                    </script>
                    </head>
                    <body>
                    <form name='returnform' id='forma' action='caveaurlscheme://{"status":"%s", "transaction_id":"%s", "result":"%s","message":%s}' method='POST'>
                            <noscript>
                            <center>Please  Click on Submit if not Redirected in several Seconds<br>
                            <input type='submit' name='submit' value='Submit'></center>
                            </noscript>
                    </body>
                    </html>

                ''' % ("Failed", transactionID, "Failed", orderCreateResponse['message'])
                return HttpResponse(htmlString)
            elif payment.transaction_source == 'cavea.ge':
                return compileWebsiteHTMLResponse(None, orderCreateResponse['message'])
    except:
        return HttpResponse("EXCEPT")
        orderID = ''
        if 'ticketOrderID' in orderCreateResponse:
            orderID = orderCreateResponse['ticketOrderID']
        else:
            if transaction_source == 'iOSAPP':
                # document.location.href = "caveaurlscheme://DoSomething"
                htmlString = '''
                    <html>
                    <head>
                    <script type="text/javascript" src="/static/js/jquery-1.8.3.min.js"></script>
                    <script>
                            var init = function(){
                                document.getElementById("forma").submit();
                            }
                            $(document).ready(init);

                    </script>
                    </head>
                    <body>
                    <form name='returnform' id='forma' action='caveaurlscheme://{"status":"%s", "transaction_id":"%s", "result":"%s","message":"%s"}' method='POST'>
                            <noscript>
                            <center>Please Click on Submit if not Redirected in several Seconds<br>
                            <input type='submit' name='submit' value='Submit'></center>
                            </noscript>
                    </body>
                    </html>

                ''' % ("Failed", transactionID, "Failed", "Problem With Order")
                return HttpResponse(htmlString)
            elif payment.transaction_source == 'cavea.ge':
                return compileWebsiteHTMLResponse(None, "Problem With Order")


    OnlineCardPayment.objects.create(trans_id=transactionID, order_id=orderID, amount = amount, description = description, transaction_source=transaction_source, transaction_userside_id=transaction_source_code,customer=customer, user=user)

    #return HttpResponse(transactionID)

    html = """
    <html>
        <head>
        <script type="text/javascript" src="/static/js/jquery-1.8.3.min.js"></script>
        <script>
            var init = function(){
                document.getElementById("forma").submit();
            }
            $(document).ready(init);
        </script>
        </head>
        <body>
    <form name='returnform' id='forma' action='https://securepay.ufc.ge/ecomm2/ClientHandler' method='POST'>
                        <input type='hidden' name='trans_id' value='%s'>

                        <noscript>
                        <center>გთხოვთ მიაჭიროთ ღილაკს თუ რამდენიმე წამში ვერ გადახვედით გადახდის გვერდზე<br>
                        <input type='submit' name='submit' value='Submit'></center>
                        </noscript>
                        </form>

    </body>
    </html>
    """ % (transactionID)

    return HttpResponse(html)

def compileResponseHTML(message):
    htmlString = '''
                    <html>
                    <head>
                    <script type="text/javascript" src="/static/js/jquery-1.8.3.min.js"></script>
                    <script>
                            var init = function(){
                                document.getElementById("forma").submit();
                            }
                            $(document).ready(init);

                    </script>
                    </head>
                    <body>
                    <form name='returnform' id='forma' action='caveaurlscheme://{"status":"%s", "result":"%s","message":"%s"}' method='POST'>
                            <noscript>
                            <center>Please  Click on Submit if not Redirected in several Seconds<br>
                            <input type='submit' name='submit' value='Submit'></center>
                            </noscript>
                    </body>
                    </html>

                ''' % ("Failed", "Failed", message)
    return htmlString

def cancel_online_order_transaction(request):
    transaction_userside_id = request.POST['transaction_source_code']
    return cancel_online_order_utility(transaction_userside_id)


def cancel_online_order_utility(transaction_userside_id):
    try:
        onlineTransactions = OnlineCardPayment.objects.filter(transaction_userside_id=transaction_userside_id)
        for onlineTransaction in onlineTransactions:
            trans_id = onlineTransaction.trans_id

            #check payment information before canceling the order
            response = check_transaction_status(trans_id)
            response = ''.join(str(e) for e in response)
            responseDict = dict(re.findall(r'(\S+): (".*?"|\S+)', response))
            # return HttpResponse(response)
            # responseDict = dict(token.split(':') for token in shlex.split(response))
            #return HttpResponse([response['RESULT']])
            #return HttpResponse([responseDict])

            payment = OnlineCardPayment.objects.get(trans_id=trans_id)
            payment.card_result = responseDict['RESULT']
            payment.card_3dsecure = responseDict['3DSECURE']

            if 'CARD_NUMBER' in responseDict:
                payment.card_number = responseDict['CARD_NUMBER']
            if 'RESULT_PS' in responseDict:
                payment.card_result_ps = responseDict['RESULT_PS']
            if 'RESULT_CODE' in responseDict:
                payment.card_result_code = responseDict['RESULT_CODE']
            if 'RRN' in responseDict:
                payment.card_rrn = responseDict['RRN']
            if 'APPROVAL_CODE' in responseDict:
                payment.card_approval_code = responseDict['APPROVAL_CODE']
            if 'AAV' in responseDict:
                payment.card_aav = responseDict['AAV']
            if 'RECC_PMNT_ID' in responseDict:
                payment.card_recc_pmnt_id = responseDict['RECC_PMNT_ID']
            if 'RECC_PMNT_EXPIRY' in responseDict:
                payment.card_pmnt_expiry = responseDict['RECC_PMNT_EXPIRY']

            payment.save()
            if payment.card_result == 'OK':
                return HttpResponse(json.dumps({"status":"ok","message":"Payment already went through, so not canceling order"}))
            else:
                payment.canceled_date = datetime.datetime.now()

            order =  TicketOrder.objects.get(id=onlineTransaction.order_id)

            session_id = order.session.uid
            for ticket in order.items:
                ticket.canceledDate = datetime.datetime.now()
                ticket.canceledByUser = "OnlinePaymentFail"
                Session.objects.raw_update({"_id": session_id}, {"$pull" : {"tickets" : {'emb_seat.uid' : ticket.emb_seat.uid}}})
                ticket.save()
            order.delete()
        return HttpResponse(json.dumps({"status":"ok","message":"Successfully Canceled"}))
    except Exception, e:
        try:
            onlineTransaction = OnlineCardPayment.objects.filter(transaction_userside_id=transaction_userside_id)
            for transaction in onlineTransaction:
                transaction.canceled_date = datetime.datetime.now()
                transaction.save()
        except:
            pass
        return HttpResponse(json.dumps({"status":"failed","message":"no such transaction", "expt":str(e)}))

@csrf_exempt
def paymentok(request):

    response = {}

    for key, value in request.POST.iteritems():
        response.update({key:value})

    try:
        trans_id = request.POST['trans_id']
        response = check_transaction_status(trans_id)
        response = ''.join(str(e) for e in response)
        responseDict = dict(re.findall(r'(\S+): (".*?"|\S+)', response))
        # return HttpResponse(response)
        # responseDict = dict(token.split(':') for token in shlex.split(response))
        #return HttpResponse([response['RESULT']])
        #return HttpResponse([responseDict])

        payment = OnlineCardPayment.objects.get(trans_id=trans_id)
        payment.card_result = responseDict['RESULT']
        payment.card_3dsecure = responseDict['3DSECURE']

        if 'CARD_NUMBER' in responseDict:
            payment.card_number = responseDict['CARD_NUMBER']
        if 'RESULT_PS' in responseDict:
            payment.card_result_ps = responseDict['RESULT_PS']
        if 'RESULT_CODE' in responseDict:
            payment.card_result_code = responseDict['RESULT_CODE']
        if 'RRN' in responseDict:
            payment.card_rrn = responseDict['RRN']
        if 'APPROVAL_CODE' in responseDict:
            payment.card_approval_code = responseDict['APPROVAL_CODE']
        if 'AAV' in responseDict:
            payment.card_aav = responseDict['AAV']
        if 'RECC_PMNT_ID' in responseDict:
            payment.card_recc_pmnt_id = responseDict['RECC_PMNT_ID']
        if 'RECC_PMNT_EXPIRY' in responseDict:
            payment.card_pmnt_expiry = responseDict['RECC_PMNT_EXPIRY']

        payment.save()

        alertViewMessage = "Payment Declined"

        if payment.card_result != 'OK':
            # ORDER/CANCEL TICKETS/REMOVE TICKETS FROM SESSION
            order = TicketOrder.objects.get(id=payment.order_id)
            session_id = order.session.uid
            for ticket in order.items:
                ticket.canceledDate = datetime.datetime.now()
                ticket.canceledByUser = "OnlinePaymentFail"
                Session.objects.raw_update({"_id": session_id}, {"$pull" : {"tickets" : {'emb_seat.uid' : ticket.emb_seat.uid}}})
                ticket.save()
            order.delete()
        else:
            alertViewMessage = "Success, Please Check your email (Including Spam folder) for tickets@cinerp.com"
            user = payment.user
            if not '@' in user:
                return show_weborder_fororder(payment.order_id)
            try:
                if payment.transaction_source == 'iOSAPP':
                    send_ticket_to_user(None, user, 'DO-NOT-REPLY', 'Thank you, come again', payment.order_id)
                elif payment.transaction_source == 'cavea.ge':
                    try:
                        send_weborder_to_user(user,payment.order_id)
                    except:
                        pass
                    return show_weborder_fororder(payment.order_id)
            except:
                pass



        if payment.transaction_source == 'iOSAPP':
            # document.location.href = "caveaurlscheme://DoSomething"
            htmlString = '''
                <html>
                <head>
                <script type="text/javascript" src="/static/js/jquery-1.8.3.min.js"></script>
                <script>
                        var init = function(){
                            document.getElementById("forma").submit();
                        }
                        $(document).ready(init);

                </script>
                </head>
                <body>
                <form name='returnform' id='forma' action='caveaurlscheme://{"status":"%s", "transaction_id":"%s", "result":"%s","message":"%s"}' method='POST'>
                        <noscript>
                        <center>Please Click on Submit if not Redirected in several Seconds<br>
                        <input type='submit' name='submit' value='Submit'></center>
                        </noscript>
                </body>
                </html>

            ''' % (payment.card_result, trans_id,payment.card_result, alertViewMessage)
            return HttpResponse(htmlString)
        elif payment.transaction_source == 'cavea.ge':
            return compileWebsiteHTMLResponse(None, alertViewMessage)
        return HttpResponse([responseDict])
    except Exception, e:
        return HttpResponse([json.dumps(response),"</br></br>",e])
    return HttpResponse(json.dumps(response))
    # return HttpResponse([request.META])
    #return HttpResponse(request.COOKIES.keys())

def compileWebsiteHTMLResponse(status, message):
    htmlString = '''
    <html>
    <body>
    %s
    </body>
    </html>
    ''' % (message)
    return HttpResponse(htmlString)

@csrf_exempt
def check_transaction_status(transactionID):
    fullPost = {'command' : "c", 'client_ip_addr' : "31.146.174.10",'trans_id':transactionID}
    response = cStringIO.StringIO()
    #31.146.174.10
    encodedPost = urllib.urlencode(fullPost)
    curl = pycurl.Curl()
    curl.setopt(pycurl.POSTFIELDS, encodedPost)
    curl.setopt(pycurl.URL, "https://securepay.ufc.ge:18443/ecomm2/MerchantHandler")
    curl.setopt(pycurl.SSLCERT,"/usr/share/nginx/html/tbconline/TBCCert.pem")
    curl.setopt(pycurl.SSLKEY,"/usr/share/nginx/html/tbconline/TBCCert.pem")
    curl.setopt(pycurl.SSLKEYPASSWD,"MYfkghc6dBVb37gfHB")
    curl.setopt(pycurl.SSL_VERIFYPEER, 0)
    curl.setopt(pycurl.SSL_VERIFYHOST, 0)
    curl.setopt(pycurl.WRITEFUNCTION, response.write)
    #curl.setopt(pycurl.CAINFO, "/path/to/updated-certificate-chain.crt")
    curl.perform()
    curl.close()

    response = response.getvalue()
    return response


def close_card_day(request):
    fullPost = {'command' : "b"}
    response = cStringIO.StringIO()
    #31.146.174.10
    encodedPost = urllib.urlencode(fullPost)
    curl = pycurl.Curl()
    curl.setopt(pycurl.POSTFIELDS, encodedPost)
    curl.setopt(pycurl.URL, "https://securepay.ufc.ge:18443/ecomm2/MerchantHandler")
    curl.setopt(pycurl.SSLCERT,"/usr/share/nginx/html/tbconline/TBCCert.pem")
    curl.setopt(pycurl.SSLKEY,"/usr/share/nginx/html/tbconline/TBCCert.pem")
    curl.setopt(pycurl.SSLKEYPASSWD,"MYfkghc6dBVb37gfHB")
    curl.setopt(pycurl.SSL_VERIFYPEER, 0)
    curl.setopt(pycurl.SSL_VERIFYHOST, 0)
    curl.setopt(pycurl.WRITEFUNCTION, response.write)
    #curl.setopt(pycurl.CAINFO, "/path/to/updated-certificate-chain.crt")
    curl.perform()
    curl.close()

    response = response.getvalue()

    closeDayReport = OnlinePaymentsDayClose.objects.create(result_log=response)
    closeDayReport.save()
    return HttpResponse(response)

@csrf_exempt
def check_transaction_status_return(request, transactionID):
    transactionID = request.POST['trans_ID']
    fullPost = {'command' : "c", 'client_ip_addr' : "31.146.174.10",'trans_id':transactionID}
    response = cStringIO.StringIO()
    #31.146.174.10
    encodedPost = urllib.urlencode(fullPost)
    curl = pycurl.Curl()
    curl.setopt(pycurl.POSTFIELDS, encodedPost)
    curl.setopt(pycurl.URL, "https://securepay.ufc.ge:18443/ecomm2/MerchantHandler")
    curl.setopt(pycurl.SSLCERT,"/usr/share/nginx/html/tbconline/TBCCert.pem")
    curl.setopt(pycurl.SSLKEY,"/usr/share/nginx/html/tbconline/TBCCert.pem")
    curl.setopt(pycurl.SSLKEYPASSWD,"MYfkghc6dBVb37gfHB")
    curl.setopt(pycurl.SSL_VERIFYPEER, 0)
    curl.setopt(pycurl.SSL_VERIFYHOST, 0)
    curl.setopt(pycurl.WRITEFUNCTION, response.write)
    #curl.setopt(pycurl.CAINFO, "/path/to/updated-certificate-chain.crt")
    curl.perform()
    curl.close()

    response = response.getvalue()
    return HttpResponse(response)

@login_required
def list_online_app_orders(request):
    if not request.user.is_superuser:
        return HttpResponse("Thanks for visiting")
    today = datetime.datetime.now()
    onemonthback = today - timedelta(days=30)
    onlineOrders = OnlineCardPayment.objects.filter(created__gte=onemonthback, card_result='OK').order_by("-created")#.limit(100)#.sort(created=-1)
    html = '''
    <html>
        <head>
            <style>
            table, td {
                border: 1px solid black;
            }
            </style>
        </head>
    <table>
    <tr>
        <th>user</th>
        <th>Order ID</th>
        <th>Date</th>
        <th>Paid Online</th>
        <th>Paid Order Info</th>
        <th>Session</th>
        <th>Ticket Cound</th>
        <th>Row</th>
        <th>Seat</th>
    </tr>
    '''
    for order in onlineOrders:
        online_amount = order.amount
        order_paid = 0
        tickets_count = 0
        row = 0
        seat = 0
        session = 'NoSession'
        user = order.user
        created = order.created
        order_id = order.order_id
        try:
            ticketOrder = TicketOrder.objects.get(id=order_id)
            if not ticketOrder:
                order_id = 'Missing Order'
            else:
                order_paid = ticketOrder.payment_by_creditcard
                row = ticketOrder.items[0].emb_seat.row
                seat = ticketOrder.items[0].emb_seat.seat
                tickets_count = len(ticketOrder.items)
                session = ticketOrder.session
        except Exception, e:
            order_id = 'Missing Order'
            pass #return HttpResponse(str(e))
        if order_id != 'Missing Order':
            order_link = "https://cinerp.com/showonlineorder.api/%s" % (order_id)
            order_id = '''<a href="%s">%s</a>''' % (order_link,order_id)
        html = html + '''
        <tr>
        <td align="center">%s</td>
        <td align="center">%s</td>
        <td align="center">%s</td>
        <td align="center">%s</td>
        <td align="center">%s</td>
        <td align="center">%s</td>
        <td align="center">%s</td>
        <td align="center">%s</td>
        <td align="center">%s</td>

         </tr>
        ''' % (user, order_id, created, online_amount, order_paid, session, tickets_count, row, seat)

    html = html + '''
        </table>'''



    return HttpResponse(html)

def sent_command_withseats(command, user, session, seatsArray, token):
    if not user:
        user = 'DjangoServerDispatch'

    user = u'%s' % user
    user = user.decode('utf-8')

    subscribestring = json.dumps({'command':'subscribe','session':session,'user':user, 'token':token})#,
    subscribestring = '%s\n' % subscribestring

    commandstring = json.dumps({"command":command,"user":user,"session":session, "seats":seatsArray, "token":token})

    fullcommandstring = '%s%s' % (subscribestring, commandstring)
    return send_to_dispatch_sock_server(fullcommandstring)

def send_to_dispatch_sock_server(data):
    HOST, PORT = "localhost", 9001

    # Create a socket (SOCK_STREAM means a TCP socket)
    #return "MING"
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Connect to server and send data
    sock.connect((HOST, PORT))
    sock.send(data)
    sock.close()




# REPORTS ========================= REPORTS
# REPORTS ========================= REPORTS

@csrf_exempt
def getOrdersForAgent(request):
    try:
        agent_id = request.POST['agent_id']
        date_from = request.POST['date_from']
        date_to = request.POST['date_to']
        hall_id = request.POST['hall_id']
        movie_id = request.POST['movie_id']

        filtersArray = []

        if 'cinema_id' in request.POST:
            cinema_id = request.POST['cinema_id']
            if cinema_id != 'ALL':
                halls = Hall.objects.raw_query({"theater_id":ObjectId(cinema_id)})
                halls_array = []
                for hall in halls:
                    halls_array.append(ObjectId(hall.id))

                sessions = Session.objects.raw_query({"hall_id":{"$in":halls_array}})
                session_ids = []
                for session in sessions:
                    session_ids.append(session.uid)
                filtersArray.append({"session_id":{"$in":session_ids}})


        if hall_id != 'ALL':
            raw = {"hall_id":hall_id}
            sessions = Session.objects.raw_query({"hall_id":ObjectId(hall_id)})
            session_ids = []
            for session in sessions:
                session_ids.append(session.uid)
            filtersArray.append({"session_id":{"$in":session_ids}})

        if movie_id != 'ALL':
            sessions = Session.objects.raw_query({"movie_id":ObjectId(movie_id)})
            session_ids = []
            for session in sessions:
                session_ids.append(session.uid)
            filtersArray.append({"session_id":{"$in":session_ids}})

        if agent_id != 'ALL':
            filtersArray.append({"agent.id":ObjectId(agent_id)})

        if 'date_type' in request.POST:
            date_type = request.POST['date_type']
            if date_type == 'by_event':
                date_from = parser.parse(date_from)
                date_to = parser.parse(date_to)
                sessions = Session.objects.filter(Q(start_time__gte = date_from) & Q(start_time__lte=date_to))
                session_ids = []
                for session in sessions:
                    session_ids.append(session.uid)
                filtersArray.append({"session_id":{"$in":session_ids}})
            else:
                if date_from != 'ALL':
                    date_from = parser.parse(date_from)
                    filtersArray.append({"date_sold":{"$gte":date_from}})#"created":{"$gte":date_from}})
                if date_to != 'ALL':
                    date_to = parser.parse(date_to)
                    filtersArray.append({"date_sold":{"$lte":date_to}})#"created":{"$lte":date_to}})
        else:
            if date_from != 'ALL':
                date_from = parser.parse(date_from)
                filtersArray.append({"date_sold":{"$gte":date_from}})#"created":{"$gte":date_from}})
            if date_to != 'ALL':
                date_to = parser.parse(date_to)
                filtersArray.append({"date_sold":{"$lte":date_to}})#"created":{"$lte":date_to}})

        ticketReservations = list()
        if len(filtersArray)==0:
            ticketReservations = TicketOrder.objects.all()
        else:
            #ticketReservations = TicketOrder.objects.raw_query(filters)
            conn = mongo_connect()
            k = conn["testing_ticketorder"]
            fullQ = [{"$and": filtersArray}]
            #return HttpResponse(fullQ)
            cursor = list(k.find({"$and": filtersArray}))
            # cursor = pymongo.Connection().rustaveli.testing_ticketorder.find({"$and": filtersArray})
            # for item in cursor:
            #     ticketReservations.append([item.copy()])
            #for item in cursor:
            #    ticketReservations.extend(json.dumps(cursor, default=json_util.default))#json.dumps(item, sort_keys=True, indent=4, default=json_util.default))

            return HttpResponse(json.dumps(cursor, default=json_util.default))
        #return HttpResponse(serializers.serialize("json",ticketReservations))
        # returnArray = []
        # for elem in ticketReservations:
        #     returnArray.append(elem)

        return HttpResponse(ticketReservations)
    except Exception, e:
        return HttpResponse(str(e))

def mongo_do_connect():
    conn = Connection()
    db = conn[DATABASES["default"]["NAME"]]
    return db

def get_sessions_by_movie(request):
    movie_id = request.GET["movie_id"]

    movie_bundle = []
    try:
        today = datetime.datetime.now()
        today = today.replace(hour=5, minute=0, second=0)
        next_week = today + timedelta(7)
        next_week = next_week.replace(hour=5, minute=0, second=0)
        #server is 3 hours behind?
        #date_one_hour_past = today.replace(hour = today.hour()-1)
        conn = mongo_do_connect()

        sessions_k = conn["testing_session"]

        #sessions = sessions_k.aggregate([{"$match":{"$and":[{"start_time":{"$gte":today}},{"start_time":{"$lt":next_week}},{"movie_id":movie_id} ]}}, {"$sort":{"start_time":1}},{"$group":{"_id": "$start_time", "sessions":{"$push":{ "language":"$session_language","is_3d":"$is_3d","start_time":"$start_time", "id":"$_id"}}}}])
        sessions = sessions_k.aggregate([{"$match":{"$and":[{"start_time":{"$gte":today}},{"start_time":{"$lt":next_week}},{"movie_id":ObjectId(movie_id)} ]}}, {"$sort":{"start_time":-1}},{"$group":{"_id": {"$substr":["$_id",6,10]} , "sessions":{"$push":{ "language":"$session_language","is_3d":"$is_3d", "id":"$_id"}}}}])
        sessions = sessions["result"]
        sessions_bundle = json.dumps(sessions, default=json_util.default)

    except Exception,e:
        print str(e)

    return HttpResponse(sessions_bundle)


def fetch_thing(url, params, method):
    params = urllib.urlencode(params)
    if method=='POST':
        f = urllib.urlopen(url, params)
    else:
        f = urllib.urlopen(url+'?'+params)
    return (f.read(), f.code)


def get_orders(agent_id, date_from, date_to, hall_id, movie_id):
    vars = {}
    vars["agent_id"] = agent_id
    vars["date_from"] = date_from
    vars["date_to"] = date_to
    vars["hall_id"] = hall_id
    vars["movie_id"] = movie_id
    content, response_code = fetch_thing(
                              'http://localhost:8000/reportboxmanager.api/',
                               vars,
                              'POST')
    return content


def test_for_android(request):
	conn = mongo_do_connect()
	orders_k = conn["testing_hall"]
        halls = orders_k.find().distinct("_id")
	return HttpResponse(json.dumps({"list":list(halls)}, default=json_util.default))

def get_sales_report(request):

    date_from = datetime.datetime.strptime("01/04/2015", "%d/%m/%Y")
    date_to = datetime.datetime.strptime("15/06/2015", "%d/%m/%Y")
    conn = mongo_do_connect()
    orders_k = conn["testing_ticketorder"]
    sessions_k = conn["testing_session"]
    movies_k = conn["testing_movie"]

    res = {}

    movies = movies_k.aggregate([{"$match":{"film_category":{"$exists": "true"}}},{"$group": {"_id":"$film_category" ,"films":{"$push":{"id": "$_id"}}}}])
    movies = movies["result"]

    #
    html = ""
    dict = {}

    for movie in movies:
        category = movie["_id"]
        films = movie["films"]
        list_ = []
        for film in films:
            list_.append(ObjectId(film["id"]))
        dict.update({str(movie["_id"]):list_})


    sess_dict = {}

    for category, m_array in dict.iteritems():
        sessions = sessions_k.aggregate([{"$match":{"$and":[{"start_time":{"$gte":date_from}},{"start_time":{"$lt":date_to}},{"movie_id":{"$in": m_array}}]}}, {"$sort":{"start_time":1}},{"$group":{"_id":"$movie_id", "sessions":{"$push":{"sess_id": "$_id"}}}}])
        sessions = sessions["result"]
        for session in sessions:
            sess = session["sessions"]
            list_ = []
            for s in sess:
                list_.append(s["sess_id"])
            sess_dict.update({category: list_})

    g_dict = {}
    counter = 1
    output = ""
    for category,sess_ids in sess_dict.iteritems():
	output += "<br><br>"
	#{"$substr":["$created",0,10]}
        #orders = orders_k.aggregate([{"$match":{"$and":[{"created":{"$gte":date_from}},{"created":{"$lt":date_to}}, {"session_id":{"$in":sess_ids}}]}}, {"$sort":{"created":1}},{"$group":{"_id": {"$substr":["$created",0,7]} , "total":{"$sum":{"$add":["$payment_by_cash", "$payment_by_creditcard"]}}}}])
        orders = orders_k.aggregate([{"$match":{"$and":[{"created":{"$gte":date_from}},{"created":{"$lt":date_to}}, {"session_id":{"$in":sess_ids}}]}}, {"$sort":{"created":1}},{"$group":{"_id": {"$substr":["$created",0,7]} , "total_cash":{"$sum":"$payment_by_cash"},"total_card":{"$sum":"$payment_by_creditcard"}}}])
	#orders = orders_k.aggregate([{"$match":{"$and":[{"created":{"$gte":date_from}},{"created":{"$lt":date_to}}, {"session_id":{"$in":sess_ids}}]}}, {"$sort":{"created":1}},{"$group":{"_id": {"$substr":["$created",0,7]} , "total":{"$sum":"$payment_by_creditcard"}}}])
        g_dict.update({category: orders["result"]})
	#[doc for doc in orders_k.find({"session_id":{"$in":sess_ids}}, {"payment_by_cash":1})]
        #orders = orders_k.find({"$and":[{"created":{"$gte":date_from}},{"created":{"$lt":date_to}}, {"session_id":{"$in":sess_ids}}]}, {"payment_by_cash":1, "created":1}).sort("created")


	#g_dict.update({category: l})
    #,{"session_id":{"$regex":hall}}   , "sales": {"$push": {"session_id":"$session_id"}}
    #orders = orders_k.aggregate([{"$match":{"$and":[{"created":{"$gte":date_from}},{"created":{"$lt":date_to}},{"session_id":{"$regex":hall}}]}}, {"$sort":{"created":-1}},{"$group":{"_id":"$user" , "total":{"$sum":"$payment_by_creditcard"}}}])
    #return HttpResponse(json.dumps(movies, default=json_util.default))
    output = []
    print g_dict
    for sess, values in g_dict.iteritems():
	total_card = 0
        total_cash = 0
	for v in values:
		print v["total_card"]
		total_card += v["total_card"]
		total_cash += v["total_cash"]
	print "total is "+str(total_card)
	print "total is "+str(total_cash)
	if total_cash or total_card:
		output.append({"cat":sess, "card":total_card, "cash":total_cash})
	#output.append({"cat":values["total_card"]})
    #return HttpResponse(json.dumps(output, default=json_util.default))

    return render(request, "reports/sales_by_category.html", {"category": output})


def get_occupancy_rates(request):


    date_from = datetime.datetime.strptime("01/02/2015", "%d/%m/%Y")
    date_to = datetime.datetime.strptime("10/06/2015", "%d/%m/%Y")
    conn = mongo_do_connect()
    sessions_k = conn["testing_session"]
    movies_k = conn["testing_movie"]
    halls_k = conn["testing_hall"]
    halls = {}
    halls_r = list(halls_k.find({"name":{"$regex":"cav"}}))
    for hall in halls_r:
       l = []
       l.append(hall["name"])
       l.append(hall["number_of_seats"])
       halls.update({hall["_id"]:l})
    movie_ids = []
    movie_dict = {}
    movies = list(movies_k.find({},{"name_english":1}))
    #halls = {ObjectId("54ecb14f95616782372bb0a8"):["cav_1",253],ObjectId("54ecb17f95616782372bb0aa"):["cav_2",162],ObjectId("54ecb1a795616782372bb0ac"):["cav_3",118],ObjectId("54ecb1c895616782372bb0ae"):["cav_4",98],ObjectId("54ecb1ee95616782372bb0b0"):["cav_5",127]}
    for movie in movies:
       movie_ids.append(ObjectId(movie["_id"]))
       movie_dict.update({str(movie["_id"]):movie["name_english"]})
    #sessions = sessions_k.aggregate([{"$match":{"$and":[{"start_time":{"$gte":date_from}},{"start_time":{"$lt":date_to}}]}},{"$project":{"count":{"$size":"$tickets"}, "hall": "$hall_id"}}])
    sessions = sessions_k.aggregate([{"$match":{"$and":[{"start_time":{"$gte":date_from}},{"start_time":{"$lt":date_to}},{"hall_id":{"$in":[hall for hall in halls.keys()]}}]}},{"$group":{"_id":"$movie_id","sessions":{"$push":{"count":{"$size":"$tickets"},"sess_id":"$_id", "hall_id":"$hall_id"}}}}])
    ar = []
    sessions = sessions["result"]
    for session in sessions:
       sub_ar = []
       c = 0.00
       for sess in session["sessions"]:
          c += float(sess["count"])/float(halls[ObjectId(sess["hall_id"])][1])*100
       percentage = c/float(len(session["sessions"]))
       ar.append({"name":movie_dict[str(session["_id"])],"percentage":percentage})
       ar.sort(key=lambda x:x["percentage"], reverse= True)


    #return HttpResponse(json.dumps(list(halls_r), default=json_util.default))
    return render(request, "reports/occupancy_rates.html", {"movies":ar})



def get_sales_by_months(request):
    date_from = datetime.datetime.strptime("01/04/2015", "%d/%m/%Y")
    date_to = datetime.datetime.strptime("15/06/2015", "%d/%m/%Y")
    conn = mongo_do_connect()
    orders_k = conn["testing_ticketorder"]
    sessions_k = conn["testing_session"]
    movies_k = conn["testing_movie"]

    res = {}

    movies = movies_k.aggregate([{"$match":{"film_category":{"$exists": "true"}}},{"$group": {"_id":"$film_category" ,"films":{"$push":{"id": "$_id"}}}}])
    movies = movies["result"]

    #
    html = ""
    dict = {}

    for movie in movies:
        category = movie["_id"]
        films = movie["films"]
        list_ = []
        for film in films:
            list_.append(ObjectId(film["id"]))
        dict.update({str(movie["_id"]):list_})


    sess_dict = {}

    for category, m_array in dict.iteritems():
        sessions = sessions_k.aggregate([{"$match":{"$and":[{"start_time":{"$gte":date_from}},{"start_time":{"$lt":date_to}},{"movie_id":{"$in": m_array}}]}}, {"$sort":{"start_time":1}},{"$group":{"_id":"$movie_id", "sessions":{"$push":{"sess_id": "$_id"}}}}])
        sessions = sessions["result"]
        for session in sessions:
            sess = session["sessions"]
            list_ = []
            for s in sess:
                list_.append(s["sess_id"])
            sess_dict.update({category: list_})

    g_dict = {}
    counter = 1
    output = ""
    for category,sess_ids in sess_dict.iteritems():
	output += "<br><br>"

        orders = orders_k.aggregate([{"$match":{"$and":[{"created":{"$gte":date_from}},{"created":{"$lt":date_to}}, {"session_id":{"$in":sess_ids}}]}}, {"$sort":{"created":-1}},{"$group":{"_id": {"$substr":["$created",0,7]} , "total_cash":{"$sum":"$payment_by_cash"},"total_card":{"$sum":"$payment_by_creditcard"}}}])

        g_dict.update({category: orders["result"]})
    output = []
    per = {}
    cats = []
    for cat, values in g_dict.iteritems():
	if not cat in cats:
	    cats.append(cat)
	for period in values:
            per[period["_id"]] = []
    for cat, values in g_dict.iteritems():
	for period in values:
            array = per[period["_id"]]
            si = {"cat":cat, "cash":period["total_cash"],"card":period["total_card"]}
            array.append(si)
	    per.update({period["_id"]:array})
      	#output.append({"cat":values["total_card"]})

    for cat, values in per.iteritems():
         output.append({"per":cat, "values":values})


    ht = ""

    for out in output:
       all_cats = []
       for cat in out["values"]:
          all_cats.append(cat["cat"])
       for c in cats:
          if not c in all_cats:
             out["values"].append({"cat":c, "cash":0, "card":0})
    for ar in output:
        ar["values"].sort(key=lambda x:x["cat"], reverse= False)
    output.sort(key=lambda x:x["per"], reverse= False)

    #return HttpResponse(json.dumps(output, default=json_util.default))
    return render(request, "reports/sales_by_months.html",{"data": output, "set": cats})


from bson.code import Code

def get_sales_by_week(request):
    now = datetime.datetime.now().strftime("%d/%m/%Y")
    date_from = datetime.datetime.strptime(request.GET.get("from","01/05/2015"), "%d/%m/%Y")
    date_to =   datetime.datetime.strptime(request.GET.get("to", now), "%d/%m/%Y")


    conn = mongo_do_connect()
    tickets_k = conn["testing_ticket"]
    sessions_k = conn["testing_session"]
    movies_k = conn["testing_movie"]
    result_k = conn["myresult"]

    res = {}
    m = Code("function(){emit(this.session_id.substr(0,5), this.price_paid);}")
    r = Code("function(key,values){return Array.sum(values);}")
    reducer = Code("function(obj, prev){prev.count++;}")
    #movies = tickets_k.map_reduce(m, r,"myresult", query={"process":"SL"})
    #ids = movies_k.distinct("_id")

    #res = list(result_k.find({"_id":"cav_1"}))
    #gr = tickets_k.group(key={"process":1}, condition={"session_id":{"$regex":"cav_2"}}, initial={"count":0}, reduce=reducer)
    movies = movies_k.group(key={"_id":1, "name_english":1}, condition={}, initial={"count":0}, reduce=reducer)
    sess_dict = {}
    for doc in movies:
      sessions = sessions_k.group(key={"_id":1}, condition={"$and":[{"movie_id":doc["_id"]},{"start_time":{"$lt":date_to}},{"start_time":{"$gt":date_from}}]}, initial={"count":0}, reduce=reducer)
      ids = []
      for s in sessions:
         ids.append(s["_id"])
      #sums = tickets_k.aggregate([{"$match":{"$and":[{"session_id":{"$in":ids}},{"price_paid":{"$gt":0}}]}}, {"$group":{"_id":"$process","sum":{"$sum":"$price_paid"}}}])

      res_r = tickets_k.group(key={"process":1}, condition={"$and":[{"session_id":{"$in":ids}},{"process":"RE"}]}, initial={"count":0}, reduce=reducer)
      res = tickets_k.group(key={"process":1}, condition={"$and":[{"session_id":{"$in":ids}},{"process":"SL"},{"canceledDate":None}]}, initial={"count":0}, reduce=reducer)
      sess_dict.update({doc["name_english"]:res})
    output =[]
    for k,values in sess_dict.iteritems():
        res = 0
        for p in values:
            if p["process"]=="RE":
                res = p["count"]
        if not res == 0:
            output.append({"movie":k,"values":[res, sale]})
    output.sort(key=lambda x:x["values"][1], reverse= True)
    return HttpResponse(json.dumps(sess_dict, default=json_util.default))

    spl_from = date_from.strftime("%d/%m/%Y").split("/")
    spl_to = date_to.strftime("%d/%m/%Y").split("/")
    from_dj = spl_from[1]+"/"+spl_from[0]+"/"+spl_from[2]
    to_dj = spl_to[1]+"/"+spl_to[0]+"/"+spl_to[2]
    return render(request, "reports/sales_by_movies.html", {"data":output, "from":date_from, "to":date_to, "from_dj":from_dj,"to_dj":to_dj})


def get_sales_by_week_(request):
    now = datetime.datetime.now().strftime("%d/%m/%Y")
    date_from = datetime.datetime.strptime(request.GET.get("from","01/05/2015"), "%d/%m/%Y")
    date_to =   datetime.datetime.strptime(request.GET.get("to", now), "%d/%m/%Y")


    conn = mongo_do_connect()
    tickets_k = conn["testing_ticket"]
    sessions_k = conn["testing_session"]
    movies_k = conn["testing_movie"]
    result_k = conn["myresult"]

    res = {}
    m = Code("function(){emit(this.session_id.substr(0,5), this.price_paid);}")
    r = Code("function(key,values){return Array.sum(values);}")
    reducer = Code("function(obj, prev){prev.count++;}")
    #movies = tickets_k.map_reduce(m, r,"myresult", query={"process":"SL"})
    #ids = movies_k.distinct("_id")

    #res = list(result_k.find({"_id":"cav_1"}))
    #gr = tickets_k.group(key={"process":1}, condition={"session_id":{"$regex":"cav_2"}}, initial={"count":0}, reduce=reducer)
    movies = movies_k.group(key={"_id":1, "name_english":1}, condition={"_id":ObjectId("5566e530956167e9d2801d31")}, initial={"count":0}, reduce=reducer)
    sess_dict = {}
    for doc in movies:
      sessions = sessions_k.group(key={"_id":1}, condition={"$and":[{"movie_id":doc["_id"]},{"start_time":{"$lt":date_to}},{"start_time":{"$gt":date_from}}]}, initial={"count":0}, reduce=reducer)
      ids = []
      for s in sessions:
         ids.append(s["_id"])
      #sums = tickets_k.aggregate([{"$match":{"$and":[{"session_id":{"$in":ids}},{"price_paid":{"$gt":0}}]}}, {"$group":{"_id":"$process","sum":{"$sum":"$price_paid"}}}])

      res = tickets_k.group(key={"process":1}, condition={"$and":[{"session_id":{"$in":ids}}, {"canceledByUser":{"$exists":"false"}}]}, initial={"count":0}, reduce=reducer)
      sess_dict.update({doc["name_english"]:res})
    output =[]
    for k,values in sess_dict.iteritems():
        res = 0
        sale = 0
        flag = False

        for p in values:
            if p["process"]=="RE":
                res = p["count"]

            elif p["process"]=="SL":
                sale = p["count"]
        if not res == 0 and not sale == 0:
            output.append({"movie":k,"values":[res, sale]})
    output.sort(key=lambda x:x["values"][1], reverse= True)
    #return HttpResponse(json.dumps(output, default=json_util.default))

    spl_from = date_from.strftime("%d/%m/%Y").split("/")
    spl_to = date_to.strftime("%d/%m/%Y").split("/")
    from_dj = spl_from[1]+"/"+spl_from[0]+"/"+spl_from[2]
    to_dj = spl_to[1]+"/"+spl_to[0]+"/"+spl_to[2]
    return render(request, "reports/sales_by_movies.html", {"data":output, "from":date_from, "to":date_to, "from_dj":from_dj,"to_dj":to_dj})

def get_sales_by_week(request):
    now = datetime.datetime.now().strftime("%d/%m/%Y")
    date_from = datetime.datetime.strptime(request.GET.get("from","01/05/2015"), "%d/%m/%Y")
    date_to =   datetime.datetime.strptime(request.GET.get("to", now), "%d/%m/%Y")


    conn = mongo_do_connect()
    tickets_k = conn["testing_ticket"]
    sessions_k = conn["testing_session"]
    movies_k = conn["testing_movie"]
    orders_k = conn["testing_ticketorder"]
    result_k = conn["myresult"]

    res = {}
    m = Code("function(){emit(this.session_id.substr(0,5), this.price_paid);}")
    r = Code("function(key,values){return Array.sum(values);}")
    reducer = Code("function(obj, prev){prev.count++;}")

    movies = movies_k.group(key={"_id":1, "name_english":1}, condition={	}, initial={"count":0}, reduce=reducer)

    sess_dict = {}
    for doc in movies:
      sess_dict.update({doc["name_english"]:[]})
    for doc in movies:
      sessions = sessions_k.group(key={"_id":1}, condition={"$and":[{"movie_id":doc["_id"]},{"start_time":{"$lt":date_to}},{"start_time":{"$gt":date_from}}]}, initial={"count":0}, reduce=reducer)
      ids = []
      for s in sessions:
         ids.append(s["_id"])
      res = tickets_k.group(key={"process":1}, condition={"$and":[{"date_sold":{"$lt":date_to}},{"date_sold":{"$gt":date_from}},{"movie_id":doc["_id"]},{"session_id":{"$regex":"cav"}},{"canceledDate":None},{"process":"SL"}]}, initial={"count":0}, reduce=reducer)
      ar = sess_dict[doc["name_english"]]
      if len(res):
        ar.append(res[0])
      res = tickets_k.group(key={"process":1}, condition={"$and":[{"created":{"$lt":date_to}},{"created":{"$gt":date_from}},{"movie_id":doc["_id"]},{"session_id":{"$regex":"cav"}},{"process":"RE"}]}, initial={"count":0}, reduce=reducer)
      ar = sess_dict[doc["name_english"]]

      if len(res):
        ar.append(res[0])
    #return HttpResponse(json.dumps(sess_dict, default=json_util.default))
    output =[]
    for k,values in sess_dict.iteritems():
        res = 0
        sale = 0
        flag = False

        for p in values:
            if p["process"]=="RE":
                res = p["count"]

            elif p["process"]=="SL":
                sale = p["count"]
        if not res == 0 and not sale == 0:
            output.append({"movie":k,"values":[res, sale]})
    output.sort(key=lambda x:x["values"][1], reverse= True)


    spl_from = date_from.strftime("%d/%m/%Y").split("/")
    spl_to = date_to.strftime("%d/%m/%Y").split("/")
    from_dj = spl_from[1]+"/"+spl_from[0]+"/"+spl_from[2]
    to_dj = spl_to[1]+"/"+spl_to[0]+"/"+spl_to[2]
    return render(request, "reports/sales_by_movies.html", {"data":output, "from":date_from, "to":date_to, "from_dj":from_dj,"to_dj":to_dj})


def get_ticket_sales_orders(request):
    now = datetime.datetime.now().strftime("%d/%m/%Y")
    date_from = datetime.datetime.strptime(request.GET.get("from","01/05/2015"), "%d/%m/%Y")
    date_to =   datetime.datetime.strptime(request.GET.get("to", now), "%d/%m/%Y")
    lookup = request.GET["lookup"]
    table = "testing_%s" % lookup

    conn = mongo_do_connect()
    order_conn = conn["testing_ticketorder"]
    lookup_conn = conn[table]
    theater_conn = conn["testing_theater"]
    hall_conn = conn["testing_hall"]
    theaters = list(theater_conn.find({"$where": "this.name.substr(0,7)!='Testing'"}).distinct("_id"))
    halls = hall_conn.find({"theater_id":{"$in":theaters}}).distinct("_id")
    objects = list(lookup_conn.find({},{"name":1}))
    objects.sort(key=lambda x:x["name"])
    objects_d = {}
    for l in objects:
         objects_d[l["name"]] = l["_id"]
    output = {}
    total = 0
    orders = None

    for k, v in objects_d.iteritems():
        lookup_name = "%s.name" % lookup
        lookup_name_with_dollar = "$%s.name" % lookup
        orders = order_conn.aggregate([{"$match":{"$and":[{"created":{"$lt":date_to}},{"created":{"$gt":date_from}},{lookup_name:k},{"hall_id":{"$in":halls}}]}},{"$group":{"_id":lookup_name_with_dollar, "sum":{"$sum":"$discounted_amount"}}}])
        orders = orders["result"]
        if len(orders):
            if orders[0]["sum"]:
                output.update({k:orders[0]["sum"]})
                print orders
                #return HttpResponse(output)
                total += float(orders[0]["sum"])
    '''
    orders_total = order_conn.aggregate([{"$group":{"_id":{"$substr":["$created",0,10]}, "sum":{"$sum":"$discounted_amount"}}}])
    orders = orders_total["result"]
    orders.sort(key=lambda x:x["_id"], reverse=True)
    '''
    spl_from = date_from.strftime("%d/%m/%Y").split("/")
    spl_to = date_to.strftime("%d/%m/%Y").split("/")
    from_dj = spl_from[1]+"/"+spl_from[0]+"/"+spl_from[2]
    to_dj = spl_to[1]+"/"+spl_to[0]+"/"+spl_to[2]

    return render(request, "reports/ticket_sales.html", {"data":output, "from":date_from, "to":date_to, "from_dj":from_dj,"to_dj":to_dj, "lookup":lookup})


def get_ticket_sales_orders_time(request):
    lookup = request.GET["lookup"]
    table = "testing_%s" % lookup

    conn = mongo_do_connect()
    order_conn = conn["testing_ticketorder"]
    lookup_conn = conn[table]
    theater_conn = conn["testing_theater"]
    hall_conn = conn["testing_hall"]
    theaters = list(theater_conn.find({"$where": "this.name.substr(0,7)!='Testing'"}).distinct("_id"))
    halls = hall_conn.find({"theater_id":{"$in":theaters}}).distinct("_id")
    output = {}
    lookup_name = "%s.name" % lookup
    lookup_name_with_dollar = "$%s.name" % lookup
    orders_total = order_conn.aggregate([{"$match":{"$and":[{"discounted_amount":{"$nin":[None, 0]}},{"agent.name":lookup},{"hall_id":{"$in":halls}}]}},{"$group":{"_id":{"$substr":["$created",0,10]},
                                                                                                                          "sum":{"$sum":"$discounted_amount"},
                                                                                                                          "count":{"$sum":{"$size":{"$ifNull":["$items",[]]}}}}},{"$sort":{"created":-1}}])
    orders = orders_total["result"]

    out = {}
    table = {}
    for order in orders:
        output.update({order["_id"]:order["sum"]})
        table.update({order["_id"]: [order["sum"],order["count"]]})
    print table
    return render(request, "reports/ticket_sales.html", {"data":output, "lookup":lookup, "table":table})




def get_distinct_ids(collection, filt, arg):
        k = MongoClient()
	result = k["rustaveli"][collection].find({filt:arg}).distinct("_id")
	return result

@csrf_exempt
def get_current_movies(request):
    halls = get_distinct_ids("testing_hall","theater_id", ObjectId("54ec23a69561676764d1a778"))
    return HttpResponse(json.dumps(halls, default=json_util.default))
