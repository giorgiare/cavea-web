# -*- coding: utf-8 -*-
#!/usr/bin/env python
import sys
import string
import random

from passbook.models import Pass, Barcode, StoreCard, EventTicket, Alignment

class PassGenerator():
    def generatePass(self, cinema, hall, date, movie, seat, orderID):
        cardInfo = EventTicket()
        cardInfo.addPrimaryField('name', cinema, 'კინო')
        cardInfo.addSecondaryField('hall', hall, 'დარბაზი')
        cardInfo.addSecondaryField('date', date, 'თარიღი', Alignment.RIGHT)
        cardInfo.addAuxiliaryField('movie', movie, 'ფილმი')
        cardInfo.addAuxiliaryField('seat', seat, 'ადგილი', Alignment.RIGHT)

        organizationName = 'Cavea Cinemas' 
        passTypeIdentifier = 'pass.com.aboslabs.Cavea' 
        teamIdentifier = '68Z48BWJ2X'

        passfile = Pass(cardInfo, \
            passTypeIdentifier=passTypeIdentifier, \
            organizationName=organizationName, \
            teamIdentifier=teamIdentifier)
        passfile.serialNumber = 'CAV001' 
        passfile.barcode = Barcode(message = orderID)    

        # Including the icon and logo is necessary for the passbook to be valid.
        passfile.addFile('icon.png', open('/home/ubuntu/rustaveli/erp/testing/PassbookUtil/images/icon.png', 'r'))
        passfile.addFile('logo.png', open('/home/ubuntu/rustaveli/erp/testing/PassbookUtil/images/logo.png', 'r'))
        filename = 'CaveaTicket_%s.pkpass' % (self.id_generator())
        ticketPathString = '/home/ubuntu/rustaveli/erp/testing/PassbookUtil/Tickets/%s' % (filename)
        passfile.create('/home/ubuntu/rustaveli/erp/testing/PassbookUtil/certificate.pem', '/home/ubuntu/rustaveli/erp/testing/PassbookUtil/key.pem', '/home/ubuntu/rustaveli/erp/testing/PassbookUtil/wwdr.pem', 'JohnRustaveli1979', ticketPathString) # Create and output the Passbook file (.pkpass)
        return filename
    
    def id_generator(self, size=8, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))


def main():
    generator = PassGenerator()
    generator.generatePass('Cavea Tbilisi Mall', 'Cavea 1', 'APR 23 2015', 'Date', 'Avengers : Age of Ultron ', 'რიგი 10 ადგილი 10', 'TamunaMikaqal')
    sys.exit()

if __name__ == '__main__':
    try:
        main()
    except:
        #mainApp.reactorThread.stop()
        sys.exit() 