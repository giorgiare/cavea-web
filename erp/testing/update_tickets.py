from testing.models import *

tickets = Ticket.objects.all()
for ticket in tickets:
  sess_id = ticket.session_id
  session = Session.objects.get(pk=sess_id)
  hall = session.hall
  theater = hall.theater
  ticket.movie = session.movie
  ticket.hall = hall
  ticket.theater = theater
  ticket.save()
  print ticket.id

