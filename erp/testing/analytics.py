from pymongo import *

from bson.objectid import ObjectId
from erp.settings import DATABASES
import datetime
from datetime import timedelta
import json
from bson import json_util
import pprint


def mongo_do_connect():
    conn = Connection()
    db = conn[DATABASES["default"]["NAME"]]
    return db


def get_movie_sessions():
    movie_id = "5553042e956167dbecb48b90"

    movie_bundle = []
    try:
        today = datetime.datetime.now()
        today = today.replace(hour=5, minute=0, second=0)
        next_week = today + timedelta(7)
        next_week = next_week.replace(hour=5, minute=0, second=0)
        #server is 3 hours behind?
        #date_one_hour_past = today.replace(hour = today.hour()-1)
        conn = mongo_do_connect()

        sessions_k = conn["testing_session"]

        #sessions = sessions_k.aggregate([{"$match":{"$and":[{"start_time":{"$gte":today}},{"start_time":{"$lt":next_week}},{"movie_id":movie_id} ]}}, {"$sort":{"start_time":1}},{"$group":{"_id": "$start_time", "sessions":{"$push":{ "language":"$session_language","is_3d":"$is_3d","start_time":"$start_time", "id":"$_id"}}}}])
        sessions = sessions_k.aggregate([{"$match":{"$and":[{"start_time":{"$gte":today}},{"start_time":{"$lt":next_week}},{"movie_id":ObjectId(movie_id)} ]}}, {"$sort":{"start_time":-1}},{"$group":{"_id": {"$substr":["$_id",6,10]} , "sessions":{"$push":{ "language":"$session_language","is_3d":"$is_3d","start_time":"$start_time", "id":"$_id"}}}}])
        sessions = sessions["result"]
        sessions_bundle = json.dumps(sessions, default=json_util.default)

    except Exception,e:
        print str(e)

    pprint.pprint(sessions)

get_movie_sessions()