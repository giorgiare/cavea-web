# -*- coding: utf-8 -*-
from django import template
import datetime
from django.utils.timezone import now
from decimal import Decimal

register = template.Library()

@register.filter
def sum_var(value,arg):
    return Decimal(value+arg)

@register.filter
def two_arguments(value, arg ):
    return value, arg

@register.filter
def cut(val_arg, arg):
    value, arg_ = val_arg
    return value.replace(arg_, arg)

@register.filter
def get_caption(arg):
    if arg =="std":
        return "სტანდარტი"
    if arg =="sta":
        return "სტატუსი"
    if arg =="eco":
        return "ეკომონი"
    if arg =="vip":
        return "VIP"

@register.filter
def to_lower(arg):
    return arg.lower()

class CurrentTimeNode(template.Node):
    def __init__(self, format_string):
        self.format_string = str(format_string)
        
    def render(self, context):
        now = datetime.datetime.now()
        return now.strftime(self.format_string)
    
@register.tag(name="current_time")
def do_current_time(parser,token):
    try:
        tag_name, format_string = token.split_contents()
    except ValueError:
        msg = '%r tag requires a single argument' % token.split_contents()[0]
        raise template.TemplateSyntaxError(msg)
    return CurrentTimeNode(format_string[1:-1])
    




