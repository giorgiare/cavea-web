# -*- coding: utf-8 -*-
from django.views.decorators.csrf import csrf_exempt
import random
import django_socketio
from jsonify.decorators import ajax_request
from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail, EmailMessage
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render

from testing.models import *
from testing.models import Ticket
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
import datetime
import json
from django.http.response import HttpResponseRedirect, HttpResponse
from decimal import Decimal
from django.core import serializers
from django.conf import settings
from pip._vendor.requests.sessions import session
from django_socketio import events
import dateutil.parser



@events.on_message(channel="^session")
def message(request, socket, context, message):
    #socket.broadcast(context)
    #    socket.broadcast(message)
    #if(message[""])
    if(message["command"]=="remove_blocked"):
        ch_name = message["channel"]
        session = Session.objects.get(uid=message["session"])
        bt = BlockedTicket.objects.filter(session=session)
        for b in bt:
            if b.seat.name == message["id"]:
                b.delete()
                socket.send_and_broadcast_channel(message, channel=ch_name)
        
    
    

@login_required
def home(request):
    employee = Employee.objects.all()
    #for invoice in invoices:
        #print invoice.emission_date
    return render(request, "base_extended.html", {'employee':employee})

def logout_user(request):
    auth.logout(request)
    # Redirect to a success page.
    return HttpResponseRedirect("/accounts/login/")

def account_login(request):
    path = request.GET.get("next")
    if path == None:
        path = "/search/" 
    return render(request, "registration/login.html", {'next': path})

def login_user(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    next = request.POST.get('next')
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        auth.login(request, user)
        return HttpResponseRedirect(next)
    else:
        return HttpResponseRedirect("/accounts/login/")

#@ensure_csrf_cookie
def post(request):
    if 'data' in request.POST and request.POST['data']:
        data = request.POST["data"]
        employees = Employee.objects.filter(name__istartswith=data)
        html =["<ul>"]
        for  employee in employees:
            html.append("<li class='fill_li' id='' onclick='fill_input(event)'>"+employee.surname+"</li>")
             
        html.append("</ul>")     
        post = "".join(html)
        #post_json = json.dumps(post)
        return HttpResponse(post)

@login_required
def search(request):
    if 'name' in request.POST and request.POST['name']:
        name = request.POST['name']
        employee = Employee.objects.get(name=name)
        if not company:
            error = "no company found with this name!"   
            return render(request, "search.html", {'error': error})
        employee_id = company.id
        return render(request, "search.html", {'surname':surname})
    name = "nothing"
    return render(request, "search.html", {'name': name})


@login_required
def hall(request):
    if 'name' in request.GET and request.GET['name']:
        name = request.GET['name']
        hall = Hall.objects.get(name=name)
        rows = []
        for row in hall.rows:        
          seats = Seat.objects.filter(hall=hall, row=row).order_by("id")
          #dict = {"name": row, "seats":seats}
          dict = {"name":row, "seats": serializers.serialize("json", seats)}
          rows.append(dict)
        #rows = Row.objects.filter(hall_id=hall.id).order_by("-id")
        if not hall or not rows:
            error = "no hall found with this name!"   
            return render(request, "hall_.html", {'error': error})
        #return render(request, "hall_.html", {'hall':hall, "rows":rows})
        return HttpResponse(json.dumps(rows))
    name = "nothing"
    return render(request, "hall_.html", {'hall': hall, "rows":rows})

def session_hall(request, hall):
    hall = Hall.objects.get(name=hall)
    sessions = Session.objects.filter(hall=hall).order_by("-start_time")
    return render(request, "session_list.html",{"sessions":sessions, "hall":hall})

def session(request, hall,date,time):
    sess_name ="%s/%s/%s" % (hall, date,time)
    #if 'sess' in request.GET and request.GET['sess']:
    var= True
    if hall and date and time:
        ip = request.META.get('REMOTE_ADDR')
        session = Session.objects.get(uid=sess_name)
        prices = session.prices
        
        
        hall = session.hall
        time = datetime.datetime.now()
        
        date_ = "%s-%s-%s" % (session.start_time.strftime("%d"),session.start_time.strftime("%m"),session.start_time.year) 
        time = "%s:%s" % (session.start_time.hour,session.start_time.strftime("%M"))
        
        
        
        #start_time = {"year":year, "month":month, "day":day, "hour":hour,"min":min}
        seat_types = session.prices
        rows = []
        for row in hall.rows:        
          seats = Seat.objects.filter(hall=hall, row=row).order_by("id")
          dict = {"name": row, "seats":seats}
          rows.append(dict)
         
         
        #rows = Row.objects.filter(hall_id=hall.id).order_by("-id")
        
        if not hall or not rows:
            error = "no hall found with this name!"   
            return render(request, "session.html", {'error': error})
        return render(request, "session.html", {'hall':hall, "rows":rows, "time":time, "session":session, "prices":prices,"ip":ip,"const":settings.CONST, "date":date_,"time":time, "seat_types":seat_types, "username":request.user.email})
    elif hall:
        return HttpResponse("smth")    
    name = "nothing"
    return render(request, "session.html", {'hall': hall, "rows":rows, "time":time})


def get_blocked(request):
    if 'sess' in request.POST and request.POST['sess']:
        sess_id = request.POST['sess']
        session = Session.objects.get(uid=sess_id)
        btickets = BlockedTicket.objects.filter(session=session)
        seats = []
        for ticket in btickets:
            ticket.seat.status = "blocked"
            seats.append(ticket.seat)
        return HttpResponse(serializers.serialize("json",seats))
        #return HttpResponse(serializers.serialize("json", btickets))
        
        
def block_ticket(request):
    if 'data' in request.POST and request.POST['data']:
        id = request.POST['data']
        sess_id = request.POST['sess']
        session = Session.objects.get(uid=sess_id)
        seat = Seat.objects.get(uid = id)
        ticket = BlockedTicket.objects.create(session=session)
        ticket.seat=seat
        ticket.has_discount=False
        ticket.price=0.00
        ticket.date_printed="2015-01-12"
        ticket.date_sold=datetime.datetime.now()
        ticket.save()
        return HttpResponse(ticket.seat.name) 
        

def test_post(request):
    if 'data' in request.POST and request.POST['data']:
        arr = json.loads(request.POST['data'])
        uid = request.POST['sess']
        process = request.POST['process']
        session = Session.objects.get(uid=uid)
        seats = []
        for ticket in arr:
            seat = Seat.objects.get(uid=ticket)
            ticket = Ticket.objects.create(session=session, seat=seat,process=process, has_discount=False, 
                                           price=0.00, date_printed="2015-01-12", date_sold=datetime.datetime.now())
            ticket.save()


def remove_ticket(request):
    if 'data' in request.GET and request.GET['data']:
        id = request.GET['data']
        id = "54c2318b0bc7cb1a87e942d4"
        obj = "ObjectId(\"%s\")" % id
        query = "{$pull:{tickets:{id:ObjectId(\"%s)}}" % id
        #return HttpResponse(obj)
        #query = "{'_id':ObjectId('54c2318b0bc7cb1a87e942d4')}"  
        
        data = Session.objects.raw_update({}, {'$pull' : {'tickets' : {'uid' : r"54c25c080bc7cb316a3ed507"}}})
        
        return HttpResponse(data)
     
#@ajax_request     
def getOrderByPin(request):
    if 'pin' in request.POST and request.POST['pin']:
        pin = request.POST["pin"]
        session = request.POST["session"]
        order = TicketOrder.objects.raw_query({"reservation.pin":pin, "session_id":session})
        seats = []
        for item in order[0].items:
            seat = Seat.objects.get(pk=item.seat.pk)
            seats.append(seat)
        seats_json = serializers.serialize("json", seats)
            
        #order_json = serializers.serialize("json", [order[0]])
        return {"seats": json.loads(seats_json), "order": {"cust_name": order[0].reservation.name, "cust_surname":order[0].reservation.surname, "cust_email": order[0].reservation.email , "cust_mobile":order[0].reservation.mobile}}
        #return {"data":json.loads(seats_json)}
        






def remove_reservation(request, hall, date, time):
    if 'data' in request.POST and request.POST['data']:
        
        sess_name ="%s/%s/%s" % (hall, date,time)
        tickets=Session.objects.get(uid=sess_name).tickets

        
        for ticket in tickets:
            if ticket.process=='RE':
                uid=ticket.uid
                Session.objects.raw_update({}, {'$pull' : {'tickets' : {'uid' : ticket.uid}}})


        return 




'''
chamouaro kvela sessias romelic aris =  CurrentTime - 10 tsuti
@returns session list


params- rangeTimeParam, (gets from config settings.py)
'''

    


def ticket_order(request):
    if 'data' in request.POST and request.POST['data']:
        arr = json.loads(request.POST['data'])
        uid = request.POST['sess']
        cust_name = request.POST['cust_name']
        cust_surname = request.POST['cust_surname']
        cust_mobile = request.POST['cust_mobile']
        cust_email = request.POST['cust_email']
        
        
        process = request.POST['process']
        
        order = TicketOrder.objects.create()
        pin = str(random.randint(1,9999)).zfill(4)
        if process=="RE":
            reservation = Reservation.objects.create(name=cust_name,surname=cust_surname, mobile=cust_mobile, email=cust_email, pin=pin)
            order.reservation = reservation
        
        if process=="SL":
            agent = Agent.objects.all()[0]
            order.agent = agent
        
        ticket_list = []
        
        if process=="RE" or process=="SL":
            session = Session.objects.get(uid=uid)
            order.session = session
            tickets =[]
            seats = []
            for seat_ in arr:
                seat = Seat.objects.get(uid=seat_)
                #t = Ticket.objects.filter(session=session, seat=seat)
                t = None
                for ticket in session.tickets:
                    if ticket.seat.id == seat.id:
                        t = ticket
                    
                if not t:
                    seat = Seat.objects.get(uid=seat_)
                    ticket = Ticket.objects.create(session=session, emb_seat=seat,seat=seat, process=process, has_discount=False, price=0.00, date_printed="2015-01-12", date_sold=datetime.datetime.now(),ticket_order=order)

                    ticket.save()
                    ticket.uid = ticket.id
                    ticket_list.append(ticket)
                    
                    
                    #seats.append(seat)

            session.tickets.extend(ticket_list)
            session.save()
        
            order.items = Ticket.objects.filter(ticket_order=order)
            order.save()           
                    
        return HttpResponse(serializers.serialize("json",seats))
            
    if 'rs' in request.POST and request.POST['rs']:
        arr = json.loads(request.POST['rs'])
        uid = request.POST['sess']
        session = Session.objects.get(uid=uid)
        seats = []
        for ticket in arr:
            seat = Seat.objects.get(uid=ticket)
            tickets = session.tickets
            for t in tickets:
                if t.process =="RE" and t.seat.uid==ticket:
                    Session.objects.raw_update({}, {'$pull' : {'tickets' : {'uid' : t.uid}}})
                    t.process = "SL"
                    session.tickets.extend([t])
                    session.save()

                    username = None
                    if request.user.is_authenticated():
                        username = request.user.email
                    else:
                        username = "giorgiare" 
                    val = Validation.objects.create(ticket=t, user=username,status="valid")
                    val.save()
                    imagepath= "qrcode/%s.jpg" % val.id
                    link = "%s/validation/%s" % (settings.LOCAL_IP, val.id)       
                    qrcode = QRCode.objects.create(ticket=t, imagepath=imagepath,link=link)
                    qrcode.save()
                    val.qrcode = qrcode;
                    val.save()
                #else:
                 #   return HttpResponse("fail")   
        return HttpResponse(serializers.serialize("json",seats))
                    
def refresh_seats(request):
    if 'sess' in request.POST and request.POST['sess']:
        pass                    
                    
def ticket(request):
    if 'sess' in request.POST and request.POST['sess']:
        sess_id = request.POST['sess']
        session = Session.objects.get(uid=sess_id)
        #tickets = Ticket.objects.filter(session=session)
        tickets = session.tickets
        seats = []
        for ticket in tickets:
            ticket.seat.status = ticket.process
            seats.append(ticket.seat)
        return HttpResponse(serializers.serialize("json",seats))
    



def ticketSales(request,hall, date,time):
    sess_name ="%s/%s/%s" % (hall, date,time)
    session = Session.objects.get(uid=sess_name)
    hall = session.hall
    
    tickets = session.tickets
    number_of_eco = len(Seat.objects.filter(hall=hall,ttype="eco"))
    number_of_std = len(Seat.objects.filter(hall=hall,ttype="std"))
    number_of_sta = len(Seat.objects.filter(hall=hall,ttype="sta"))
    number_of_vip = len(Seat.objects.filter(hall=hall,ttype="vip"))    
    
    number_of_reserved = 0
    number_of_sold = 0 
    
    occupied_eco = 0
    occupied_std = 0
    occupied_sta = 0
    occupied_vip = 0
    
    for ticket in tickets:
        if ticket.seat.ttype == "eco":
            occupied_eco += 1
        if ticket.seat.ttype == "std":
            occupied_std += 1
        if ticket.seat.ttype == "sta":
            occupied_sta += 1
        if ticket.seat.ttype == "vip":
            occupied_vip += 1
        if ticket.process =="RE":
            number_of_reserved +=1
        if ticket.process =="SL":
            number_of_sold +=1
    
    ototal = number_of_reserved+number_of_sold        
    total =   number_of_eco-occupied_eco+  number_of_std-occupied_std+number_of_sta-occupied_sta+number_of_vip-occupied_vip
    dict = {"eco": (number_of_eco-occupied_eco),"std":number_of_std-occupied_std,
                "sta":number_of_sta-occupied_sta,"vip":number_of_vip-occupied_vip, 
                "total":total, "reserved":number_of_reserved, 
                "sold":number_of_sold, "ototal":ototal}
        
    if not hall:
        error = "no hall found with this name!"   
        return render(request, "test.html", {'error': error})
    
    return HttpResponse(json.dumps(dict))

def validation(request, ticket):
    val = Validation.objects.get(id=ticket)
    
    if val:
        if val.status =="used":
            return HttpResponse("<html><head></head><body style='background-color: red;'></body></html>")
        val.status = "used"
        val.save()
        return HttpResponse("<html><head></head><body style='background-color: green;'></body></html>")
    else:
        return HttpResponse("<html><head></head><body style='background-color: red;'></body></html>")

def test(request):
        
        return render(request, "test.html", {"seat":"seat"})
        #return HttpResponse(seat))

def session_add(request):
    movies = Movie.objects.all()
    halls = Hall.objects.all().order_by("title")
    return render(request, "session_add.html", {"movies":movies, "halls":halls})

    
def check_digits(obj):
     return obj    
    
def create_session(request):
     if 'hall_id' in request.POST and request.POST['hall_id']:
        hall_id = request.POST["hall_id"]
        movie_id = request.POST["movie_id"]
        #start_time = request.POST["start_time"]
        day = int(request.POST["day"])
        month =  int(request.POST["month"]) 
        year = int(request.POST["year"])
        hour = int(request.POST["hour"])
        minute = int(request.POST["minute"])
        stime = "%s/%s/%s %s:%s" % (day, month,year, hour, minute)
        start_time =  datetime.datetime(year,month, day, hour, minute)
        hall = Hall.objects.get(id=hall_id)
        movie = Movie.objects.get(id=movie_id)
        duration = request.POST["duration"]
        poster_url = request.POST["poster_url"]
        prices = json.loads(request.POST["prices"])
        sess_s = Session.objects.filter(hall=hall, start_time=start_time)
        sessions = []
        if not sess_s:
            session = Session.objects.create(hall=hall, movie=movie, duration=duration, poster_url=poster_url, start_time=start_time, prices=prices)
            session.save()
            sessions.append(session)
            return HttpResponse(serializers.serialize("json", sessions))
        return HttpResponse(serializers.serialize("json", sessions))
        #testComment
        #anotherTestComment

def SaveConfig(request):
    
    username = request.user.email
    data = json.loads(request.POST["data"])
    hall_name = request.POST["hall"]
    config = DesktopConfig.objects.filter(username=username, hall=hall_name)
    config_list = []
    if not config:
        config = DesktopConfig.objects.create(username=username, hall=hall_name)
        
        config.dic = data
        config.save()
        return HttpResponse(config)
    else:
        
        config[0].dic = data
        config[0].save()
        return HttpResponse(data)


def GetConfig(request):
    username = request.user.email
    hall_name = request.POST["hall"]
    config = DesktopConfig.objects.filter(username=username, hall = hall_name)
    if config:
        return HttpResponse(json.dumps(config[0].dic))
    return HttpResponse(json.dumps(config[0].dic))        
        
@login_required        
def create_session_set(request):
    movies = Movie.objects.all()
    halls = Hall.objects.all().order_by("title")
    return render(request, "create_session_set.html", {"movies":movies, "halls": halls})
            


def get_seats_by_session(request,hall, date,time):
    sess_name = "%s/%s/%s" % (hall, date,time)
    session = Session.objects.get(uid=sess_name)
    hall_id = session.hall_id
    #return HttpResponse(sess_name)
    #if session_id in request.GET and request.GET[session_id]:
    #    sess_id=request.GET['session_id']
    #Session.objects.get(session_id=sess_id)[:1]
    seats=Seat.objects.filter(hall=hall_id)
    return HttpResponse(serializers.serialize("json",seats))




def get_halls_by_cinema(request, cinema_id):
    if 'cinema_id' in request.GET and request.GET['cinema_id']:
        cinema_id=request.POST['cinema_id']
    halls = Hall.objects.filter(theater=cinema_id)
    jsonOfHalls=serializers.serialize("json", halls)
    halls_dict={}
    if 1==1:
        json_status="OK!"
    else:
        json_status="no data.."
    
    halls_dict['status'] = json_status
    halls_dict['data'] = jsonOfHalls
    return HttpResponse(json.dumps(halls_dict))


    return HttpResponse(json.dumps(serializers.serialize("json", halls)))


def get_sessions_by_hall(request, hall_id):
    sessions=Session.objects.filter(hall_id=hall_id)
    jsonOfSessions=serializers.serialize("json", sessions, fields=('movie', 'start_time', 'hall','m_name','prices','duration', 'poster_url'))
    for jsonSession in jsonOfSessions:
        movie = Movie.objects.get(id=jsonSession['movie'])
        jsonSession['emb_movie'] = serializers.serialize("json", movie)
    sessions_dict={}
    json_status=""
    if 1==1:
        json_status="OK!"
    else:
        json_status="no data.."
    sessions_dict['status'] = json_status
    sessions_dict['data']= jsonOfSessions
    return HttpResponse(json.dumps(sessions_dict))

@ajax_request
def get_seats_states_by_session(request,hall, date,time):
    sess_name = "%s/%s/%s" % (hall, date,time)
    tickets = Ticket.objects.filter(session_id=sess_name)
    sessions_dict = {}
    sessions_dict['status'] = "OK!"
    sessions_dict["data"] = serializers.serialize("json", tickets)
    return sessions_dict


def get_theaters(request):
 
    theaters =  Theater.objects.all()
    jsonOfTheaters=serializers.serialize("json", theaters)
    response_dict = {}
    json_status=""
    if 1==1:
        json_status = "OK!"
    else:
        json_status = "no data.."

    response_dict['status'] = json_status
    response_dict['data'] = jsonOfTheaters
    return HttpResponse(json.dumps(response_dict))
    

def sendemail(request):
    email = request.GET["email"]
    to_emails = []
    to_emails.append(email)
    subject, from_email, to = 'hello', 'giorgiare@gmail.com', email
    text_content = 'This is an important message.'
    html_content = '<p>This is an <strong>important</strong> message.</p>'
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    #msg.attach_alternative(html_content, "text/html")
    msg.send()
    return HttpResponse("done")

@ajax_request
@csrf_exempt
def testPython(request):
    if "hall" in request.GET and request.GET["hall"]:
        hall = Hall.objects.get(name=request.GET["hall"])
        #return {"response": hall.pk}
        session = Session.objects.filter(hall_id=hall.pk)
        sessions = []
        for sess in session:
            sess.tickets = []
            sess.emb_movie = json.loads(serializers.serialize("json",[Movie.objects.get(pk=sess.movie.pk)])) 
            sessions.append(sess)
        return {"response": serializers.serialize("json", sessions)}
    return HttpResponse("done")




#helper functions down

def isTicketAvailable(sess_id,seat_id):
    session = Session.objects.get(uid=sess_id)
    tickets = session.tickets
    for ticket in tickets:
      seat = Seat.objects.get(pk=ticket.seat.pk)
      if seat.uid == seat_id:
        return False # if ticket is not Availble
    return True   



def getTicketBySeatId(sess_id, seat_id):
    session = Session.objects.get(uid=sess_id)
    tickets = session.tickets
    for ticket in tickets:
      seat = Seat.objects.get(pk=ticket.seat.pk) # strange code here..
      if seat.uid == seat_id:
        return ticket



def block_ticket_api(request, data): # why do we need data if Request itself is gonna be sliced by params?
    if 'data' in request.POST and request.POST['data']:
        id = request.POST['data']
        sess_id = request.POST['sess']
        session = Session.objects.get(uid=sess_id)
        seat = Seat.objects.get(uid = id)
        ticket = BlockedTicket.objects.create(session=session)
        ticket.seat=seat
        ticket.has_discount=False
        ticket.price=0.00
        ticket.date_printed="2015-01-12"
        ticket.date_sold=datetime.datetime.now()
        ticket.save()
        return HttpResponse(ticket.seat.name) 

def get_blocked_api(request,hall,day,month,year,hour,):   # same here.. these functions resembles ancestor ones above.
    if 'sess' in request.POST and request.POST['sess']:
        sess_id = request.POST['sess']
        session = Session.objects.get(uid=sess_id)
        btickets = BlockedTicket.objects.filter(session=session)
        seats = []
        for ticket in btickets:
            ticket.seat.status = "blocked"
            seats.append(ticket.seat)
        return HttpResponse(serializers.serialize("json",seats))
        #return HttpResponse(serializers.serialize("json", btickets))
        


#view functions down

def ticket_selects(request, hall, date, time, seat_id):
    session_id=hall+"/"+date+"/"+time
    tickets = Session.objects.get(uid=session_id).tickets

    for ticket in tickets:
        seatID = Seat.objects.get(uid=ticket.emb_seat)
        if(seat_id==seatID.uid):
            if (ticket.process):
                order = TicketOrder.objects.get(pk=ticket.ticket_order_id)
#               return HttpResponse(order.pk)
#               to = TicketOrder.objects.all()[0]
                order_items = []
                for item in order.items:
                    jsonOfItem = json.loads(serializers.serialize("json", [item]))
                    order_items.append(jsonOfItem)
                order.items = order_items
                if order.reservation != None:
                    order.reservation = json.loads(serializers.serialize("json", [order.reservation]))
                to = serializers.serialize("json", [order])

#                order_items=[]
                #for item in order.items:
                 #   jsonOfItem=json.loads(serializers.serialize("json", [item]))
                  #  order_items.append(jsonOfItem)
                #order.items = order_items
                return HttpResponse(json.dumps(to))
            
            # elif (ticket.process=="RE"):
           #     order = TicketOrder.objects.get(pk=ticket.ticket_order_id)
            #    return HttpResponse(order.pk)
             #   order_items=[]
              #  for item in order.items:
               #     jsonOfItem=json.loads(serializers.serialize("json", [item]))
                #    order_items.append(jsonOfItem)
                #order.items = order_items
                #return HttpResponse(serializers.serializer("json", [order]))
            #else :


             #return HttpResponse(iterator)

    else:
        return HttpResponse("{\"TicketStatus\": \"Ticket is Free\"}")




#    return HttpResponse(serializers.serialize("json", all_ticks))   
#    return HttpResponse(serializers.serialize("json",all_ticks))



def ticket_book(request, hall, date, time, seat_id):
    session_id=hall+"/"+date+"/"+time
    all_ticks = Session.objects.get(uid=session_id).tickets

    booked_tickets = {}
    for ticket in all_ticks:
        i=0
        booked_tickets[i] = ticket.status 



    return HttpResponse(serializers.serialize("json", booked_tickets)) 


# ("rus1_r1_s1", "rus1_r1_s2"), "RE", "SL" "SR"


def ticket_buy(request, hall, date, time, seat_id):
    session_id=hall+"/"+date+"/"+time
    all_ticks = Session.objects.get(uid=session_id).tickets
    ticket=all_ticks.objects.get()
   
   

    if isTicketAvailable(session_id, seat_id):

        ticket = getTicketBySeatId(session_id, seat_id)
        orders = TicketOrder.objects.filter(session= Session.objects.get(uid=session_id))
        for order in orders:
            for item in order.items:
                if ticket.pk == item.pk:
                    item.process = "SL"
                    item.save()
                    return HttpResponse(item) 

    else:

        pass



    return HttpResponse (isTicketAvailable)








#copied
@csrf_exempt
def DiscountsApi(request):
    discounts =  Discount.objects.all()
    jsonOfDiscounts=serializers.serialize("json", discounts)
    response_dict = {}
    json_status=""
    if 1==1:
        json_status = "OK!"
    else:
        json_status = "no data.."

    response_dict['status'] = json_status
    response_dict['data'] = jsonOfDiscounts
    return HttpResponse(json.dumps(response_dict))



@csrf_exempt
def TicketBookApi(request):
    if validate_posted_variables(request, "seats", "session_id", "cust_name","cust_surname"):
        
        # get posted variables
        seats = request.POST["seats"]
        session_id = request.POST["session_id"]
        cust_name = request.POST["cust_name"]
        cust_surname = request.POST["cust_surname"]
        cust_mobile = request.POST['cust_mobile']
        cust_email = request.POST['cust_email']
        # get django objects from database
        session = Session.objects.get(uid=session_id)
        #create order
        order = TicketOrder.objects.create()
        #create reservation

        pin = str(random.randint(1,9999)).zfill(4)
        reservation = Reservation.objects.create(name=cust_name,surname=cust_surname, mobile=cust_mobile, email=cust_email, pin=pin)
        order.reservation = reservation
        #create tickets for separate seat in posted variable
        ticket_list = []
        for seat_ in json.loads(seats):
            seat = Seat.objects.get(uid=seat_)
            #make sure ticket does not exist in db
            t = None
            for ticket in session.tickets:
                if ticket.seat.id == seat.id:
                        t = ticket
                        return HttpResponse("already exists")
            # if ticket does not exist : create ticket
            if not t:
                price = session.prices[seat.ttype]
                ticket = Ticket.objects.create(session=session, emb_seat=seat,seat=seat, process="RE", has_discount=False, price=price, date_printed=datetime.datetime.now(), date_sold=datetime.datetime.now(),ticket_order=order)
                ticket.save()
                ticket.uid = ticket.id
                ticket_list.append(ticket)
        #seats.append(seat)
        if ticket_list:
            session.tickets.extend(ticket_list)
            session.save()
        order.items = Ticket.objects.filter(ticket_order=order)
        order.save()
        response = {"status":"Failed"}
        if ticket_list:
            response['status'] = 'Ok!'
        return HttpResponse(json.dumps(response))
    else:
        return HttpResponse("variables not posted")

@csrf_exempt
def TicketBuyApi(request):
    
    if validate_posted_variables(request, "session_id", "seats"):
        # get posted variables
        seats = request.POST["seats"]
        session_id = request.POST["session_id"]

        # get django objects from database
        session = Session.objects.get(uid=session_id)
        #create order
        order = TicketOrder.objects.create()
        #create tickets for separate seat in posted variable
        ticket_list = []
        for seat_ in json.loads(seats):
            seat = Seat.objects.get(uid=seat_)
            #make sure ticket does not exist in db
            t = None
            for ticket in session.tickets:
                if ticket.seat.id == seat.id:
                        t = ticket
                        return HttpResponse("already exists")
            # if ticket does not exist : create ticket
            if not t:
                price = session.prices[seat.ttype]
                ticket = Ticket.objects.create(session=session, emb_seat=seat,seat=seat, process="SL", has_discount=False, price=price, date_printed=datetime.datetime.now(), date_sold=datetime.datetime.now(),ticket_order=order)
                ticket.save()
                ticket.uid = ticket.id
                ticket_list.append(ticket)
        #seats.append(seat)
        if ticket_list:
            session.tickets.extend(ticket_list)
            session.save()
        order.items = Ticket.objects.filter(ticket_order=order)
        order.save()
        response = {"status":"Failed"}
        if ticket_list:
            response['status'] = 'Ok!'
        return HttpResponse(json.dumps(response))
    else:
        return HttpResponse("variables not posted")

@csrf_exempt
def TicketBuyBookedApi(request):
    if validate_posted_variables(request, "session_id", "seats"):
        # get posted variables
        seats = request.POST["seats"]
        session_id = request.POST["session_id"]

        # get django objects from database
        session = Session.objects.get(uid=session_id)
        #create order
        order = TicketOrder.objects.create()
        #create tickets for separate seat in posted variable
        ticket_list = []
        for seat_ in json.loads(seats):
        
            seat = Seat.objects.get(uid=seat_)
            tickets = session.tickets
            for t in tickets:
                if t.process =="RE" and t.seat.uid==seat_:
                    t.process = "SL"
                    ticket_list.append(t)
                    session.save()
        return HttpResponse(ticket_list)            


def validate_posted_variables(request, *args):
    coll = []
    for arg in args:
        if arg in request.POST and request.POST[arg]:
            coll.append(True)
    if len(coll)==len(args):
        return True