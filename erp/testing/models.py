# -*- coding: utf-8 -*-
from twisted.internet.protocol import Protocol, Factory, DatagramProtocol
from twisted.internet import reactor
from djangotoolbox.fields import EmbeddedModelField, DictField
from django_mongodb_engine.contrib import MongoDBManager
from jsonfield import JSONField
from django.db import models
from forms import StringListField
from djangotoolbox.fields import ListField
from django.db.models.signals import post_init
from forms import ObjectListField
import qrcode
from django.db.models import signals 
from datetime import datetime, timedelta


class Permission(models.Model):
    name = models.CharField(max_length=255)
    model_name = models.CharField(max_length=40)
    codename = models.CharField(max_length=255)

    def __unicode__(self):
        return "%s | %s | %s" % (self.name, self.model_name, self.codename)

class EmbedOverrideField(EmbeddedModelField):
    def formfield(self, **kwargs):
        return models.Field.formfield(self, ObjectListField, **kwargs)

class CategoryField(ListField):
    def formfield(self, **kwargs):
        return models.Field.formfield(self, StringListField, **kwargs)


class Size():
    width = None
    height = None
    def __init__(self, width, height):
        self.width = width
        self.height = height

class Theater(models.Model):
    name = models.CharField(max_length=40)
    company_id = models.CharField(max_length=20)
    address = models.CharField(max_length=70)
    lat = models.FloatField(blank=True, null=True)
    lng = models.FloatField(blank=True, null=True)
    zip_code = models.CharField(max_length=8)
    city = models.CharField(max_length=30)
    phone = models.CharField(max_length=30)
    testing_only = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.name

class Hall(models.Model):
    name = models.CharField(max_length=20)
    title = models.CharField(max_length=30)
    theater = models.ForeignKey(Theater,blank=True, null=True);
    number_of_seats = models.IntegerField(blank=True, null=True)
    rows = JSONField()
    objects = MongoDBManager()
     
    def __unicode__(self):
        return self.title

class Section(models.Model):
    name = models.CharField(max_length=20)
    category = models.CharField(max_length=20)	    

class Seat(models.Model):
    uid = models.CharField(max_length=20, primary_key=True)
    hall = models.ForeignKey(Hall, null=True)
    section = models.CharField(max_length=30, null=True)
    name = models.CharField(max_length=20)
    row = models.CharField(max_length=10)
    seat = models.IntegerField(blank=True,null=True)
    status = models.CharField(max_length=40,blank=True,null=True)
    left  = models.FloatField(null=True, blank=True)
    top  = models.FloatField(null=True, blank=True)
    width  = models.FloatField(null=True, blank=True)

    EMPTY = "emp"
    ECONOMY = "eco"
    STANDARD = "std"
    STATUS = "sta"
    VIP = "vip"
    
    SEAT_TYPES = ((ECONOMY, "Economy"),(STANDARD, "Standard"),(STATUS,"Status"),(VIP,"VIP"),(EMPTY,"Empty"))
    ttype = models.CharField(max_length=3,choices=SEAT_TYPES)

    objects = MongoDBManager()
    
    def __unicode__(self):
        return self.uid  #r"Row:%s Seat:%s" % (self.row, self.seat)

class Movie(models.Model):
    name = models.CharField(max_length=120)
    name_english = models.CharField(max_length=120, null=True, blank=True)
    country = models.CharField(max_length=30)
    duration = models.IntegerField()
    poster_base = models.FileField(upload_to="movies/posters/images/")
    poster_thumbnail = models.FileField(upload_to="movies/posters/thumbnails/")
    mpaa_rating = models.CharField(max_length=10, default='G', blank=True)
    # GEORGIAN = 'geo'
    # RUSSIAN = 'rus'
    # ENGLISH = 'eng'
    # LANGUAGES = ((GEORGIAN,"ქართული"), (ENGLISH, "English"), (RUSSIAN, "Russian"))
    # language = models.CharField(max_length=10, choices=LANGUAGES, default=RUSSIAN)

    poster_url = models.CharField(max_length=256, null=True, blank=True)
    trailer_url = models.CharField(max_length=256, null=True, blank=True)

    is_active = models.BooleanField(default=True)
    coming_soon = models.BooleanField(default=False)
    
    def __unicode__(self):
        return self.name

class Session(models.Model):
    m_name = models.CharField(max_length=50, editable=False)
    uid = models.CharField(max_length=60, primary_key=True)
    start_time = models.DateTimeField()

    GEORGIAN = 'geo'
    RUSSIAN = 'rus'
    ENGLISH = 'eng'
    NO_LANGUEGE = 'nlg'
    LANGUAGES = ((GEORGIAN,"ქართული"), (ENGLISH, "English"), (RUSSIAN, "Russian"), (NO_LANGUEGE, "NOLANGUAGE"))
    session_language = models.CharField(max_length=10, choices=LANGUAGES, default=RUSSIAN)
    is_3d = models.BooleanField(default=False)

    poster_url = models.CharField(max_length=256, editable=False)
    emb_movie = JSONField(null=True, blank=True,editable=False)
    emb_tickets = JSONField(null=True, blank=True, editable=True)
    duration=models.IntegerField()
    hall = models.ForeignKey(Hall)
    movie = models.ForeignKey(Movie)
    prices = JSONField(null=True, blank=True)
    tickets = ListField(EmbeddedModelField("Ticket"), editable=False)

    is_canceled = models.BooleanField(default=False)

    def mpaa_rating(self):
        return self.movie.mpaa_rating

    objects = MongoDBManager()
    
    def save(self, *args, **kwargs):
        str = self.hall.name.split("_")
        self.m_name = self.movie.name
        self.uid = r"%s_%s/%s-%s-%s/%s:%s" % (str[0][:3],str[1],self.start_time.strftime("%d"),self.start_time.strftime("%m"),self.start_time.year,self.start_time.hour,self.start_time.strftime("%M")) 
        super(self.__class__, self).save(*args, **kwargs)
    
    def __unicode__(self):
        #return "%s | %s | %s-%s %s:%s" % (self.hall.title, self.movie.name ,self.start_time.strftime("%d"),self.start_time.strftime("%b"), self.start_time.hour,self.start_time.strftime("%M")) 
        return self.uid

#to be changed to Client
class Agent(models.Model):    
    ttype = models.CharField(max_length=3,choices=(("BO","Box Office"),("WEB","Website"),("SEL","Seller"),("IAP","iOS APP"),("AAP","Android App"),("CTR","Controller"), ("EXT","External"),("TST","Testing")))
    name = models.CharField(max_length=40)
    description = models.CharField(max_length=64)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    permissions = JSONField()

    # def __unicode__(self):
    #     return self.id

class Discount(models.Model):
    ttype = models.CharField(max_length=3, choices=(("TK", "Ticket"), ("CS", "Concessions")))
    name = models.CharField(max_length=40)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    is_percent = models.BooleanField(default=False)
    percent_value = models.DecimalField(max_digits=6,decimal_places=2, blank=True, null=True)
    fixed_value = models.DecimalField(max_digits=6,decimal_places=2, blank=True, null=True)
    fixed_price = models.DecimalField(max_digits=6,decimal_places=2, blank=True, null=True)
    is_active = models.BooleanField(blank=True, default=True)
    #need to implemplement 2+1 and other types of promotions, if not here in separate class



class TestField(models.Model):
    name=models.CharField(max_length=22)
    surnames= ListField()
    tickets = ListField(EmbeddedModelField("Ticket"), null=True, blank=True)



class TicketOrder(models.Model):
    created = models.DateTimeField(auto_now_add=True, blank=True)
    date_sold = models.DateTimeField(blank=True, null=True)
    agent = EmbeddedModelField("Agent", editable=False, null=True)
    session = models.ForeignKey(Session, null=True)
    items = ListField(EmbeddedModelField("Ticket"), null=True, blank=True)
    box_office_items = ListField(EmbeddedModelField('BoxOfficeProductItem'), blank=True, null=True)
    status = models.CharField(max_length=30, default="pending")
    reservation =EmbeddedModelField("Reservation", null=True)
    reservation_sold = models.DateField(null=True, blank=True)
    
    payment_by_cash = models.FloatField(null=True)
    payment_by_giftcard = models.FloatField(null=True)
    payment_by_creditcard = models.FloatField(null=True)
    discount_amount = models.FloatField(null=True)
    discounted_amount = models.FloatField(null=True)
    initial_price = models.FloatField(null=True)
    user=models.CharField(null=True, max_length=256)
    cart_log = models.CharField(max_length=5000, null=True, blank=True)
    movie = models.ForeignKey(Movie, null=True)
    hall = models.ForeignKey(Hall, null=True)
    theater = models.ForeignKey(Theater, null=True) 
    
    objects = MongoDBManager()

    def placeOrder(self):
        self.status = "paid"
        

            
        
class Ticket(models.Model):
    uid = models.CharField(max_length=100, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    session = models.ForeignKey(Session)
    ticket_order  = models.ForeignKey(TicketOrder, null=True)
    seat = models.ForeignKey(Seat, null=True, blank=True)
    emb_seat = EmbeddedModelField("Seat",null=True, blank=True)
    initial_price = models.FloatField(null=True)
    discount_amount = models.FloatField(null=True)
    price_paid = models.FloatField(null=True)
    stype = models.CharField(max_length=20)
    canceledDate = models.DateField(blank=True, null=True)
    canceledByUser = models.CharField(max_length=64,blank=True, null=True)
    movie = models.ForeignKey(Movie, null=True)
    hall = models.ForeignKey(Hall, null=True)
    theater = models.ForeignKey(Theater, null=True) 
    #implement canceled by who?
    
    @property
    def Price(self):
        return "GEL %s" % self.price
    process = models.CharField(max_length=2)
     
    PRINTED = "PR"
    DIGITAL = "DG"
    TICKET_TYPES = ((PRINTED, "Printed"),(DIGITAL, "Digital"))
    ttype = models.CharField(max_length=2,choices=TICKET_TYPES, default=PRINTED)
    has_discount = models.BooleanField(default=False)
    price = models.FloatField()#DecimalField(max_digits=6,decimal_places=2)
    date_printed = models.DateField(blank=True, null=True)
    date_sold = models.DateTimeField(blank=True, null=True)
    objects = MongoDBManager()
    
    def save(self, *args, **kwargs):
        
        if not self.price:
            if self.seat.ttype == "std":
                self.price = self.session.prices["std"]
                self.stype = "Standard"
            elif self.seat.ttype == "eco":
                self.stype = "Economy"
                self.price = self.session.prices["eco"]
            elif self.seat.ttype == "vip":
                self.stype = "VIP"
                self.price = self.session.prices["vip"]
            elif self.seat.ttype == "sta":
                self.stype = "Status"
                self.price = self.session.prices["sta"]   
        super(self.__class__, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return "%s_%s_%s" % (self.session.hall.name,self.session.start_time,self.seat)

class BlockedTicket(models.Model):
    expireAt = models.DateTimeField(default=datetime.now()-timedelta(hours=4), blank=True)
    uid = models.CharField(max_length=100, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    session = models.ForeignKey(Session)
    ticket_order  = models.ForeignKey(TicketOrder, null=True)
    seat = models.ForeignKey(Seat, null=True, blank=True)
    #seat = EmbeddedModelField("Seat",null=True, blank=True)
    stype = models.CharField(max_length=20)
    has_discount = models.BooleanField(default=False)
    price = models.FloatField()#DecimalField(max_digits=6,decimal_places=2, null=True)
    date_printed = models.DateField(blank=True, null=True)
    date_sold = models.DateTimeField(blank=True, null=True)
    objects = MongoDBManager()
    
    def __unicode__(self):
        return "%s_%s_%s" % (self.session.hall.name,self.session.start_time,self.seat) 

class TicketBlocked(models.Model):
    uid = models.CharField(max_length=100, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    block_user = models.CharField(max_length=100,null=True, blank=True)
    session = models.ForeignKey(Session)
    ticket_order  = models.ForeignKey(TicketOrder, null=True)
    seat = models.ForeignKey(Seat, null=True, blank=True)
    emb_seat = EmbeddedModelField("Seat",null=True, blank=True)
    initial_price = models.FloatField(null=True)
    discount_amount = models.FloatField(null=True)
    price_paid = models.FloatField(null=True)
    stype = models.CharField(max_length=20)
    canceledDate = models.DateField(blank=True, null=True)
    canceledByUser = models.CharField(max_length=64,blank=True, null=True)
    #implement canceled by who?
    
    @property
    def Price(self):
        return "GEL %s" % self.price
    process = models.CharField(max_length=2)
     
    PRINTED = "PR"
    DIGITAL = "DG"
    TICKET_TYPES = ((PRINTED, "Printed"),(DIGITAL, "Digital"))
    ttype = models.CharField(max_length=2,choices=TICKET_TYPES, default=PRINTED)
    has_discount = models.BooleanField(default=False)
    price = models.FloatField()#DecimalField(max_digits=6,decimal_places=2)
    date_printed = models.DateField(blank=True, null=True)
    date_sold = models.DateTimeField(blank=True, null=True)
    objects = MongoDBManager()
    
    def save(self, *args, **kwargs):
        
        if not self.price:
            if self.seat.ttype == "std":
                self.price = self.session.prices["std"]
                self.stype = "Standard"
            elif self.seat.ttype == "eco":
                self.stype = "Economy"
                self.price = self.session.prices["eco"]
            elif self.seat.ttype == "vip":
                self.stype = "VIP"
                self.price = self.session.prices["vip"]
            elif self.seat.ttype == "sta":
                self.stype = "Status"
                self.price = self.session.prices["sta"]   
        super(self.__class__, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return "%s_%s_%s" % (self.session.hall.name,self.session.start_time,self.seat)  





class BoxOfficeProduct(models.Model):
    sku = models.CharField(max_length=120, primary_key=True)
    name = models.CharField(max_length=50)
    description = models.TextField()
    enabled = models.BooleanField()
    price = models.DecimalField(max_digits=3,decimal_places=2)
    barcode = models.CharField(max_length=120)
    has_discount = models.BooleanField(default=False)
    objects = MongoDBManager()

    def __unicode__(self):
        return self.sku

class BoxOfficeProductItem(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    quantity = models.FloatField(null=True,blank=True)
    discount_applied = models.FloatField(null=True,blank=True)
    price_paid_total = models.FloatField(null=True,blank=True)
    emb_product = EmbeddedModelField("BoxOfficeProduct",null=True, blank=True)
    canceledDate = models.DateField(blank=True, null=True)
    canceledByUser = models.CharField(max_length=64,blank=True, null=True)
    ticket_order  = models.ForeignKey(TicketOrder, null=True, related_name='boxofficeproductitem_order')
    objects = MongoDBManager()

    def __unicode__(self):
        return self.id


class OnlineCardPayment(models.Model):
    trans_id = models.CharField(max_length=64, null=True, blank=True)
    amount = models.FloatField(blank=True, null=True)
    description = models.CharField(max_length=264, null=True, blank=True)
    transaction_source = models.CharField(max_length=264, null=True, blank=True)
    customer = EmbeddedModelField("Customer",null=True, blank=True)
    order_id = models.CharField(max_length=64, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    user = models.CharField(max_length=64, blank=True, null=True)
    transaction_userside_id = models.CharField(max_length=64, null=True, blank=True)
    canceled_date = models.DateField(blank=True, null=True)
    canceled_user = models.CharField(max_length=25,null=True,blank=True)
    
    #tbc -c operation response fields
    card_result = models.CharField(max_length=64, null=True, blank=True)
    card_result_ps = models.CharField(max_length=64, null=True, blank=True)
    card_result_code = models.CharField(max_length=64, null=True, blank=True)
    card_3dsecure = models.CharField(max_length=64, null=True, blank=True)
    card_rrn = models.CharField(max_length=64, null=True, blank=True)
    card_approval_code = models.CharField(max_length=64, null=True, blank=True)
    card_aav = models.CharField(max_length=64, null=True, blank=True)
    card_number = models.CharField(max_length=64, null=True, blank=True)
    card_recc_pmnt_id = models.CharField(max_length=64, null=True, blank=True)
    card_pmnt_expiry = models.CharField(max_length=64, null=True, blank=True)


    objects = MongoDBManager()

class OnlinePaymentsDayClose(models.Model):
    day_closed_date = models.DateField(auto_now_add=True, blank=True)
    result_log = models.CharField(max_length=256, null=True, blank=True)

    objects = MongoDBManager()


class GiftCard(models.Model):
    card_code = models.CharField(max_length=64, null=True, blank=True)
    customer = EmbeddedModelField("Customer",null=True, blank=True)
    expiresAt = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    face_value = models.FloatField()
    remaining_balance = models.FloatField()
    #add created by
    #add modified by
    #add payment info
    objects = MongoDBManager()


class Customer(models.Model):
    name = models.CharField(max_length=40, null=True, blank=True)
    surname = models.CharField(max_length=40, null=True, blank=True)
    email = models.CharField(max_length=40, null=True, blank=True)
    phone = models.CharField(max_length=40, null=True, blank=True)
    national_id = models.CharField(max_length=40, null=True, blank=True)
    objects = MongoDBManager()

class User(models.Model):
    name = models.CharField(max_length=40)
    surname = models.CharField(max_length=40)
    email = models.EmailField()
    permissions = ListField(EmbeddedModelField(Permission))


    def __unicode__(self):
        return "%s %s " % (self.name, self.surname)


class Order(models.Model):
    ticket = models.ForeignKey(Ticket)
    utype = models.CharField(max_length=2,choices=(("ANON","Anonymous"),("USER","User")), default="ANON")
    user = models.ForeignKey(User, null=True, blank=True)

class Employee(models.Model):
    ser_num = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    date_of_birth = models.DateField()
    id = models.AutoField(primary_key=True)
    hired = models.DateField()
    position = models.CharField(max_length=50)
    education = models.CharField(max_length=50)
    job_descr = models.TextField(blank=True)
   # def __init__(self, *args, **kwargs):
        #self.ser_num =  Employee.objects.all().order_by("-ser_num")[0].ser_num + 1	
    #    pass
    def save(self, *args, **kwargs):
        if not self.ser_num: 
            self.ser_num = len(Employee.objects.all())+1
            super(self.__class__, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return u'%s %s' % (self.name, self.surname)

def incrementEmployee(**kwargs):
    instance = kwargs.get("instance")
    instance.ser_num = Increment.objects.all().order_by("-employee")[0].employee +18 
    
class BoxOffice(models.Model):
    name = models.CharField(max_length=20)
    theater = EmbeddedModelField("Theater", blank=True, null=True)
    number_of_cash_registers = models.IntegerField()
    
class CashRegister(models.Model):
    name = models.CharField(max_length=20)
    box_office = EmbeddedModelField("BosOffice", blank=True, null=True)

class Shift(models.Model):
    started = models.DateTimeField(auto_now_add=True, blank=True)
    user = models.CharField(max_length=64,blank=True, null=True)
    agent = models.CharField(max_length=64,blank=True, null=True)
    csrftoken = models.CharField(max_length=64,blank=True, null=True)
    
class Row(models.Model):
    hall_id = models.CharField(max_length=60, blank=True)
    seats = ListField(blank=True, null=True)
    name = models.CharField(max_length=10)
    objects = MongoDBManager()

class Transaction(models.Model):
    created = models.DateTimeField(auto_now_add=True, blank=True)
    ticket = models.ForeignKey(Ticket)
    shift = models.ForeignKey(Shift,blank=True, null=True)
    ttype = models.CharField(max_length=2,choices=(("SL","Sold"),("RT","Returned")), default="SL")
    amount = models.DecimalField(max_digits=6,decimal_places=2)

class Validation(models.Model):
    ticket = EmbeddedModelField("Ticket", editable=True)
    user = models.CharField(max_length=40,blank=True, null=True)
    created = models.DateField(auto_now_add=True,blank=True)
    status = models.CharField(max_length=40,blank=True, null=True, default="valid")
    qrcode = EmbeddedModelField("QRCode", editable=False, null=True)
    
class QRCode(models.Model):
    created = models.DateTimeField(auto_now_add=True, blank=True)
    ticket = EmbeddedModelField("Ticket", editable=True)
    imagepath = models.FilePathField()
    link = models.URLField()
        
    def save(self, *args, **kwargs):
        img = qrcode.make(self.link)
        img.save(self.imagepath, "JPEG")
        super(self.__class__, self).save(*args, **kwargs)

class DesktopConfig(models.Model):
    username = models.CharField(max_length="40")
    dic = DictField()
    hall = models.CharField(max_length=5)
    objects = MongoDBManager()
    
class Reservation(models.Model):
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)
    mobile = models.CharField(max_length=30)
    email = models.CharField(max_length=50)
    pin =models.CharField(max_length=5)
    customer = models.ForeignKey(Customer, null=True, blank=True)
    reservation_category = models.CharField(max_length=30,null=True, blank=True)
    expiration_time = models.FloatField(blank=True,null=True)

class Token(models.Model):
    user_id = models.CharField(max_length=50)
    hardware_id = models.CharField(max_length=80)
    logged_date = models.DateTimeField()
    exp_date = models.DateTimeField(null=True)
    ip = models.CharField(max_length=50)



class CancelledTicketsLog(models.Model):
    uid = models.CharField(max_length=100, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    session = models.ForeignKey(Session)
    ticket_order  = models.ForeignKey(TicketOrder, null=True)
    seat = models.ForeignKey(Seat, null=True, blank=True)
    stype = models.CharField(max_length=20)
    cancel_time = models.DateTimeField(blank=True, null=True)




    @property
    def Price(self):
        return "GEL %s" % self.price
    process = models.CharField(max_length=2)

    PRINTED = "PR"
    DIGITAL = "DG"
    TICKET_TYPES = ((PRINTED, "Printed"),(DIGITAL, "Digital"))
    ttype = models.CharField(max_length=2,choices=TICKET_TYPES, default=PRINTED)
    has_discount = models.BooleanField(default=False)
    price = models.FloatField()#DecimalField(max_digits=6,decimal_places=2)
    date_printed = models.DateField(blank=True, null=True)
    date_sold = models.DateTimeField(blank=True, null=True)
    objects = MongoDBManager()

    def save(self, *args, **kwargs):

        if not self.price:
            if self.seat.ttype == "std":
                self.price = self.session.prices["std"]
                self.stype = "Standard"
            elif self.seat.ttype == "eco":
                self.stype = "Economy"
                self.price = self.session.prices["eco"]
            elif self.seat.ttype == "vip":
                self.stype = "VIP"
                self.price = self.session.prices["vip"]
            elif self.seat.ttype == "sta":
                self.stype = "Status"
                self.price = self.session.prices["sta"]
        super(self.__class__, self).save(*args, **kwargs)

    def __unicode__(self):
        return "%s_%s_%s" % (self.session.hall.name,self.session.start_time,self.seat)



class AccessLogger(models.Model):
    client_ip = models.CharField(max_length=30, null=True)
    accessed = models.DateTimeField(auto_now_add=True, blank=True)
    url_accessed = models.URLField()
    user = models.CharField(max_length=50, null=True)

#film industry metadata
class MovieDescription(models.Model):
    movie = models.ForeignKey(Movie, null=True, blank=True)
    genre = models.CharField(max_length=256, null=True, blank=True)
    directors = models.CharField(max_length=256, null=True, blank=True)
    producers = models.CharField(max_length=256, null=True, blank=True)
    actorsmain = models.CharField(max_length=256, null=True, blank=True)
    script = models.CharField(max_length=256, null=True, blank=True)
    budget = models.CharField(max_length=256, null=True, blank=True)
    premiere = models.CharField(max_length=256, null=True, blank=True)
    premiere_in_georgia = models.CharField(max_length=256, null=True, blank=True)
    description_short = models.CharField(max_length=256, null=True, blank=True)
    description_long = models.CharField(max_length=512, null=True, blank=True)
    poster_url = models.CharField(max_length=256, blank=True, null=True, editable=False)
    trailer_url = models.CharField(max_length=256, null=True, blank=True, editable=False)
    imdb_score = models.FloatField(blank=True, default=5.0)
    fan_score = models.FloatField(blank=True, default=3.5)
    objects = MongoDBManager()

    def movie_name(self):
        return self.movie.name

    def __init__(self, *args, **kwargs):
        super(MovieDescription, self).__init__(*args, **kwargs)
        if not self.poster_url and self.movie:
            self.poster_url = self.movie.poster_url
        if not self.trailer_url and self.movie:
            self.trailer_url = self.movie.trailer_url


class MovieDescriptionEnglish(models.Model):
    movie = models.ForeignKey(Movie, null=True, blank=True)
    genre = models.CharField(max_length=256, null=True, blank=True)
    directors = models.CharField(max_length=256, null=True, blank=True)
    producers = models.CharField(max_length=256, null=True, blank=True)
    actorsmain = models.CharField(max_length=256, null=True, blank=True)
    script = models.CharField(max_length=256, null=True, blank=True)
    budget = models.CharField(max_length=256, null=True, blank=True)
    premiere = models.CharField(max_length=256, null=True, blank=True)
    premiere_in_georgia = models.CharField(max_length=256, null=True, blank=True)
    description_short = models.CharField(max_length=256, null=True, blank=True)
    description_long = models.CharField(max_length=512, null=True, blank=True)
    poster_url = models.CharField(max_length=256, blank=True, null=True, editable=False)
    trailer_url = models.CharField(max_length=256, null=True, blank=True, editable=False)
    imdb_score = models.FloatField(blank=True, default=5.0)
    fan_score = models.FloatField(blank=True, default=3.5)
    objects = MongoDBManager()

    def movie_name(self):
        return self.movie.name

    def __init__(self, *args, **kwargs):
        super(MovieDescriptionEnglish, self).__init__(*args, **kwargs)
        if not self.poster_url and self.movie:
            self.poster_url = self.movie.poster_url
        if not self.trailer_url and self.movie:
            self.trailer_url = self.movie.trailer_url
    
    # def poster_url_get(self):
    #     return self.movie.poster_url

class AngularTest(models.Model):
    name = models.CharField(max_length=50)
    site = models.CharField(max_length=200)
    description = models.CharField(max_length=400)










