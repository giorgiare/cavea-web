from testing.models import Seat
from django.shortcuts import get_object_or_404
from django.utils.html import strip_tags
from django_socketio import events

from django.http.response import HttpResponse


@events.on_message(channel="^session")
def message(request, socket, context, message):
    #socket.broadcast(context)
    #    socket.broadcast(message)
    ch_name = message["channel"]
    socket.broadcast_channel(message, channel=ch_name)
    

@events.on_subscribe
def subscribe(request, socket, context, channel):
    socket.broadcast_channel("connected", channel=channel)
    

@events.on_finish(channel="^room-")
def finish(request, socket, context):
    """
    Event handler for a socket session ending in a room. Broadcast
    the user leaving and delete them from the DB.
    """
    try:
        user = context["user"]
    except KeyError:
        return
    left = {"action": "leave", "name": user.name, "id": user.id}
    socket.broadcast_channel(left)
    user.delete()