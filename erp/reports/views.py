# -*- coding: utf-8 -*-
from pymongo import *
import pymongo
from bson.objectid import ObjectId
from erp.settings import DATABASES

from django.views.decorators.csrf import csrf_exempt
import random
import testing
import django_socketio
from jsonify.decorators import ajax_request
from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail, EmailMessage
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render

from testing.models import *
from api.models import *
# from api.models import ClientRegistry

from django.core.serializers.json import DjangoJSONEncoder
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
import datetime
import json
from django.http.response import HttpResponseRedirect, HttpResponse
from decimal import Decimal
from django.core import serializers
from django.conf import settings
from pip._vendor.requests.sessions import session
from django_socketio import events
import dateutil.parser
from datetime import timedelta


def mongo_connect():
    conn = MongoClient()
    db = conn[DATABASES["default"]["NAME"]]
    return db


@csrf_exempt
def all_tickets_report(request):
    if not (request.POST['datefrom'] and request.POST['dateto'] and request.POST['cinema_id'] and request.POST[
        'hall_id'] and request.POST['type'] and request.POST['film_id']):
        return HttpResponse("error!")

    conn = mongo_connect()

    datefrom = request.POST['datefrom']
    dateto = request.POST['dateto']
    cinema_id = request.POST['cinema_id']
    hall_id = request.POST['hall_id']
    type = request.POST['type']
    film_id = request.POST['film_id']

    pline = []

    result = {
        "DateFrom": datefrom,
        "DateTo": dateto,
        "type": type,
        "film": "All",
        "cinema": "All",
        "hall": "All",
        "data": []
    }

    dateformat = "%Y-%m-%d"
    fromto = {
        "$gte": datetime.datetime.strptime(datefrom, dateformat),
        "$lt": datetime.datetime.strptime(dateto, dateformat) + datetime.timedelta(days=1)
    }

    if cinema_id != "all":
        halls = list(conn["testing_hall"].find({"theater_id": ObjectId(cinema_id)}))
        hall_list = [i["_id"] for i in halls]  # ID-ebis wamogeba
        pline.append({"$match": {"hall_id": {"$in": hall_list}}})
        result["cinema"] = list(conn["testing_theater"].find({"_id": ObjectId(cinema_id)}))[0]["name"]

    if film_id != "all":
        cdata = list(conn["testing_movie"].find({"_id": ObjectId(film_id)}))[0]
        cname = cdata["name"]
        pline.append({"$match": {"m_name": cname}})
        result["film"] = cname

    if hall_id != "all":
        result["hall"] = list(conn["testing_hall"].find({"_id": ObjectId(hall_id)}))[0]["title"]
        pline.append({"$match": {"hall_id": ObjectId(hall_id)}})

    if type == "event":
        pline.append({"$match": {"start_time": fromto}})

    pline.append({"$project": {"_id": "$_id", "tickets": "$tickets"}})
    pline.append({"$unwind": "$tickets"})

    if type == "date":
        pline.append({"$match": {"tickets.date_sold": fromto}})

    pline.append({"$group": {"_id": "$tickets.stype", "total": {"$sum": "$tickets.price"}, "qty": {"$sum": 1}}})

    k = conn["testing_session"]
    dbresult = k.aggregate(pline)["result"]

    for ttype in dbresult:
        result["data"].append({
            "type": ttype["_id"],
            "total": ttype["total"],
            "qty": ttype["qty"]
        })

    return HttpResponse(json.dumps(result))  # serializers.serialize("json",result)


    # mytest(datefrom="2015-03-05", dateto="2015-04-05", hall_id="54a80c0270371f0b6890f002", type="date",
    # filmname="Once upon a Time in America")


@csrf_exempt
def bar_full_report(request):
    if not (request.POST['datefrom'] and request.POST['dateto'] and request.POST['client_id']):
        return HttpResponse("error!")

    conn = MongoClient()["testdb"]
    barorders = conn["barorders"]
    products_list = list(conn["products"].find())

    products_names = {}
    for i in products_list:
        products_names[i["_id"]] = i["name"]

    print products_list
    pline = []
    datefrom = request.POST["datefrom"]
    dateto = request.POST["dateto"]
    client_id = request.POST["client_id"]

    dateformat = "%Y-%m-%d"
    fromto = {
        "$gte": datetime.datetime.strptime(datefrom, dateformat),
        "$lt": datetime.datetime.strptime(dateto, dateformat) + datetime.timedelta(days=1)
    }

    pline.append({"$match":
                      {"created": fromto}
                  })

    pline.append({"$group":
                      {
                          "_id": "1",
                          "cash": {"$sum": "$paidByCash"},
                          "credit": {"$sum": "$paidByCreditCard"}
                      }
                  })
    request = list(barorders.aggregate(pline)["result"])


    if len(request) > 0:
        k = request[0]
    else:
        k = {"cash": 0.0, "credit": 0.0}

    pline_products = []
    pline_products.append({"$match":
                               {"created": fromto}
                           })
    if client_id != "all":
        pline_products.append({"$match":
                                   {"client_id": ObjectId(client_id)}
                               })

    pline_products.append({"$unwind": "$items"})
    pline_products.append({"$group": {
        "_id": "$items.product_id",
        "qty": {"$sum": "$items.qty"},
        "total_value": {
            "$sum": {
                "$multiply": ["$items.priceAtSale", "$items.qty"]
            }
        }
    }
                           })

    items_info = list(barorders.aggregate(pline_products)["result"])

    for i in items_info:
        i["name"] = products_names[i["_id"]]

    return HttpResponse(json.dumps({"total": k, "items": items_info}, indent=4))