
from django.db import models
from django.core.validators import RegexValidator
from decimal import Decimal
from django.contrib.auth.models import User
from HR.models import Employee
from datetime import datetime
from production.models import Line
from utilities.models import  CompanyType, TermsOfPayment, Doct


class Company(models.Model):

    name = models.CharField(max_length=170, blank=False)
    caption = models.TextField(blank=True, null=True, editable=False)
    type = models.ForeignKey(CompanyType, null=True, blank=True)
    address = models.CharField(max_length=80,blank=True, null=True, editable=False)
    zip_code = models.CharField(max_length=40, blank=True, null=True, editable=False)
    city = models.CharField(max_length=70,blank=True, null=True, editable=False)
    country = models.CharField(max_length=18, blank=True, null=True, editable=False)
    phone = models.CharField(max_length=70,blank=True, null=True, editable=False)
    fax = models.CharField(max_length=30, blank=True, null=True, editable=False)
    email = models.EmailField(blank=True,  null=True, editable=False)
    our_id = models.CharField(max_length=75, blank=True, null=True, editable=False)
    IBAN = models.CharField(max_length=35, blank=True,validators=[RegexValidator(regex='^.{22}$', message='Length has to be 22 ', code='nomatch')], null=True, editable=False)
    BIC = models.CharField(max_length=25, blank=True, null=True, editable=False)
    contact_person = models.ManyToManyField("Person",  blank=True)
    terms_of_payment = models.ForeignKey(TermsOfPayment, null=True, blank=True)
    company_uid = models.CharField(max_length=20, null=True, blank=True, editable=False)
    address_rel = models.ManyToManyField("Address", blank=True)

    created_by = models.ForeignKey(User, related_name='company_created_by', null=True, editable=False)
    created_on = models.DateTimeField(auto_now_add = True, null=True)
    edited_by  = models.ForeignKey(User, related_name='company_edited_by', null=True, editable=False)
    edited_on  = models.DateTimeField(auto_now = True, null=True)


    def __unicode__(self):
        return self.name
    class Meta:
        ordering = ['name']
        verbose_name_plural = "companies"

    def save(self, *args, **kwargs):
        if not self.company_uid:
            self.company_uid = 60000+len(Company.objects.all())
        super(self.__class__,self).save(*args,**kwargs)


class Person(models.Model):
    first_name = models.CharField(max_length=120, blank=True, null=True)
    last_name = models.CharField(max_length=80, blank=True, null=True)
    email = models.EmailField(blank=True, default="nomail@gmail.com", null=True)
    phone_number = models.CharField(max_length=40, blank=True, null=True)
    position = models.CharField(max_length=50, blank=True, null=True)
    title = models.CharField(max_length=30, blank=True, null=True)
    company_uid = models.CharField(max_length=30, blank=True, null=True)
    prefix = models.CharField(max_length=10, null=True, blank=True)
    contactt = models.ManyToManyField(Doct)

    def __unicode__(self):
        return u'%s %s' % (self.first_name, self.last_name)



class Report(models.Model):
    name = models.CharField(max_length=200)
    file = models.FileField(upload_to="reports/")
    insider_attendees = models.ManyToManyField(Employee)
    outsider_attendees = models.ManyToManyField(Person)
    meeting_date = models.DateField(default=datetime.now)
    line = models.ForeignKey(Line, null=True)
    company = models.ForeignKey(Company, null=True)
    created_by = models.ForeignKey(User, related_name='report_created_by', editable=False)
    created_on = models.DateTimeField(auto_now_add = True, null=True)
    edited_by  = models.ForeignKey(User, related_name='report_edited_by', null=True, editable=False)
    edited_on  = models.DateTimeField(auto_now = True, null=True)



class Tag(models.Model):
    loc_id = models.CharField(max_length=10)
    name = models.CharField(max_length=70)
    description = models.TextField(null=True)

    def __unicode__(self):
        return "%s %s" % (self.loc_id, self.name)




class Address(models.Model):
    name = models.CharField(max_length=170, blank=False)
    caption = models.TextField(null=True, blank=True)
    department = models.CharField(max_length=200, blank=True, null=True)
    address = models.CharField(max_length=80,blank=True, null=True)
    zip_code = models.CharField(max_length=40, blank=True, null=True)
    city = models.CharField(max_length=70,blank=True, null=True)
    country = models.CharField(max_length=18, blank=True, null=True)
    phone = models.CharField(max_length=70,blank=True, null=True)
    fax = models.CharField(max_length=30, blank=True, null=True)
    email = models.EmailField(blank=True,  null=True)
    addrt = models.ManyToManyField(Doct, blank=True)

    class Meta:
        verbose_name_plural = "Addresses"

    def __unicode__(self):
        return self.name



