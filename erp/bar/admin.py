from django.contrib import admin
from testing.models import *
from models import *

class ClientAdmin(admin.ModelAdmin):
    list_display = ("ip",)


class PermissionAdmin(admin.ModelAdmin):
    list_display = ("name","model_name", "codename")


class AgentAdmin(admin.ModelAdmin):
    list_display = ("ttype", "name")

def product_name(object):
    return "%s - %s" % (object.product.sku,object.product.name,)
class InventoryAdmin(admin.ModelAdmin):
    list_display = (product_name,"bar", "qty", )
    list_filter = ("bar",)
def get_qty_of_ingr_inventory(object):
    return "%s%s" % (str(object.qty*1000), object.ingredient.measure)

def RecipeIngredient(objects):
    return "%s%s" %(objects.qty, objects.ingredient.measure)

class ProductAdmin(admin.ModelAdmin):
    list_display = ("sku","name", "stock")

class CategoryAdmin(admin.ModelAdmin):
    list_display = ("name",)

class BarAdmin(admin.ModelAdmin):
    list_display = ("uid",)
class IngredientAdmin(admin.ModelAdmin):
    list_display = ("name",)
class RecipeItemAdmin(admin.ModelAdmin):
    list_display = ("ingredient",RecipeIngredient, "product")
    list_filter = ("product",)
class IngrInventoryAdmin(admin.ModelAdmin):
    list_display = ("ingredient",get_qty_of_ingr_inventory, "bar")
    list_filter = ("bar",)


admin.site.register(Client, ClientAdmin)
admin.site.register(Permission, PermissionAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Bar, BarAdmin)
admin.site.register(Inventory, InventoryAdmin)
admin.site.register(Ingredient, IngredientAdmin)
admin.site.register(RecipeItem, RecipeItemAdmin)
admin.site.register(IngredientInventory, IngrInventoryAdmin)