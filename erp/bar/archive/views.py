from django.http import HttpResponse
from django.shortcuts import render
from bar.models import Category, Product
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from testing.views import validate_posted_variables
# -*- coding: utf-8 -*-
from django.middleware.csrf import get_token
from django.views.decorators.csrf import csrf_exempt
import random
import django_socketio
from jsonify.decorators import ajax_request
from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail, EmailMessage
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render
from testing.models import *
#from api.models import *
from api.models import Client
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
import datetime
import json
from django.http.response import HttpResponseRedirect, HttpResponse
from decimal import Decimal
from django.core import serializers
from django.http import HttpResponseRedirect
import json
from bar.forms import ProductForm, CategoryForm, CSVUploadForm
from django.conf import settings
from django.contrib.auth.models import User as DjangoUser
from bar.models import BarOrder as BarBarOrder, Item, Product as BarProduct, BarTransaction, CancelledBarOrder, Inventory, Bar, Item
import csv

def handle_uploaded_file(f, name):
    #'/home/giorgiare/temp_repo/rustaveli/erp/media/product/images/base'
    destination = open(name, 'w')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()


def catalog(request):
    categories = Category.objects.all().exclude(name="root")
    obj = []
    for category in categories:
        obj.append(has_children(category))
    return render(request, "bar/catalog.html", {"categories": categories, "obj":json.dumps(obj)})


def has_children(category):
    dict = {}
    dict["name"] = category.name
    dict["pk"] = category.pk
    if len(category.children):
        children = []
        for cat in category.children:
            children.append(has_children(cat))
        dict["children"] = children
    return dict


@csrf_exempt
def get_category_products(request):
    if validate_posted_variables(request, "cat_id"):
        category = Category.objects.get(pk=request.POST["cat_id"])
        products = category.products
        p_list = []
        for product in products:
            print product
            product_ = Product.objects.get(pk=product)
            product_.inventory = []
            p_list.append(product_)

        return HttpResponse(serializers.serialize("json", p_list))


'#utilities'

def client_registry_checked(ip, hardware_id, location_id ):
    print ip, hardware_id, location_id
    reg = ClientRegistry.objects.filter(ip=ip,hardware_id=hardware_id, location_id=location_id)
    if reg:
        return True
    else:
        return False

def user_has_permission(user,permission):
    for perms in user.permissions:
        if perms.codename==permission:
            return True
    return False

def theaterApi(request):
    if "theater" in request.GET and request.GET["theater"]:
        theater = Theater.objects.get(name=request.GET["theater"])
        halls = Hall.objects.filter(theater=theater)
        return {"response":serializers.serialize("json", halls)}
    return HttpResponse("done")


def validate_posted_variables(request, *args):
    coll = []
    for arg in args:
        if arg in request.POST and request.POST[arg]:
            coll.append(True)
    if len(coll)==len(args):
        return True

@csrf_exempt
def createBarOrder(request):
    if validate_posted_variables(request,  "client_id", "user_id"):
        client_id = request.POST["client_id"]
        user_id = request.POST["user_id"]
	return HttpResponse(client_id)
        client = Client.objects.get(pk=client_id)
	return HttpResponse(client.pk)
        user = DjangoUser.objects.get(pk=user_id)
        #if user_has_permission(user, "create_bar_order") and client_registry_checked(client.ip,client.hardware_id, client.emb_obj.uid):
        if user in client.users:
            str = "%s%s" % (client.pk, user.pk)
            barOrder = BarBarOrder(client=client, user=user)
            barOrder.items = []
            bar = client.emb_obj
            json_items = json.loads(request.POST['items'])
            for json_item in json_items:
                product_id, qty = json_item
                product = BarProduct.objects.get(pk=product_id)
                item = Item(product=product, qty=qty)
                bar = Bar.objects.get(pk=client.emb_obj.uid)
                inventory = Inventory.objects.get(product_id=item.product.pk, bar=bar)
                inventory.qty = int(inventory.qty) - int(item.qty)
                inventory.save()
                for inv in product.inventory:
                    if inv.bar_id==bar.pk:
                        #if inv.qty -int(qty)>0:
                        inv.qty = inv.qty - int(qty)
                        product.save()
                        #else:
                        #    return HttpResponse("inventory negative")
                barOrder.total = barOrder.total + product.price*int(qty)
                barOrder.items.extend([item])
                barOrder.save()
            return HttpResponse(barOrder.pk)
        else:
            return HttpResponse("user not authorized")
    else:
	return HttpResponse("not validated")

	


def remove_embedded_item(bar_order_id, product_id):
    BarBarOrder.objects.raw_update({"uid": bar_order_id }, {'$pull' : {'items' : {'product.sku' : product_id}}})

@csrf_exempt
def createTransaction(request):
    if validate_posted_variables(request, "bar_order_id", "client_id", "user_id", "p_method", "ttype", "value"):
        client_id = request.POST["client_id"]
        user_id = request.POST["user_id"]
        ttype = request.POST["ttype"]
        p_method = request.POST["p_method"]
        client = Client.objects.get(pk=client_id)
        user = DjangoUser.objects.get(pk=user_id)
        bar_order = BarBarOrder.objects.get(pk=request.POST["bar_order_id"])
        tr = []
        '''
        try:
            tr = BarTransaction.objects.get(order_id=bar_order)
        except BarTransaction.DoesNotExist:
            tr = None
        if not tr:
        '''
        transaction = BarTransaction(order=bar_order)
        transaction.payment_method = p_method
        transaction.ttype = ttype
        transaction.total=0
        value = request.POST["value"]
        for item in bar_order.items:
            transaction.total += item.qty*item.product.price
        if ttype=="CN":
            cancelled_order = CancelledBarOrder()
            cancelled_order.populate(bar_order)
            cancelled_order.save()
            bar_order.delete()
        if value:
            transaction.total = -1*Decimal(value)
        else:
            transaction.total = -1*transaction.total
        transaction.save()
        return HttpResponse("Transaction created")
        '''
        else:
            transaction.total = -1*transaction.total
        '''


@csrf_exempt
def removeItemsFromBarOrder(request):
    if validate_posted_variables(request, "items", "bar_order_id", "client_id", "user_id"):
        client_id = request.POST["client_id"]
        product_id = request.POST["items"]
        user_id = request.POST["user_id"]
        bar_order_id = request.POST["bar_order_id"]

        barOrder = BarBarOrder.objects.get(pk=bar_order_id)
        client = Client.objects.get(pk=client_id)
        user = DjangoUser.objects.get(pk=user_id)
        bar = client.emb_obj
        json_items = json.loads(request.POST['items'])

        if user in client.users:
            for json_item in json_items:
                product_id, qty = json_item
                product = Product.objects.get(pk=product_id)
                for inv in product.inventory:
                    if inv.bar ==bar:
                        inv.qty = inv.qty + int(qty)
                product.save()
                for item in barOrder.items:
                    if product_id==item.product.pk:
                        item.qty = item.qty - int(qty)
                    barOrder.save()
                    if item.qty ==0:
                        item = Item(qty=qty,product=product)
                        barOrder.items.remove(item)
                        barOrder.save()

        return HttpResponse(product.sku)


@csrf_exempt
def cancelBarOrder(request):
    if validate_posted_variables(request, "client_id", "user_id", "bar_order_id"):
        client_id = request.POST["client_id"]
        user_id = request.POST["user_id"]
        bar_order_id = request.POST["bar_order_id"]
        client = Client.objects.get(pk=client_id)
        user = User.objects.get(pk=user_id)
        barOrder = BarBarOrder.objects.get(pk=bar_order_id)
        if user in client.users:
        #if user_has_permission(user, "cancel_bar_order") and client_registry_checked(client.ip,client.hardware_id, client.emb_obj.uid):
            items = barOrder.items
            bar_id = barOrder.client.emb_obj.pk
            for item in items:
                bar = Bar.objects.get(pk=client.emb_obj.uid)
                inventory = Inventory.objects.get(product_id=item.product.pk, bar=bar)
                inventory.qty = int(inventory.qty) + int(item.qty)
                inventory.save()
                product = item.product
                for inv in product.inventory:
                    if inv.bar.pk == bar_id:
                        inv.qty = inv.qty + item.qty
                product.save()

            return HttpResponse(barOrder.pk)

@login_required
def bar(request):
    return render(request, "bar.html")

def BarCategoryCreate(request):
    errors = []
    text = ""
    created_cat = None
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if not request.POST.get('name', ''):
            errors.append('Enter name')

        if not request.POST.get("parent", ""):
            errors.append('Enter Parent.')

        if not errors:

            name = request.POST.get("name", "")
            children = request.POST.getlist("children")
            products = request.POST.getlist("products")
            cat = Category(name=name)
            prods = []
            for product in products:
                pr = Product.objects.get(pk=product)
                prods.append(pr)
            cats = []
            for child in children:
                ch = Category.objects.get(pk=child)
                cats.append(ch)

            cat.children.extend([obj for obj in cats])
            cat.products.extend([obj for obj in prods])

            cat.enabled = request.POST.get("enabled", "")
            cat.show_in_navigation = request.POST.get("show_in_navigation", "")
            if not request.POST["parent"]=="none":
                parent = Category.objects.get(pk=request.POST.get("parent", ""))
                cat.parent = parent
            cat.save()
            if not request.POST["parent"]=="none":

                parent = Category.objects.get(pk=request.POST["parent"])
                parent.children.extend([cat])
                parent.save()
            text = "Category Successfully Created"
            created_cat = {"id":cat.uid, "name":cat.name}
            form = CategoryForm()
    else:
        form = CategoryForm()
    products = Product.objects.all()
    categories = Category.objects.all()
    return render(request, 'bar/BarCategoryCreate.html', {
        'errors': errors,
        'categories':categories,
        "products": products,
        "form":form,
        "text":text,
        "created_cat": created_cat
    })


def BarProductCreate(request):
    errors = []
    text = ""
    created_prod = None
    if request.method == 'POST':

        form = ProductForm(request.POST,request.FILES)

        if not request.POST.get('sku', ''):
            errors.append('Enter SKU.')
        if not request.POST.get('name', ''):
            errors.append('Enter name')
        if not request.POST.get('description', ''):
            errors.append('Enter description.')
        if not request.POST.get('price',''):
            errors.append('Enter Price.')
        if not request.POST.get('barcode',''):
            errors.append('Enter barcode.')

        if request.POST.get("has_discount"):
            if not request.POST.get('special_price',''):
                errors.append('Enter Special Price.')

        for f in request.FILES.getlist('image_base'):
            handle_uploaded_file(f, settings.MEDIA_ROOT+"product/images/base/%s.jpg" % (request.POST["sku"]))
        for f in request.FILES.getlist('image_thumbnail'):
            handle_uploaded_file(f, settings.MEDIA_ROOT+"product/images/thumbnail/%s.jpg" % (request.POST["sku"]))

        if not errors and form.is_valid():

            form.save()
            data = form.cleaned_data
            created_prod = {"id":data["sku"], "name":data["name"]}
            text = "Product successfully created"
    elif request.method == "GET" and request.GET and "uid" in request.GET:
        product = Product.objects.get(pk=request.GET["uid"])
        dict = {"sku":product.sku, "name":product.name, "barcode":product.barcode}
        dict["description"] = product.description
        dict["enabled"] = product.enabled
        dict["price"] = product.price
        dict["has_discount"] = product.has_discount
        dict["special_price"] = product.special_price
        form = ProductForm(initial=dict)
    else:
        form = ProductForm()
    return render(request, 'bar/BarProductCreate.html', {
        "text":text,
        "created_prod":created_prod,
        "form":form
    })

def clear_category(uid):
    cat = Category.objects.get(uid=uid)
    prods = cat.products
    for p in prods:
        Category.objects.raw_update({"uid":str(cat.uid)},{"$pull":{"products":{"sku":str(p.sku)}}})
    return True

def BarCategoryEdit(request):

    errors = []
    parent = None
    children = None
    products = None
    prods = []
    text = ""
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if not request.POST.get('name', ''):
            errors.append('Enter name')



        if not errors:
            category = Category.objects.filter(pk=int(request.POST["uid"]))
            name = request.POST.get("name", "")
            enabled = request.POST.get("enabled", "")
            show_in_navigation = request.POST.get("show_in_navigation", "")
            if request.POST.get("parent", "")!="none":
                parent = Category.objects.filter(pk=request.POST.get("parent", ""))
            children = request.POST.getlist("children")
            products = request.POST.getlist("products")

            if not len(category):

                cat = Category(name=name)
                prods = []
                for product in products:
                    pr = Product.objects.get(pk=product)
                    prods.append(pr)
                cats = []
                for child in children:
                    ch = Category.objects.get(pk=child)
                    cats.append(ch)

                    cat.children.extend([obj for obj in cats])
                    cat.products.extend([obj for obj in prods])

                cat.enabled = enabled
                cat.show_in_navigation = show_in_navigation
                cat.parent = parent
                cat.save()
                parent = Category.objects.get(pk=request.POST["parent"])
                parent.children.extend([cat])
                parent.save()
                return HttpResponse('category created')
            else:

                category = category[0]
                category.name=name
                prods = []
                category.products = []
                category.children = []
                category.save()
                category = Category.objects.get(id=category.id)
                for product in products:
                    pr = Product.objects.get(pk=product)
                    prods.append(pr)
                cats = []
                for child in children:
                    ch = Category.objects.get(pk=child)
                    cats.append(ch)


                category.children.extend([obj for obj in cats])
                category.products.extend([obj for obj in prods])


                category.enabled = enabled
                category.show_in_navigation = show_in_navigation
                if parent:
                    category.parent = parent[0]
                category.save()
                if request.POST["parent"] is not "none":
                    if request.POST["parent"]!="none":
                        parent = Category.objects.get(pk=request.POST["parent"])
                        parent.children.extend([category])
                        parent.save()

                return HttpResponseRedirect("/bar/category/edit?uid="+str(category.id)+"&text=success")
    elif request.method == "GET" and request.GET and "uid" in request.GET:
        category = Category.objects.get(pk=request.GET["uid"])
        children = category.children
        parent = category.parent
        prods = category.products
        dict = {"name":category.name, "enabled":category.enabled, "show_in_navigation":category.show_in_navigation}
        form = CategoryForm(initial=dict)
        if "text" in request.GET:
            text = "Category updated successfully"
    else:
        form = CategoryForm()
    products = Product.objects.all()
    categories = Category.objects.all()
    return render(request, 'bar/BarCategoryEdit.html', {
        'errors': errors,
        'categories':categories,
        "products": products,
        "parent":parent,
        "children":children,
        "prods":prods,
        "cat_uid":request.GET.get("uid", ""),
        "form":form,
        "text":text,
        "url": request.META["HTTP_HOST"]

    })

def ProductList(request):
    products = Product.objects.all()
    return render(request, "bar/ProductList.html", {"products":products})


def CSVUpload(request):
    if request.method =="POST":
        file = request.FILES["file"]
        reader = csv.reader(file)
        for row in reader:
            print row[0]
        form = CSVUploadForm(request.FILES)
    else:
        form = CSVUploadForm()
    return render(request, "bar/CSVUpload.html", {"form":form})



# giorgis damatebuli
def getCategories(request):
    categories = productCategory.objects.all()
    jsonOfCategories = serializers.serialize("json",categories)
    return HttpResponse(jsonOfCategories)


def getAllProducts(request):
    products = barProduct.objects.all()
    jsonOfbarProducts = serializers.serialize("json", products)
    return HttpResponse(jsonOfbarProducts)


def productsByCategory(request, categoryId):
    catId=categoryId
    print request
    products = barProduct.objects.filter(productCategoryID=catId)
    jsonOfProducts=serializers.serialize("json", products)
    print jsonOfProducts
    return HttpResponse (jsonOfProducts)

# giorgis damatebulis dasasruli
