from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from bar.views import *


urlpatterns = patterns('',
    # Examples:
    url(r'^catalog/$', catalog),
    url(r'^get_category_products/$', get_category_products),
    url(r'^product/create/$', BarProductCreate),
    url(r'^category/create/$', BarCategoryCreate),
    url(r'^category/edit/$', BarCategoryEdit),
    url(r'^product/list/$', ProductList),
    url(r'^category/clear/(?P<uid>\w+)$', clear_category),

    #url(r'^getProductsByCategory.api/$', getProductsByCategory),

    url(r'^cancelBarOrder/$',cancelBarOrder),
    url(r'^createBarOrder/$',createBarOrder),
    url(r'^removeItemsFromBarOrder/$',removeItemsFromBarOrder),
    url(r'^createTransaction/$',createTransaction),
    url(r'^CSVUpload/$',CSVUpload),


    url(r'^bar/categories/$', getCategories)


    )