from django.contrib import admin
from testing.models import *
from models import *

class ClientAdmin(admin.ModelAdmin):
    list_display = ("ip",)


class PermissionAdmin(admin.ModelAdmin):
    list_display = ("name","model_name", "codename")


class AgentAdmin(admin.ModelAdmin):
    list_display = ("ttype", "name")

class InventoryAdmin(admin.ModelAdmin):
    list_display = ("bar", "qty", "product")
    list_filter = ("bar",)


class ProductAdmin(admin.ModelAdmin):
    list_display = ("sku","name", "stock")

class CategoryAdmin(admin.ModelAdmin):
    list_display = ("name",)

class BarAdmin(admin.ModelAdmin):
    list_display = ("uid",)

admin.site.register(Client, ClientAdmin)
admin.site.register(Permission, PermissionAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Bar, BarAdmin)
admin.site.register(Inventory, InventoryAdmin)