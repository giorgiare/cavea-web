from django.db import models
import random
from datetime import datetime
from djangotoolbox.fields import EmbeddedModelField, DictField
from django_mongodb_engine.contrib import MongoDBManager
from jsonfield import JSONField
from django.db import models
from djangotoolbox.fields import ListField
from django.contrib.auth.models import User
from datetime import datetime, timedelta
from testing.models import Theater

from django.contrib.auth.models import User as DjangoUser
from django.db.models import signals



class Bar(models.Model):
    uid = models.CharField(max_length=30, primary_key=True)
    #theater = models.ForeignKey(Theater)
    created_on = models.DateField(auto_now_add=True)

    def __unicode__(self):
        return self.uid

class Client(models.Model):
    uid = models.CharField(max_length=120)
    ip = models.CharField(max_length=16)
    users = ListField(EmbeddedModelField(DjangoUser))
    hardware_id = models.CharField(max_length=120)
    ttype = models.CharField(max_length=3,choices=(("box", "Box Office"),("bar", "Bar"), ("web", "Website")))
    emb_obj = EmbeddedModelField("Bar")

    def __unicode__(self):
        return self.uid

def set_ingredients(sender, instance, **kwargs):
    recipeItems = RecipeItem.objects.filter(product=instance)
    if len(recipeItems) and not len(instance.recipe_items):
        instance.recipe_items.extend(recipeItems)
        instance.has_ingredients = True

def set_stock(sender, instance, **kwargs):
    qty = 0
    if instance.inventory:
        for inv in instance.inventory:
            qty+= inv.qty
    if qty:
        instance.is_in_stock = True
    else:
        instance.is_in_stock = False
    instance.stock_qty = qty

class Inventory(models.Model):
    product = models.ForeignKey("Product", related_name="item")
    bar = models.ForeignKey(Bar)
    qty = models.IntegerField(default=100)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    updated = models.DateTimeField(null=True, blank=True)
    show_out_of_stock_at = models.IntegerField(default=0)
    notify_front_about_stock_qty_at = models.IntegerField(default=5)
    is_in_stock = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s-%s" % (self.product, self.bar)

    def save(self, *args, **kwargs):
        product = self.product
        if product.inventory:
            product.inventory.extend([self])
        product.save()
        if self.qty == 0:
            self.is_in_stock = False
        self.updated = datetime.now()
        super(self.__class__, self).save(*args, **kwargs)
    class Meta:
        verbose_name_plural = "Inventories"


# Create your models here.
class Product(models.Model):
    sku = models.CharField(max_length=120, primary_key=True)
    name = models.CharField(max_length=50)
    description = models.TextField(default="product description goes here")
    enabled = models.BooleanField(default=True)
    price = models.FloatField()
    barcode = models.CharField(max_length=120, default="DABARCODE")
    inventory = ListField(EmbeddedModelField(Inventory, editable=False, null=True), editable=False, null=True, blank=True)
    has_discount = models.BooleanField(default=False)
    special_price = models.DecimalField(max_digits=6,decimal_places=2,null=True, blank=True)
    image_base = models.FileField(upload_to="product/images/base/", blank=True, null=True)
    image_thumbnail = models.FileField(upload_to="product/images/thumbnail/", blank=True, null=True)
    stock_qty = models.IntegerField(null=True, editable=False)
    has_ingredients = models.BooleanField(default=False)
    recipe_items = ListField(EmbeddedModelField("RecipeItem",editable=False, null=True), editable=False, null=True, blank=True)

    @property
    def stock(self):
        invs = []
        if self.inventory:
            invs = self.inventory
        qty = 0
        if len(invs):
            for inv in invs:
                qty += inv.qty
        else:
            qty = 0
        return qty
    objects = MongoDBManager()

    def __unicode__(self):
        return self.sku
    def save(self, *args, **kwargs):
        if not self.inventory:
            self.inventory = []
        if not self.recipe_items:
            self.recipe_items = []
        super(self.__class__,self).save(*args, **kwargs)


class Item(models.Model):
    product =models.ForeignKey(Product, related_name="item_product")
    qty = models.IntegerField()
    priceAtSale=models.FloatField()
    priceOnStock=models.FloatField()
    hasDiscount = models.BooleanField(default=False)
    discountAmount = models.FloatField()
    discountId = models.CharField(max_length=24) #for security purpose to be an explicit ID length.
    





    def __unicode__(self):
        return "%s-%s" % (self.product.name, self.qty)


class BarOrder(models.Model):
    uid = models.CharField(max_length=120, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    client = models.ForeignKey(Client)
    user = models.ForeignKey(DjangoUser)
    #emb_user = EmbeddedModelField(User)
    #items = ListField(models.ForeignKey(Item), null=True)
    initialPrice=models.FloatField()
    discountAmount=models.FloatField()
    discountedPrice=models.FloatField()
    paidByCash=models.FloatField()
    paidByCreditCard=models.FloatField()
    paidByGiftCard=models.FloatField()






    items = ListField(EmbeddedModelField(Item), null=True)
    total = models.FloatField()
    status = models.CharField(choices=(("pnd","pending"),("pad","paid"),("cnl","cancelled")),default='pnd', max_length=3)
    objects = MongoDBManager()


    def add_item(self,item):
        self.items.extend([item])
        self.save()

    def save(self, *args, **kwargs):
        self.uid = "%s%s" % (self.client ,str(random.randint(1,100000)).zfill(6))
        super(self.__class__, self).save(*args, **kwargs)

class CancelledBarOrder(models.Model):
    uid = models.CharField(max_length=120,primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    client = models.ForeignKey(Client)
    user = models.ForeignKey(DjangoUser)
    #emb_user = EmbeddedModelField(User)
    #items = ListField(models.ForeignKey(Item), null=True)
    items = ListField(EmbeddedModelField(Item), null=True)
    total = models.DecimalField(max_digits=4, decimal_places=2, default=0)
    status = models.CharField(choices=(("pnd","pending"),("pad","paid"),("cnl","cancelled")),default='pnd', max_length=3)
    objects = MongoDBManager()

    def populate(self, model):
        self.uid = model.pk
        self.created = model.created
        self.client = model.client
        self.user = model.user
        self.items = model.items
        self.total = model.total
        self.status = model.status


    def add_item(self,item):
        self.items.extend([item])
        self.save()


class BarTransaction(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    #client = models.ForeignKey(Client)
    #user = models.ForeignKey(User)
    #amount = models.DecimalField(decimal_places=2, max_digits=3)
    order =models.ForeignKey(BarOrder)
    ttype = models.CharField(choices=(("SL", "Sell"),("CN", "Cancell"),("RT", "Return")), default="SL", max_length=2)
    payment_method = models.CharField(choices=(("csh", "Cash"),("crd","Card")),default='csh', max_length=3)
    total = models.DecimalField(max_digits=9, decimal_places=2)


class Menu(models.Model):
    name = models.CharField(max_length=120)
    products = ListField(EmbeddedModelField("Product"))
    price = models.DecimalField(max_digits=4,decimal_places=2)
    has_discount = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

class Media(models.Model):
    thumbnail_url = models.URLField()
    base_image_url = models.URLField()
    title = models.CharField(max_length=120)
    tumbnail_image_file = models.FilePathField()
    base_image_file = models.FilePathField()

class Category(models.Model):
    id = models.IntegerField(primary_key=True, editable=False)
    name = models.CharField(max_length=30, null=False, blank=False)
    children = ListField(EmbeddedModelField("Category"), editable=False)
    products = ListField(models.ForeignKey(Product), editable=False)
    enabled = models.BooleanField(default=True)
    show_in_navigation = models.BooleanField(default=True)
    parent = models.ForeignKey("self", null=True, blank=True)
    uid = models.CharField(max_length=30, editable=False)
    objects = MongoDBManager()

    def save(self, *args, **kwargs):
        if self.parent:
            parent = self.parent
            parent.children.extend([self])
            parent.save()



        if not self.id:

            self.id = int(len(self.__class__.objects.all())+1)
            self.uid = str(self.id)
        super(self.__class__, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Categories"



class IngredientInventory(models.Model):
    ingredient = models.ForeignKey("Ingredient", related_name="item")
    bar = models.ForeignKey(Bar)
    qty = models.FloatField()
    created = models.DateTimeField(auto_now_add=True, blank=True)
    updated = models.DateTimeField(null=True, blank=True)
    show_out_of_stock_at = models.IntegerField(default=0)
    notify_front_about_stock_qty_at = models.IntegerField(default=5)
    is_in_stock = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s-%s" % (self.ingredient, self.bar)

    def save(self, *args, **kwargs):
        ingredient = self.ingredient
        if ingredient.inventory:
            ingredient.inventory.extend([self])
        ingredient.save()
        if self.qty == 0:
            self.is_in_stock = False
        self.updated = datetime.now()
        super(self.__class__, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Ingredient Inventories"


class Ingredient(models.Model):
    name = models.CharField(max_length=30)
    measure = models.CharField(max_length=3, choices=(("gr", "grams"),("ml","millilitre"),("un", "unit")))
    inventory = ListField(EmbeddedModelField(IngredientInventory), editable=False)

    def __unicode__(self):
        return self.name

class RecipeItem(models.Model):
    product = models.ForeignKey(Product)
    ingredient = models.ForeignKey(Ingredient)
    qty = models.FloatField() # quantity of a specific ingredient per one product

    def __unicode__(self):
        return "%s %s%s" % (self.ingredient, self.qty, self.ingredient.measure)






class BarIncomeLog(models.Model):
    priceWOVat = models.FloatField()
    priceWVat = models.FloatField()
    productId = models.ForeignKey(Product)
 #  productId = models.CharField(max_length=120)
    productProviderName = models.CharField(max_length=100)
    quantity = models.FloatField()
    importDate=models.DateField()




signals.post_init.connect(set_stock, sender=Product)
signals.post_init.connect(set_ingredients, sender=Product)
