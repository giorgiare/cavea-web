from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from bar.views import *


urlpatterns = patterns('',
    # Examples:
    url(r'^catalog/$', catalog),
    url(r'^get_category_products/$', get_category_products),
    url(r'^product/create/$', BarProductCreate),
    url(r'^category/create/$', BarCategoryCreate),
    url(r'^category/edit/$', BarCategoryEdit),
    url(r'^product/list/$', ProductList),
    url(r'^category/clear/(?P<uid>\w+)$', clear_category),
    url(r'^getordersbydate/(?P<dateOfOrder>\d{4}-\d{2}-\d{2})/$', getOrdersByDate),
    url(r'^getordersbydateandclientid/(?P<dateOfOrder>\d{4}-\d{2}-\d{2})/(?P<clientId>\w+)/$', getOrdersByDateAndClientId),
    url(r'^order/(?P<orderId>\w+)/$', getOrderById),
    url(r'^inventorystats/$', getAllInventoriesQuantity),
    url(r'^fillinventory/$', fillInventory),
    #url(r'^getProductsByCategory.api/$', getProductsByCategory),

    url(r'^cancelbarorder/$',cancelBarOrder),
    url(r'^createBarOrder/$',createBarOrder),
    url(r'^removeItemsFromBarOrder/$',removeItemsFromBarOrder),
    url(r'^createTransaction/$',createTransaction),
    url(r'^PRODCSVUpload/$',PRODCSVUpload),
    url(r'^INVCSVUpload/$',INVCSVUpload),
    url(r'^addclientuser/(?P<barid>\w+)/(?P<user>\w+)/(?P<teststring>\w+)/$', addClientUser),

    )
