from django.http import HttpResponse
from django.shortcuts import render
from bar.models import Category, Product
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from testing.views import validate_posted_variables
# -*- coding: utf-8 -*-
from django.middleware.csrf import get_token
from django.views.decorators.csrf import csrf_exempt
import random
import django_socketio
from jsonify.decorators import ajax_request
from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail, EmailMessage
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render
from testing.models import *

from django.core.serializers.json import DjangoJSONEncoder
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
import datetime
import json
from django.http.response import HttpResponseRedirect, HttpResponse
from decimal import Decimal
from django.core import serializers
from django.http import HttpResponseRedirect
import json
from bar.forms import ProductForm, CategoryForm, PRODCSVUploadForm,INVCSVUploadForm
from django.conf import settings
from django.contrib.auth.models import User as DjangoUser
from models import BarOrder as BarBarOrder, Item, Product as BarProduct, BarTransaction, CancelledBarOrder, Inventory, Bar, Item
from models import Client, IngredientInventory  , Ingredient, RecipeItem
import csv
from bar.models import BarOrder
from bar.models import BarIncomeLog
import time
from bson.objectid import ObjectId

def handle_uploaded_file(f, name):
    #'/home/giorgiare/temp_repo/rustaveli/erp/media/product/images/base'
    destination = open(name, 'w')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()


def catalog(request):
    categories = Category.objects.all().exclude(name="root")
    obj = []
    for category in categories:
        obj.append(has_children(category))
    return render(request, "bar/catalog.html", {"categories": categories, "obj":json.dumps(obj)})


def has_children(category):
    dict = {}
    dict["name"] = category.name
    dict["pk"] = category.pk
    if len(category.children):
        children = []
        for cat in category.children:
            children.append(has_children(cat))
        dict["children"] = children
    return dict


@csrf_exempt
def get_category_products(request, categoryId):
    if 1==1:
        category = Category.objects.get(pk=categoryId)
        products = category.products
        print type(products)
        p_list = []
        for product in products:
            pkForProduct=product
            print pkForProduct
            print "----------------"


            specificProduct = Product.objects.get(sku=pkForProduct)
            print specificProduct.barcode

            specificProduct.inventory = []


            p_list.append(specificProduct)

        return HttpResponse(serializers.serialize("json", p_list))


'#utilities'

def client_registry_checked(ip, hardware_id, location_id ):
    print ip, hardware_id, location_id
    reg = ClientRegistry.objects.filter(ip=ip,hardware_id=hardware_id, location_id=location_id)
    if reg:
        return True
    else:
        return False

def user_has_permission(user,permission):
    for perms in user.permissions:
        if perms.codename==permission:
            return True
    return False

def theaterApi(request):
    if "theater" in request.GET and request.GET["theater"]:
        theater = Theater.objects.get(name=request.GET["theater"])
        halls = Hall.objects.filter(theater=theater)
        return {"response":serializers.serialize("json", halls)}
    return HttpResponse("done")




def validate_posted_variables(request, *args):
    coll = []
    for arg in args:
        if arg in request.POST and request.POST[arg]:
            coll.append(True)
    if len(coll)==len(args):
        return True

@csrf_exempt
def createBarOrder(request):
    if validate_posted_variables(request,  "client_id", "user_id"):
        client_id = request.POST["client_id"]
        user_id = request.POST["user_id"]
        client = Client.objects.get(id=ObjectId(client_id.encode('utf-8')))
        user = DjangoUser.objects.get(pk=user_id)


        order_data=json.loads(request.POST["order_data"])
        order_items=order_data['products']
        payment_info=order_data['payment_info']









        #if user_has_permission(user, "create_bar_order") and client_registry_checked(client.ip,client.hardware_id, client.emb_obj.uid):
        if user in client.users:




            str = "%s%s" % (client.pk, user.pk)
            barOrder = BarBarOrder(client=client, user=user, total=0.0) #initialization value bugfix
            barOrder.initialPrice=payment_info['initialPrice']
            barOrder.discountAmount=payment_info['discountAmount']
            barOrder.discountedPrice=payment_info['discountedPrice']
            barOrder.paidByCash=payment_info['paidByCash']
            barOrder.paidByCreditCard=payment_info['paidByCreditCard']
            barOrder.paidByGiftCard=payment_info['paidByGiftCard']





            barOrder.items = []
            bar = client.emb_obj
            json_items = order_items
            for json_item in json_items:
                product_id, qty, priceAtSale, hasDiscount, discountId, discountAmount = json_item
                product = BarProduct.objects.get(pk=product_id)
                price = product.price
                item = Item(product=product, qty=qty, priceAtSale=priceAtSale, priceOnStock=price, hasDiscount=hasDiscount, discountAmount=discountAmount, discountId=discountId)
              # item = Item(product=product, qty=qty, priceAtSale=price)
                bar = Bar.objects.get(pk=client.emb_obj.uid)
                if product.has_ingredients:
                    for recipe_item in product.recipe_items:
                        ingr_inv = IngredientInventory.objects.get(ingredient=recipe_item.ingredient, bar=bar)
                        ingr_inv.qty = ingr_inv.qty - recipe_item.qty/1000
                        ingr_inv.save()
                else:
                    inventory = Inventory.objects.get(product_id=item.product.pk, bar=bar)
                    inventory.qty = int(inventory.qty) - int(item.qty)
                    inventory.save()

                    for inv in product.inventory:
                        if inv.bar_id==bar.pk:
                            #if inv.qty -int(qty)>0:
                            inv.qty = inv.qty - int(qty)
                            product.save()
                            #else:
                            #    return HttpResponse("inventory negative")
                print type(barOrder.total)
                print type(product.price)
                barOrder.total = barOrder.total + product.price*int(qty)
                barOrder.items.extend([item])
                barOrder.save()


            return HttpResponse(barOrder.pk)
        else:
            return HttpResponse("user not authorized")

def remove_embedded_item(bar_order_id, product_id):
    BarBarOrder.objects.raw_update({"uid": bar_order_id }, {'$pull' : {'items' : {'product.sku' : product_id}}})

@csrf_exempt
def createTransaction(request):
    if validate_posted_variables(request, "bar_order_id", "client_id", "user_id", "p_method", "ttype", "value"):
        client_id = request.POST["client_id"]
        user_id = request.POST["user_id"]
        ttype = request.POST["ttype"]
        p_method = request.POST["p_method"]
        client = Client.objects.get(pk=client_id)
        user = DjangoUser.objects.get(pk=user_id)
        bar_order = BarBarOrder.objects.get(pk=request.POST["bar_order_id"])
        tr = []
        '''
        try:
            tr = BarTransaction.objects.get(order_id=bar_order)
        except BarTransaction.DoesNotExist:
            tr = None
        if not tr:
        '''
        transaction = BarTransaction(order=bar_order)
        transaction.payment_method = p_method
        transaction.ttype = ttype
        transaction.total=0
        value = request.POST["value"]
        for item in bar_order.items:
            transaction.total += item.qty*item.product.price
        if ttype=="CN":
            cancelled_order = CancelledBarOrder()
            cancelled_order.populate(bar_order)
            cancelled_order.save()
            bar_order.delete()
            if value:
                transaction.total = -1*Decimal(value)
            else:
                transaction.total = -1*transaction.total
        transaction.save()
        return HttpResponse("Transaction created")
        '''
        else:
            transaction.total = -1*transaction.total
        '''
@csrf_exempt
def removeItemsFromBarOrder(request):
    if validate_posted_variables(request, "items", "bar_order_id", "client_id", "user_id"):
        client_id = request.POST["client_id"]
        product_id = request.POST["items"]
        user_id = request.POST["user_id"]
        bar_order_id = request.POST["bar_order_id"]

        barOrder = BarBarOrder.objects.get(pk=bar_order_id)
        client = Client.objects.get(pk=client_id)
        user = DjangoUser.objects.get(pk=user_id)
        bar = client.emb_obj
        json_items = json.loads(request.POST['items'])

        if user in client.users:
            for json_item in json_items:
                product_id, qty = json_item
                product = Product.objects.get(pk=product_id)
                for inv in product.inventory:
                    if inv.bar ==bar:
                        inv.qty = inv.qty + int(qty)
                product.save()
                for item in barOrder.items:
                    if product_id==item.product.pk:
                        item.qty = item.qty - int(qty)
                    barOrder.save()
                    if item.qty ==0:
                        item = Item(qty=qty,product=product)
                        barOrder.items.remove(item)
                        barOrder.save()

        return HttpResponse(product.sku)

@csrf_exempt
def cancelBarOrder(request):
    if validate_posted_variables(request, "client_id", "user_id", "bar_order_id"):
        client_id = request.POST["client_id"]
        user_id = request.POST["user_id"]
        bar_order_id = request.POST["bar_order_id"]
        client = Client.objects.get(pk=client_id)
        user = DjangoUser.objects.get(pk=user_id)
        barOrder = BarBarOrder.objects.get(pk=bar_order_id)
        print barOrder.uid
        if user in client.users:
        #if user_has_permission(user, "cancel_bar_order") and client_registry_checked(client.ip,client.hardware_id, client.emb_obj.uid):
            items = barOrder.items
            bar_id = barOrder.client.emb_obj.pk
            for item in items:
                bar = Bar.objects.get(pk=client.emb_obj.uid)
                inventory = Inventory.objects.get(product_id=item.product.pk, bar=bar)
                inventory.qty = int(inventory.qty) + int(item.qty)
                inventory.save()
                product = item.product
                for inv in product.inventory:
                    if inv.bar.pk == bar_id:
                        inv.qty = inv.qty + item.qty
                product.save()
            barOrder.delete()

            return HttpResponse(barOrder.pk)

@login_required
def bar(request):
    return render(request, "bar.html")

def BarCategoryCreate(request):
    errors = []
    text = ""
    created_cat = None
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if not request.POST.get('name', ''):
            errors.append('Enter name')

        if not request.POST.get("parent", ""):
            errors.append('Enter Parent.')

        if not errors:

            name = request.POST.get("name", "")
            children = request.POST.getlist("children")
            products = request.POST.getlist("products")
            cat = Category(name=name)
            prods = []
            for product in products:
                pr = Product.objects.get(pk=product)
                prods.append(pr)
            cats = []
            for child in children:
                ch = Category.objects.get(pk=child)
                cats.append(ch)

            cat.children.extend([obj for obj in cats])
            cat.products.extend([obj for obj in prods])

            cat.enabled = request.POST.get("enabled", "")
            cat.show_in_navigation = request.POST.get("show_in_navigation", "")
            if not request.POST["parent"]=="none":
                parent = Category.objects.get(pk=request.POST.get("parent", ""))
                cat.parent = parent
            cat.save()
            if not request.POST["parent"]=="none":

                parent = Category.objects.get(pk=request.POST["parent"])
                parent.children.extend([cat])
                parent.save()
            text = "Category Successfully Created"
            created_cat = {"id":cat.uid, "name":cat.name}
            form = CategoryForm()
    else:
        form = CategoryForm()
    products = Product.objects.all()
    categories = Category.objects.all()
    return render(request, 'bar/BarCategoryCreate.html', {
        'errors': errors,
        'categories':categories,
        "products": products,
        "form":form,
        "text":text,
        "created_cat": created_cat
    })

def BarProductCreate(request):
    errors = []
    text = ""
    created_prod = None
    if request.method == 'POST':

        

        form = ProductForm(request.POST,request.FILES)

        if not request.POST.get('sku', ''):
            errors.append('Enter SKU.')
        if not request.POST.get('name', ''):
            errors.append('Enter name')
        if not request.POST.get('description', ''):
            errors.append('Enter description.')
        if not request.POST.get('price',''):
            errors.append('Enter Price.')
        if not request.POST.get('barcode',''):
            errors.append('Enter barcode.')

        if request.POST.get("has_discount"):
            if not request.POST.get('special_price',''):
                errors.append('Enter Special Price.')

        for f in request.FILES.getlist('image_base'):
            handle_uploaded_file(f, settings.MEDIA_ROOT+"/product/images/base/%s.jpg" % (request.POST["sku"]))
        for f in request.FILES.getlist('image_thumbnail'):
            handle_uploaded_file(f, settings.MEDIA_ROOT+"/product/images/thumbnail/%s.jpg" % (request.POST["sku"]))

        if not errors and form.is_valid():

            form.save()
            data = form.cleaned_data
            created_prod = {"id":data["sku"], "name":data["name"]}
            text = "Product successfully created"
    elif request.method == "GET" and request.GET and "uid" in request.GET:
        product = Product.objects.get(pk=request.GET["uid"])
        dict = {"sku":product.sku, "name":product.name, "barcode":product.barcode}
        dict["description"] = product.description
        dict["enabled"] = product.enabled
        dict["price"] = product.price
        dict["has_discount"] = product.has_discount
        dict["special_price"] = product.special_price
        form = ProductForm(initial=dict)
    else:
        form = ProductForm()
    return render(request, 'bar/BarProductCreate.html', {
        "text":text,
        "created_prod":created_prod,
        "form":form
    })

def clear_category(uid):
    cat = Category.objects.get(uid=uid)
    prods = cat.products
    for p in prods:
        Category.objects.raw_update({"uid":str(cat.uid)},{"$pull":{"products":{"sku":str(p.sku)}}})
    return True

# def BarCategoryEdit(request):
#     errors = []
#     parent = None
#     children = None
#     products = None
#     prods = []
#     text = ""
#     if request.method == 'POST':
#         form = CategoryForm(request.POST)
#         if not request.POST.get('name', ''):
#             errors.append('Enter name')



#         if not errors:
#             category = Category.objects.filter(pk=int(request.POST["uid"]))
#             name = request.POST.get("name", "")
#             enabled = request.POST.get("enabled", "")
#             show_in_navigation = request.POST.get("show_in_navigation", "")
#             if request.POST.get("parent", "")!="none":
#                 parent = Category.objects.filter(pk=request.POST.get("parent", ""))
#             children = request.POST.getlist("children")
#             products = request.POST.getlist("products")

#             if not len(category):

#                 cat = Category(name=name)
#                 prods = []
#                 for product in products:
#                     pr = Product.objects.get(pk=product)
#                     prods.append(pr)
#                 cats = []
#                 for child in children:
#                     ch = Category.objects.get(pk=child)
#                     cats.append(ch)

#                     cat.children.extend([obj for obj in cats])
#                     cat.products.extend([obj for obj in prods])

#                 cat.enabled = enabled
#                 cat.show_in_navigation = show_in_navigation
#                 cat.parent = parent
#                 cat.save()
#                 parent = Category.objects.get(pk=request.POST["parent"])
#                 parent.children.extend([cat])
#                 parent.save()
#                 return HttpResponse('category created')
#             else:

#                 category = category[0]
#                 category.name=name
#                 prods = []
#                 category.products = []
#                 category.children = []
#                 category.save()
#                 category = Category.objects.get(id=category.id)
#                 for product in products:
#                     pr = Product.objects.get(pk=product)
#                     prods.append(pr)
#                 cats = []
#                 for child in children:
#                     ch = Category.objects.get(pk=child)
#                     cats.append(ch)


#                 category.children.extend([obj for obj in cats])
#                 category.products.extend([obj for obj in prods])


#                 category.enabled = enabled
#                 category.show_in_navigation = show_in_navigation
#                 if parent:
#                     category.parent = parent[0]
#                 category.save()
#                 if request.POST["parent"] is not "none":
#                     if request.POST["parent"]!="none":
#                         parent = Category.objects.get(pk=request.POST["parent"])
#                         parent.children.extend([category])
#                         parent.save()

#                 return HttpResponseRedirect("/bar/category/edit?uid="+str(category.id)+"&text=success")
#     elif request.method == "GET" and request.GET and "uid" in request.GET:
#         category = Category.objects.get(pk=request.GET["uid"])
#         children = category.children
#         parent = category.parent
#         prods = category.products
#         dict = {"name":category.name, "enabled":category.enabled, "show_in_navigation":category.show_in_navigation}
#         form = CategoryForm(initial=dict)
#         if "text" in request.GET:
#             text = "Category updated successfully"
#     else:
#         form = CategoryForm()
#     products = Product.objects.all()
#     categories = Category.objects.all()
#     return render(request, 'bar/BarCategoryEdit.html', {
#         'errors': errors,
#         'categories':categories,
#         "products": products,
#         "parent":parent,
#         "children":children,
#         "prods":prods,
#         "cat_uid":request.GET.get("uid", ""),
#         "form":form,
#         "text":text,
#         "url": request.META["HTTP_HOST"]

#     })





def BarCategoryEdit(request):
    errors = []
    parent = None
    children = None
    products = None
    prods = []
    text = ""
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if not request.POST.get('name', ''):
            errors.append('Enter name')



        if not errors:
            category = Category.objects.filter(pk=int(request.POST["uid"]))
            name = request.POST.get("name", "")
            enabled = request.POST.get("enabled", "")
            show_in_navigation = request.POST.get("show_in_navigation", "")
            if request.POST.get("parent", "")!="none":
                parent = Category.objects.filter(pk=request.POST.get("parent", ""))
            children = request.POST.getlist("children")
            products = request.POST.getlist("products")

            if not len(category):

                cat = Category(name=name)
                prods = []
                for product in products:
                    pr = Product.objects.get(pk=product)
                    prods.append(pr)
                cats = []
                for child in children:
                    ch = Category.objects.get(pk=child)
                    cats.append(ch)

                    cat.children.extend([obj for obj in cats])
                    cat.products.extend([obj for obj in prods])

                cat.enabled = enabled
                cat.show_in_navigation = show_in_navigation
                cat.parent = parent
                cat.save()
                parent = Category.objects.get(pk=request.POST["parent"])
                parent.children.extend([cat])
                parent.save()
                return HttpResponse('category created')
            else:

                category = category[0]
                category.name=name
                prods = []
                category.products = []
                category.children = []
                category.save()
                category = Category.objects.get(id=category.id)
                for product in products:
                    pr = Product.objects.get(pk=product)
                    prods.append(pr)
                cats = []
                for child in children:
                    ch = Category.objects.get(pk=child)
                    cats.append(ch)


                category.children.extend([obj for obj in cats])
                category.products.extend([obj for obj in prods])


                category.enabled = enabled
                category.show_in_navigation = show_in_navigation
                if parent:
                    category.parent = parent[0]
                category.save()
                if request.POST["parent"] is not "none":
                    if request.POST["parent"]!="none":
                        parent = Category.objects.get(pk=request.POST["parent"])
                        parent.children.extend([category])
                        parent.save()

                return HttpResponseRedirect("/bar/category/edit?uid="+str(category.id)+"&text=success")
    elif request.method == "GET" and request.GET and "uid" in request.GET:
        category = Category.objects.get(pk=request.GET["uid"])
        children = category.children
        parent = category.parent
        prods = category.products
        dict = {"name":category.name, "enabled":category.enabled, "show_in_navigation":category.show_in_navigation}
        form = CategoryForm(initial=dict)
        if "text" in request.GET:
            text = "Category updated successfully"
    else:
        form = CategoryForm()
    products = Product.objects.all()
    categories = Category.objects.all()
    return render(request, 'bar/BarCategoryEdit.html', {
        'errors': errors,
        'categories':categories,
        "products": products,
        "parent":parent,
        "children":children,
        "prods":prods,
        "cat_uid":request.GET.get("uid", ""),
        "form":form,
        "text":text,
        "url": request.META["HTTP_HOST"]

    })













































def ProductList(request):
    products = Product.objects.all()
    return render(request, "bar/ProductList.html", {"products":products})


def PRODCSVUpload(request):
    if request.method =="POST":
        file = request.FILES["file"]
        reader = csv.reader(file)
        for row in reader:
            product = Product(recipe_items=[])
            product.sku = row[0]
            product.name = row[1]
            product.description = row[2]
            product.price = row[3]
            if row[4]=="True":
                product.enabled = True
            else:
                product.enabled = False
            if row[5]=="True":
                product.has_ingredients= True
            else:
                product.has_ingredients= False

            product.save()
        form = PRODCSVUploadForm(request.FILES)
    else:
        form = PRODCSVUploadForm()
    return render(request, "bar/PRODCSVUpload.html", {"form":form})

def INVCSVUpload(request):
    if request.method =="POST":
        file = request.FILES["file"]
        reader = csv.reader(file)
        for row in reader:
            inv = Inventory()
            prod = Product.objects.get(sku=row[0])
            bar = Bar.objects.get(uid=row[1])
            inv.product = prod
            inv.bar = bar
            inv.qty = row[2]

            inv.save()
        form = INVCSVUploadForm(request.FILES)
    else:
        form = INVCSVUploadForm()
    return render(request, "bar/INVCSVUpload.html", {"form":form})



# giorgis damatebuli

def getCategories(request):
    categories = Category.objects.all()
    cat_list = []
    for cat in categories:
        cat_dict = {"pk":cat.pk, "name":cat.name}
        cat_list.append(cat_dict)


#    jsonOfCategories = serializers.serialize("json", categories, fields=("name", "id"))
    return HttpResponse(json.dumps(cat_list))



def getAllProducts(request):
    products=Product.objects.all()
    jsonOfProducts = serializers.serialize("json", products, fields=('pk','name','price','barcode','has_discount', 'image_thumbnail'))
    return HttpResponse(jsonOfProducts)







def productsByCategory(request, categoryId):
    catId=categoryId
    products = barProduct.objects.filter(productCategoryID=catId)
    jsonOfProducts=serializers.serialize("json", products)
    return HttpResponse (jsonOfProducts)






def getOrderById(request, orderId):
    order = BarOrder.objects.get(uid=orderId)
    order_items=[] #list of order items
    orders = {} #dictionary of orders
    for o in order.items:
        order_items.append(json.loads(serializers.serialize("json", [o])))
    order.items=order_items

    userId = order.user.username # gets appropriate user object related by FK relation with order's field/document "user_id"
    hardwareId = order.client.hardware_id
#    formattedDate = datetime.datetime.strptime(order.created,'%Y-%m-%d')
    formattedDate = "%s-%s-%s %s:%s:%s" % (order.created.strftime("%d"),order.created.strftime("%m"),
        order.created.strftime("%Y"), order.created.strftime("%H"), order.created.strftime("%M"), order.created.strftime("%S"))

    print userId
    print hardwareId
    print formattedDate
    print "------------"
        


    orders['user_name']=userId
    orders['hardware_name']=hardwareId
    orders['status']="OK!"
    orders['data']= json.loads(serializers.serialize("json",[order]))
    orders['date']=formattedDate

    






 #   print request.META['REMOTE_ADDR']
#    orders['client_id']=request.META['REMOTE_USER']



    return HttpResponse(json.dumps(orders))





#    jsonI=serializers.serialize("json",order)




#    return HttpResponse(json.dumps(json.loads(jsonI)))










# def getOrdersByDate(request, dateOfOrder):
#     ordersToBeReturned=[]
#     dateFormatted = datetime.datetime.strptime(dateOfOrder, '%Y-%m-%d').date()
    




#currently Not Used


def getOrdersByDate(request, dateOfOrder):

    ordersToBeReturned = []

    numberOfRows = BarOrder.objects.count()
    for i in range(0, numberOfRows):


        ithOrder = BarOrder.objects.all()[i]
        ithOrderDict={}

        dateOfIthOrder = datetime.datetime.strftime(ithOrder.created, '%H:%M:%S %Y-%m-%d')
        comparableDate = datetime.datetime.strftime(ithOrder.created, '%Y-%m-%d')
        itemsOfIthOrder=[]

        for i in ithOrder.items:
            itemsOfIthOrder.append(json.loads(serializers.serialize("json", [i])))

        comparatorLeft = comparableDate.decode('utf-8')
        comparatorRight = dateOfOrder
        
        ithOrderDict['id']=ithOrder.uid
        ithOrderDict['total']=ithOrder.total
        ithOrderDict['items']=itemsOfIthOrder
        ithOrderDict['date']=dateOfIthOrder

        # print (type(comparatorRight))
        # print (type(comparatorLeft))
        # print "------------"


        if (comparatorLeft==comparatorRight):
            ordersToBeReturned.append(ithOrderDict)



    counter = len(ordersToBeReturned)
    return HttpResponse(json.dumps(ordersToBeReturned))

#    return HttpResponse(json.dumps(ordersToBeReturned))


    

#    for itera in range(0, BarOrder.objects.count())
 #       print itera

    # listOfOrders = BarOrder.objects.all()
    # datesOfOrders = []

    # for o in listOfOrders:
    #     if dateOfOrder==
    #     datesOfOrders.append(o.created.date())




    # return HttpResponse(datesOfOrders)






def addClientUser(request, barid, user, teststring):
    if teststring == "dingdong123":
        bar = Bar.objects.get(uid=barid)
        user = DjangoUser.objects.get(username=user)
        cl = Client()
        cl.users.extend([user])
        cl.emb_obj = bar
        cl.ttype = "bar"
        cl.hardware_id = ""
        cl.ip = "123.456.789.123"
        cl.save()

        return HttpResponse("Done")
    else:
        return HttpResponse("eh")




def getOrdersByDateAndClientId(request, dateOfOrder, clientId):
    #return HttpResponse("shemodis")

    ordersToBeReturned = []

    numberOfRows = BarOrder.objects.filter(client_id=clientId).count()
    for i in range(0, numberOfRows):


        ithOrder = BarOrder.objects.filter(client_id=clientId)[i]
        ithOrderDict={}

        dateOfIthOrder = datetime.datetime.strftime(ithOrder.created, '%H:%M:%S %Y-%m-%d')
        comparableDate = datetime.datetime.strftime(ithOrder.created, '%Y-%m-%d')
        itemsOfIthOrder=[]

        for i in ithOrder.items:
            itemsOfIthOrder.append(json.loads(serializers.serialize("json", [i])))

        comparatorLeft = comparableDate.decode('utf-8')
        comparatorRight = dateOfOrder
        
        ithOrderDict['id']=ithOrder.uid
        ithOrderDict['total']=ithOrder.total
        ithOrderDict['items']=itemsOfIthOrder
        ithOrderDict['date']=dateOfIthOrder

        if (comparatorLeft==comparatorRight):
            ordersToBeReturned.append(ithOrderDict)



    counter = len(ordersToBeReturned)
    return HttpResponse(json.dumps(ordersToBeReturned))

@csrf_exempt
def getAllInventoriesQuantity(request):
    
    resultSetToReturn=[]
    resultItem={}
    clientId=request.POST['clientId']
    client = Client.objects.get(pk=clientId)
    bar_id = client.emb_obj.uid
    print bar_id
    #"aqamde modis"

    for inv in Inventory.objects.filter(bar_id=bar_id):
#        print inv.product_id
        product_name=Product.objects.get(pk=inv.product_id).name.encode('utf-8')
 #       print product_name

        resultItem['productId']=inv.product_id
        resultItem['productName']=product_name
        resultItem['quantity']=inv.qty
  #      print resultItem
        print resultItem['productId']

        resultSetToReturn.append(resultItem.copy()) #MAJOR THING FIXED!................


    return HttpResponse (json.dumps(resultSetToReturn))




@csrf_exempt
def fillInventory(request):
    item = request.POST["itemId"]
    clientId=request.POST["clientId"]
    quantity = request.POST["quantity"]

    #fields for IncomeLog method
    priceWOVat = request.POST["priceWOVat"]
    priceWVat = request.POST["priceWVat"]

    product = Product.objects.get(pk=item)
    insertIncomeLog(product, priceWVat, priceWOVat, quantity=quantity)






    client = Client.objects.get(pk=clientId)
    bar_id=client.uid
    wasMatched=False

    for inv in Inventory.objects.all():
        if inv.product_id==item:
            wasMatched=True
            print ("adding already existed inventory")
            itemsQuantity=int(inv.qty)
            quantityPosted=int(quantity)
            print type(itemsQuantity)
            print type(quantityPosted)
            inv.qty=itemsQuantity+quantityPosted
            print "quantity amount extended"
            inv.save()
            return HttpResponse("Product's Quantity Extended in Inventory")


    print item 
    print client
    print quantity
    inv = Inventory.objects.create(show_out_of_stock_at=0,product_id=item,notify_front_about_stock_qty_at=5,bar_id=bar_id,qty=quantity)

    if inv:
        return HttpResponse("successfully added")
    else:
        return HttpResponse("..unsuccessfull try.. :(")


def insertIncomeLog(item, priceWVat, priceWOVat, quantity):
    priceWithoutVat = priceWOVat
    priceWithVat = priceWVat
    date = datetime.datetime.now()
    BarIncomeLog.objects.create(productId=item, priceWVat=priceWVat, priceWOVat=priceWOVat, quantity=quantity, importDate=date)

