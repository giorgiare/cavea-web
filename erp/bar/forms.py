from django import forms
from django.forms import ModelForm
from django.forms.extras.widgets import SelectDateWidget
from models import Product, Category


class ProductForm(ModelForm):

    class Meta:
        model = Product
        fields = ["sku", "name", "barcode", "description", "enabled", "price", "has_discount", "special_price", "image_base", "image_thumbnail" ]
        exclude = ("inventory",)
    '''
    sku = forms.CharField(required=True)
    name = forms.CharField(required=True)
    barcode = forms.CharField(required=True)
    description = forms.CharField(widget=forms.Textarea, required=False)
    enabled = forms.BooleanField(required=False)
    price = forms.DecimalField(decimal_places=2, max_digits=6, required=True)
    has_discount = forms.BooleanField(required=False)
    special_price = forms.DecimalField(decimal_places=2, max_digits=6, required=False)
    image_base = forms.FileField()
    image_thumbnail = forms.FileField()
    '''


class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = ["name", "enabled", "show_in_navigation"]
        exclude = ("children", "products")

class PRODCSVUploadForm(forms.Form):
    file = forms.FileField(required=False)

class INVCSVUploadForm(forms.Form):
    file = forms.FileField(required=False)
