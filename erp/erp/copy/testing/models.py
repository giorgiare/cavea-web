from djangotoolbox.fields import EmbeddedModelField
from django_mongodb_engine.contrib import MongoDBManager
from jsonfield import JSONField
from django.db import models
from forms import StringListField
from djangotoolbox.fields import ListField
from django.db.models.signals import post_init
from forms import ObjectListField

 



class EmbedOverrideField(EmbeddedModelField):
    def formfield(self, **kwargs):
        return models.Field.formfield(self, ObjectListField, **kwargs)

class CategoryField(ListField):
    def formfield(self, **kwargs):
        return models.Field.formfield(self, StringListField, **kwargs) 


    
class Size():
    width = None
    height = None
    def __init__(self, width, height):
        self.width = width
        self.height = height

class Theater(models.Model):
    name = models.CharField(max_length=40)
    company_id = models.CharField(max_length=20)
    address = models.CharField(max_length=70)
    lat = models.FloatField(blank=True, null=True)
    lng = models.FloatField(blank=True, null=True)
    zip_code = models.CharField(max_length=8)
    city = models.CharField(max_length=30)
    
    def __unicode__(self):
        return self.name

class Hall(models.Model):
    name = models.CharField(max_length=20)
    title = models.CharField(max_length=30)
    theater = models.ForeignKey(Theater,blank=True, null=True);
    number_of_seats = models.IntegerField(blank=True, null=True)
    rows = JSONField()
     
    def __unicode__(self):
        return self.title

class Section(models.Model):
    name = models.CharField(max_length=20)
    category = models.CharField(max_length=20)	    

class Seat(models.Model):
    uid = models.CharField(max_length=20)
    hall = models.ForeignKey(Hall, null=True)
    section = models.CharField(max_length=30)
    name = models.CharField(max_length=20)
    row = models.CharField(max_length=10)
    seat = models.IntegerField(blank=True,null=True)
    status = models.CharField(max_length=40,blank=True,null=True)
    EMPTY = "emp"
    ECONOMY = "eco"
    STANDARD = "std"
    STATUS = "sta"
    VIP = "vip"
    
    SEAT_TYPES = ((ECONOMY, "Economy"),(STANDARD, "Standard"),(STATUS,"Status"),(VIP,"VIP"),(EMPTY,"Empty"))
    ttype = models.CharField(max_length=3,choices=SEAT_TYPES)
    
    def __unicode__(self):
        return r"Row:%s Seat:%s" % (self.row, self.seat)

class Movie(models.Model):
    name = models.CharField(max_length=120)
    country = models.CharField(max_length=30)
    duration = models.IntegerField()
    
    def __unicode__(self):
        return self.name

class Session(models.Model):
    m_name = models.CharField(max_length=50, editable=False)
    uid = models.CharField(max_length=60, blank=True, null=True,editable=True)
    start_time = models.DateTimeField()
    ddate = models.DateField()
    duration=models.IntegerField()
    hall = models.ForeignKey(Hall)
    movie = models.ForeignKey(Movie)
    prices = JSONField(null=True, blank=True)
    
    def save(self, *args, **kwargs):
        str = self.hall.name.split("_")
        self.m_name = self.movie.name
        self.uid = r"%s_%s/%s-%s-%s/%s:%s" % (str[0][:3],str[1],self.start_time.day,self.start_time.strftime("%m"),self.start_time.year,self.start_time.hour,self.start_time.strftime("%M")) 
        super(self.__class__, self).save(*args, **kwargs)
    
    def __unicode__(self):
        #return "%s | %s | %s-%s %s:%s" % (self.hall.title, self.movie.name ,self.start_time.strftime("%d"),self.start_time.strftime("%b"), self.start_time.hour,self.start_time.strftime("%M")) 
        return self.uid

class Ticket(models.Model):
    created = models.DateTimeField(auto_now_add=True, blank=True)
    session = models.ForeignKey(Session)
    seat = models.ForeignKey(Seat)
    stype = models.CharField(max_length=20)
    
    @property
    def Price(self):
        return "GEL %s" % self.price
    
    RESERVED = "RE"
    SOLD = "SL"
    TICKET_TR_TYPES = ((RESERVED, "RE"),(SOLD, "SL"))
    trtype = models.CharField(max_length=2,choices=TICKET_TR_TYPES)
    
    PRINTED = "PR"
    DIGITAL = "DG"
    TICKET_TYPES = ((PRINTED, "Printed"),(DIGITAL, "Digital"))
    ttype = models.CharField(max_length=2,choices=TICKET_TYPES, default=PRINTED)
    has_discount = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=3,decimal_places=2)
    date_printed = models.DateField(blank=True, null=True)
    date_sold = models.DateTimeField(blank=True, null=True)
 
    def save(self, *args, **kwargs):
        if not self.price:
            if self.seat.ttype == "std":
                self.price = self.session.prices["std"]
                self.stype = "Standard"
            elif self.seat.ttype == "eco":
                self.stype = "Economy"
                self.price = self.session.prices["eco"]
            elif self.seat.ttype == "vip":
                self.stype = "VIP"
                self.price = self.session.prices["vip"]
            elif self.seat.ttype == "sta":
                self.stype = "Status"
                self.price = self.session.prices["sta"]   
        super(self.__class__, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return "%s_%s_%s" % (self.session.hall.name,self.session.start_time,self.seat) 
    
class User(models.Model):
    name = models.CharField(max_length=40)
    surname = models.CharField(max_length=40)
    email = models.EmailField()
    
class Order(models.Model):
    ticket = models.ForeignKey(Ticket)
    utype = models.CharField(max_length=2,choices=(("ANON","Anonymous"),("USER","User")), default="ANON")
    user = models.ForeignKey(User, null=True, blank=True)
    
class Employee(models.Model):
    ser_num = models.IntegerField(blank=True, null=True) 
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    date_of_birth = models.DateField()
    id = models.AutoField(primary_key=True)
    hired = models.DateField()
    position = models.CharField(max_length=50)
    education = models.CharField(max_length=50)
    job_descr = models.TextField(blank=True)
   # def __init__(self, *args, **kwargs):
        #self.ser_num =  Employee.objects.all().order_by("-ser_num")[0].ser_num + 1	
    #    pass
    def save(self, *args, **kwargs):
        if not self.ser_num: 
            self.ser_num = len(Employee.objects.all())+1
            super(self.__class__, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return u'%s %s' % (self.name, self.surname)

def incrementEmployee(**kwargs):
    instance = kwargs.get("instance")
    instance.ser_num = Increment.objects.all().order_by("-employee")[0].employee +18 
    
class BoxOffice(models.Model):
    name = models.CharField(max_length=20)
    theater = EmbeddedModelField("Theater", blank=True, null=True)
    number_of_cash_registers = models.IntegerField()
    
class CashRegister(models.Model):
    name = models.CharField(max_length=20)
    box_office = EmbeddedModelField("BosOffice", blank=True, null=True)

class Shift(models.Model):
    started = models.DateTimeField()
    employee = EmbeddedModelField("Employee", blank=True, null=True)
    cash_register = EmbeddedModelField("CashRegister", blank=True, null=True)
    
class Row(models.Model):
    hall_id = models.CharField(max_length=60, blank=True)
    seats = ListField(blank=True, null=True)
    name = models.CharField(max_length=10)
    objects = MongoDBManager()

class Transaction(models.Model):
    created = models.DateTimeField(auto_now_add=True, blank=True)
    ticket = models.ForeignKey(Ticket)
    shift = models.ForeignKey(Shift,blank=True, null=True)
    ttype = models.CharField(max_length=2,choices=(("SL","Sold"),("RT","Returned"),("RS","Reserved")), default="SL")
    amount = models.DecimalField(max_digits=6,decimal_places=2)

class Validation(models.Model):
    ticket = EmbeddedModelField("Ticket", editable=True)
    user = models.CharField(max_length=40,blank=True, null=True)
    created = models.DateField(auto_now_add=True,blank=True)
    status = models.CharField(max_length=40,blank=True, null=True)
    
