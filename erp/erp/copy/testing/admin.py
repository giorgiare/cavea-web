from django.contrib import admin
from models import *

def ttype(object):
    return object.seat.ttype

def movie_name(object):
    return object.movie.name

def hall_name(object):
    return object.hall.title

def movie_name_ticket(objects):
    return objects.session.movie.name

def theater_name(object):
    return object.hall.theater.name
     
class TheaterAdmin(admin.ModelAdmin):
    #list_filter = ('title','t')
    #search_fields = ['company_id']
    #list_display = ('company','amount','vat','discount','description', 'original_id', company_IBAN)
    list_display = ('name','address')

class HallAdmin(admin.ModelAdmin):
    ordering = ("title",)
    list_display = ('title', "theater", "number_of_seats", "rows")

class SessionAdmin(admin.ModelAdmin):
    #date_hierarchy = "ddate"
    
    ordering = ("-ddate",)
    list_filter = ("ddate","hall", "movie")
    list_display = (movie_name, hall_name,'start_time', "ddate","duration", "prices")
    

class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('name','surname', "date_of_birth", "hired", "position", "education", "job_descr")
    
class MovieAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', 'duration')

class SeatAdmin(admin.ModelAdmin):
    #search_fields = ['company_id']
    list_filter = ("hall","ttype")
    list_display = ('name', hall_name, "row","seat","ttype")

class TicketAdmin(admin.ModelAdmin):
    list_filter = ("session","trtype")
    ordering = ("-created",)
    list_display = ( 'session',movie_name_ticket,'created', "seat", "trtype",ttype)

class TransactionAdmin(admin.ModelAdmin):
    list_display = ('created', "ticket", "shift", "ttype","amount")

class ValidationAdmin(admin.ModelAdmin):
    list_display = ("id","ticket", "user","created")


admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Session, SessionAdmin)
admin.site.register(Hall, HallAdmin)
admin.site.register(Theater, TheaterAdmin)
admin.site.register(Movie, MovieAdmin)
admin.site.register(Seat, SeatAdmin)
admin.site.register(Ticket, TicketAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(Validation, ValidationAdmin)
