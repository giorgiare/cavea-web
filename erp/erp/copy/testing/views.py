
from django.core.mail import EmailMultiAlternatives
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render
from testing.models import *
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
import datetime
import json
from django.http.response import HttpResponseRedirect
from decimal import Decimal
from django.core import serializers
from django.conf import settings
import qrcode

@login_required
def home(request):
    employee = Employee.objects.all()
    #for invoice in invoices:
        #print invoice.emission_date
    return render(request, "base_extended.html", {'employee':employee})

def logout_user(request):
    auth.logout(request)
    # Redirect to a success page.
    return HttpResponseRedirect("/accounts/login/")

def account_login(request):
    path = request.GET.get("next")
    if path == None:
        path = "/search/" 
    return render(request, "registration/login.html", {'next': path})

def login_user(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    next = request.POST.get('next')
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        auth.login(request, user)
        return HttpResponseRedirect(next)
    else:
        return HttpResponseRedirect("/accounts/login/")

#@ensure_csrf_cookie
def post(request):
    if 'data' in request.POST and request.POST['data']:
        data = request.POST["data"]
        employees = Employee.objects.filter(name__istartswith=data)
        html =["<ul>"]
        for  employee in employees:
            html.append("<li class='fill_li' id='' onclick='fill_input(event)'>"+employee.surname+"</li>")
             
        html.append("</ul>")     
        post = "".join(html)
        #post_json = json.dumps(post)
        return HttpResponse(post)

@login_required
def search(request):
    if 'name' in request.POST and request.POST['name']:
        name = request.POST['name']
        employee = Employee.objects.get(name=name)
        if not company:
            error = "no company found with this name!"   
            return render(request, "search.html", {'error': error})
        employee_id = company.id
        return render(request, "search.html", {'surname':surname})
    name = "nothing"
    return render(request, "search.html", {'name': name})


@login_required
def hall(request):
    if 'name' in request.GET and request.GET['name']:
        name = request.GET['name']
        hall = Hall.objects.get(name=name)
        rows = []
        for row in hall.rows:        
          seats = Seat.objects.filter(hall=hall, row=row).order_by("id")
          dict = {"name": row, "seats":seats}
          rows.append(dict)
         
         
        #rows = Row.objects.filter(hall_id=hall.id).order_by("-id")
        
        if not hall or not rows:
            error = "no hall found with this name!"   
            return render(request, "hall_.html", {'error': error})
        return render(request, "hall_.html", {'hall':hall, "rows":rows})
    name = "nothing"
    return render(request, "hall_.html", {'hall': hall, "rows":rows})
def session_hall(request, hall):
    hall = Hall.objects.get(name=hall)
    sessions = Session.objects.filter(hall=hall).order_by("-ddate")
    return render(request, "session_list.html",{"sessions":sessions})

def session(request, hall,date,time):
    sess_name ="%s/%s/%s" % (hall, date,time)
    #if 'sess' in request.GET and request.GET['sess']:
    var= True
    if hall and date and time:
        ip = request.META.get('REMOTE_ADDR')
    
        session = Session.objects.get(uid=sess_name)
        hall = session.hall
        time = datetime.datetime.now()
        
        date_ = "%s-%s-%s" % (session.start_time.day,session.start_time.strftime("%m"),session.start_time.year) 
        time = "%s:%s" % (session.start_time.hour,session.start_time.strftime("%M"))
        
        
        
        #start_time = {"year":year, "month":month, "day":day, "hour":hour,"min":min}
        seat_types = session.prices
        rows = []
        for row in hall.rows:        
          seats = Seat.objects.filter(hall=hall, row=row).order_by("id")
          dict = {"name": row, "seats":seats}
          rows.append(dict)
         
         
        #rows = Row.objects.filter(hall_id=hall.id).order_by("-id")
        
        if not hall or not rows:
            error = "no hall found with this name!"   
            return render(request, "session.html", {'error': error})
        return render(request, "session.html", {'hall':hall, "rows":rows, "time":time, "sess":sess_name,"ip":ip,"const":settings.CONST, "date":date_,"time":time, "seat_types":seat_types})
    elif hall:
        return HttpResponse("smth")    
    name = "nothing"
    return render(request, "session.html", {'hall': hall, "rows":rows, "time":time})




def ticket_order(request):
    if 'data' in request.POST and request.POST['data']:
        arr = json.loads(request.POST['data'])
        uid = request.POST['sess']
        trtype = request.POST['trtype']
        session = Session.objects.get(uid=uid)
        seats = []
        for ticket in arr:
            seat = Seat.objects.get(uid=ticket)
            tickets = Ticket.objects.filter(seat=seat, session=session)
            
            if not tickets:
                ticket = Ticket.objects.create(session=session, seat=seat,trtype=trtype, has_discount=False, 
                                           price=0.00, date_printed="2015-01-12", date_sold=datetime.datetime.now())
                ticket.save()
                if ticket.trtype=="SL":
                    val = Validation.objects.create(ticket=ticket, user="giorgiare")
                    val.save()
                    img = qrcode.make("http://192.168.1.40:8000/validation/%s" % val.id)
                    img.save("%s.jpg" % val.id,"JPEG")
                    subject, from_email, to = 'hello', 'from@example.com', 'giorgiare@gmail.com'
                    text_content = 'This is an important message.'
                    html_content = '<p>This is an <strong>important</strong> message.</p>'
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.attach_file("%s.jpg" % val.id)
                    msg.send()
                seats.append(seat)
        return HttpResponse(serializers.serialize("json",seats))
    if 'rs' in request.POST and request.POST['rs']:
        arr = json.loads(request.POST['rs'])
        uid = request.POST['sess']
        
        session = Session.objects.get(uid=uid)
        seats = []
        for ticket in arr:
            seat = Seat.objects.get(uid=ticket)
            tickets = Ticket.objects.filter(seat=seat, session=session)
            for t in tickets:
                if t.trtype =="RE":
                    t.trtype = "SL"
                    t.save()
                    val = Validation.objects.create(ticket=t, user="giorgiare")
                    val.save()
                    img = qrcode.make(val.id)
                    img.save(val.id,"JPEG")
        return HttpResponse(serializers.serialize("json",seats))
                    

                        
                    
def ticket(request):
    if 'sess' in request.POST and request.POST['sess']:
        sess_id = request.POST['sess']
        session = Session.objects.get(uid=sess_id)
        tickets = Ticket.objects.filter(session=session)
        seats = []
        for ticket in tickets:
            ticket.seat.status = ticket.trtype
            seats.append(ticket.seat)
        return HttpResponse(serializers.serialize("json",seats))
    

def test(request):
    return HttpResponse("testing...")

def ticketSales(request,hall, date,time):
    sess_name ="%s/%s/%s" % (hall, date,time)
    session = Session.objects.get(uid=sess_name)
    hall = session.hall
    
    tickets = Ticket.objects.filter(session=session)
    number_of_eco = len(Seat.objects.filter(hall=hall,ttype="eco"))
    number_of_std = len(Seat.objects.filter(hall=hall,ttype="std"))
    number_of_sta = len(Seat.objects.filter(hall=hall,ttype="sta"))
    number_of_vip = len(Seat.objects.filter(hall=hall,ttype="vip"))    
    
    number_of_reserved = 0
    number_of_sold = 0 
    
    occupied_eco = 0
    occupied_std = 0
    occupied_sta = 0
    occupied_vip = 0
    
    for ticket in tickets:
        if ticket.seat.ttype == "eco":
            occupied_eco += 1
        if ticket.seat.ttype == "std":
            occupied_std += 1
        if ticket.seat.ttype == "sta":
            occupied_sta += 1
        if ticket.seat.ttype == "vip":
            occupied_vip += 1
        if ticket.trtype =="RE":
            number_of_reserved +=1
        if ticket.trtype =="SL":
            number_of_sold +=1
    
    ototal = number_of_reserved+number_of_sold        
    total =   number_of_eco-occupied_eco+  number_of_std-occupied_std+number_of_sta-occupied_sta+number_of_vip-occupied_vip
    dict = {"eco": (number_of_eco-occupied_eco),"std":number_of_std-occupied_std,
                "sta":number_of_sta-occupied_sta,"vip":number_of_vip-occupied_vip, 
                "total":total, "reserved":number_of_reserved, 
                "sold":number_of_sold, "ototal":ototal}
        
    if not hall:
        error = "no hall found with this name!"   
        return render(request, "test.html", {'error': error})
    
    return HttpResponse(json.dumps(dict))

def validation(request, ticket):
    val = Validation.objects.get(id=ticket)
    
    if val:
        if val.status =="valid":
            return HttpResponse("<html><head></head><body style='background-color: red;'></body></html>")
        val.status = "valid"
        val.save()
        return HttpResponse("<html><head></head><body style='background-color: green;'></body></html>")
    else:
        return HttpResponse("<html><head></head><body style='background-color: red;'></body></html>")
