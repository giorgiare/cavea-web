from django.contrib import admin
from models import Post, Theater, Hall

class PostAdmin(admin.ModelAdmin):
    #list_filter = ('title','t')
    #search_fields = ['company_id']
    #list_display = ('company','amount','vat','discount','description', 'original_id', company_IBAN)
    list_display = ('title','text','tags','comments')
    

class TheaterAdmin(admin.ModelAdmin):
    #list_filter = ('title','t')
    #search_fields = ['company_id']
    #list_display = ('company','amount','vat','discount','description', 'original_id', company_IBAN)
    list_display = ('name','address')
admin.site.register(Post, PostAdmin)
admin.site.register(Theater, TheaterAdmin)
