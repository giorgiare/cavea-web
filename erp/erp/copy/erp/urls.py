from django.conf.urls import patterns, include, url
from django.contrib import admin

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'testing.views.home', name='home'),
    url(r'^logout/$', "testing.views.logout_user"),
    url(r'^accounts/login/$',"testing.views.account_login"),
    url(r'^login/$', "testing.views.login_user"),  
    url(r'^search/$', "testing.views.search"),
    url(r'^post/$', "testing.views.post"),
    url(r'^hall/$', "testing.views.hall"),
    url(r'^session/$', "testing.views.session"),
    url(r'^ticket/$', "testing.views.ticket"),
    url(r'^ticket_order/$', "testing.views.ticket_order"),
    url(r'^ticketSales/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{2}:\d{2})/$',"testing.views.ticketSales"),
    url(r'^session/(?P<hall>\w{5})/(?P<date>\d{2}-\d{2}-\d{4})/(?P<time>\d{2}:\d{2})/$',"testing.views.session"),
    url(r'^session/(?P<hall>\w{5})/$',"testing.views.session_hall"),
    url(r'^validation/(?P<ticket>\w+)/$',"testing.views.validation"),
    url(r'^session/add/$',"testing.views.session_add"),
    #url(r'^erp/', include('erp.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
)
