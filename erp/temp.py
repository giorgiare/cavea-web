from testing.models import Hall, Seat

hall = Hall.objects.get(name="rustaveli_1")



row_names = ["A", "B", "C", "D"]
rows = [10,12,12,12]

for r in range(0,3):
  for n in range(1,rows[r]):
    uid = "rus1_r%s_s%s" % (row_names[r],n)
    name = "r%ss%s" % (row_names[r],n)
    row = "%s" % row_names[r]
    seat = Seat.objects.create(hall=hall, uid=uid, name=name, row=row, seat=n,section="TBCinema", ttype="vip")
    seat.save()
        
        
for n  in range(1,31):
  uid = "rus1_r16_s%s" % n
  name = "r16s%s" % n
  row = "16"      
  seat = Seat.objects.create(hall=hall, uid=uid, name=name, row=row, seat=n,section="TBCinema", ttype="vip")
  seat.save()            
        

seats = Seat.objects.filter(uid__contains="rus1_r5")
for seat in seats:
    seat.ttype= "eco"
    seat.save()
    
str = [(),(7,20), (6,21), (6,21),(6,21),(6,21),(6,21),(6,21),(7,20),(8,19)]
c = 0
for r in range(6,15):
    c+=1
    for n in range(str[c][0],str[c][1]):
        uid = "rus1_r%s_s%s" % (r,n)            
        seat = Seat.objects.get(uid=uid)
        seat.ttype= "sta"
        seat.save()
        
for n in range(1,26):
    uid = "rus1_r5_s%s" % n

#rustaveli_2

hall = Hall.objects.get(name="rustaveli_2")       
       
for r in range(1,len(hall.rows)+1):
  for n in range(1,14):
    uid = "rus2_r%s_s%s" % (r,n)
    name = "r%ss%s" % (r,n)
    row = "%s" % r
    seat = Seat.objects.create(hall=hall, uid=uid, name=name, row=row, seat=n,section="main", ttype="std")
    seat.save()

            
str = [(),(4,11), (4,11), (4,11)]
c = 0
for r in range(4,7):
    c+=1
    for n in range(str[c][0],str[c][1]):
        uid = "rus2_r%s_s%s" % (r,n)            
        seat = Seat.objects.get(uid=uid)
        seat.ttype= "sta"
        seat.save()     
        
seats = Seat.objects.filter(uid__contains="rus2_r3")
for seat in seats:
    seat.ttype= "eco"
    seat.save()     
    
#rustaveli 3   

hall = Hall.objects.get(name="rustaveli_3")       
       
for r in range(1,len(hall.rows)+1):
  for n in range(1,14):
    uid = "rus3_r%s_s%s" % (r,n)
    name = "r%ss%s" % (r,n)
    row = "%s" % r
    seat = Seat.objects.create(hall=hall, uid=uid, name=name, row=row, seat=n,section="main", ttype="std")
    seat.save()

            
str = [(),(4,11), (4,11), (4,11),(4,11)]
c = 0
for r in range(4,8):
    c+=1
    for n in range(str[c][0],str[c][1]):
        uid = "rus3_r%s_s%s" % (r,n)            
        seat = Seat.objects.get(uid=uid)
        seat.ttype= "sta"
        seat.save()     
        
seats = Seat.objects.filter(uid__contains="rus3_r3")
for seat in seats:
    seat.ttype= "eco"
    seat.save()         
    
 #rustaveli 4 

hall = Hall.objects.get(name="rustaveli_4")       
       
for r in range(1,len(hall.rows)+1):
  for n in range(1,13):
    uid = "rus4_r%s_s%s" % (r,n)
    name = "r%ss%s" % (r,n)
    row = "%s" % r
    seat = Seat.objects.create(hall=hall, uid=uid, name=name, row=row, seat=n,section="main", ttype="std")
    seat.save()

            
str = [(),(4,10), (4,10), (4,10),(4,10)]
c = 0
for r in range(4,8):
    c+=1
    for n in range(str[c][0],str[c][1]):
        uid = "rus4_r%s_s%s" % (r,n)            
        seat = Seat.objects.get(uid=uid)
        seat.ttype= "sta"
        seat.save()     
        
seats = Seat.objects.filter(uid__contains="rus4_r3")
for seat in seats:
    seat.ttype= "eco"
    seat.save()          
    
 #rustaveli 5

hall = Hall.objects.get(name="rustaveli_5")       
       
for r in range(1,len(hall.rows)+1):
  for n in range(1,19):
    uid = "rus5_r%s_s%s" % (r,n)
    name = "r%ss%s" % (r,n)
    row = "%s" % r
    seat = Seat.objects.create(hall=hall, uid=uid, name=name, row=row, seat=n,section="main", ttype="std")
    seat.save()

            
str = [(),(8,13), (7,14), (6,15),(6,15),(7,14), (8,13)]
c = 0
for r in range(4,10):
    c+=1
    for n in range(str[c][0],str[c][1]):
        uid = "rus5_r%s_s%s" % (r,n)            
        seat = Seat.objects.get(uid=uid)
        seat.ttype= "sta"
        seat.save()     
        
seats = Seat.objects.filter(uid__contains="rus5_r3")
for seat in seats:
    seat.ttype= "eco"
    seat.save()                  
    
#amirani_1
hall = Hall.objects.get(name="amirani_1")   
str = [0,27,28,29,30,31,30,31,30,31,30,29,28,27,26,25,20]


for r in range(1,len(hall.rows)+1):
    for n in range(1,str[r]+1):
        uid = "ami1_r%s_s%s" % (r,n)
        name = "r%ss%s" % (r,n)
        row = "%s" % r
        seat = Seat.objects.create(hall=hall, uid=uid, name=name, row=row, seat=n,section="main", ttype="std")
        seat.save()
    
str = [(),(9,23), (9,24), (8,24),(8,25),(8,24), (8,23), (8,22)]
c = 0
for r in range(6,13):
    c+=1
    for n in range(str[c][0],str[c][1]):
        uid = "ami1_r%s_s%s" % (r,n)            
        seat = Seat.objects.get(uid=uid)
        seat.ttype= "sta"
        seat.save()     
        
seats = Seat.objects.filter(uid__startswith="ami1_r1")
for seat in seats:
    seat.ttype= "std"
    seat.save()        
        